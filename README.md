# Evaluation of Anterior Cruciate Ligament Surgical Reconstruction Through Finite Element Analysis

## Description
This directory contains the numerical models used throughout the following 
publication:

(insert publication here)

If you use any model in your research please cite us:

(insert citation here)

## Contents

1. **scripts**: This directory contains the scripts for replicating the 
  published results.
  
1. **data**: This directory contains the FEBio models (.feb), and the 
  simulation results (.txt) for all cases.
    - **reference_model**: Reference model data.
    - **single_bundle**: ACL reconstructed model with the Single Bundle 
    technique.
    - **double_bundle**: ACL reconstructed model with the Double Bundle 
      technique.
      
1. **results**: Directory containing the generated figures, after executing 
   the scripts.
  
1. **software**
   - FEBio Software Suite installer

1. **videos**
    
    - **aclr_movie.mp4**: A video that highlights methodology and results 
       of the publication.
    
1. **venv**
    
    - A virtual environment to ensure that the required dependencies for the 
      scripts are available. 
  
## Reproducing the published results

Run the scripts inside the "scripts" directory. The figures will be stored 
in the relevant subdirectory inside the "results" folder. The functions to 
process the experimental data were acquired by the OpenKnee project 
repository: https://simtk.org/projects/openknee.
 
- _anterior_posterior_laxity_validation.py_: This script generates the 
  results for the sensitivity analysis of ligament parameters. The results 
  are saved in the results/AP_Laxity subdirectory.

- _passive_flexion_validation.py_: This script prints the results for the 
  passive flexion simulation that was used to validate the reference FE 
  model. The results are saved in the results/Passive_Flexion subdirectory.
  
- _graft_tension_radius_results.py_: This script prints the effect of graft 
  radius and graft tension in restraining knee laxity. The results 
  are saved in the results/Lachman subdirectory.
  
- _graft_choice_results.py_: This script prints the effect of three different 
  graft tissues on knee laxity. It visualizes all combinations of graft 
  pretension and radius values for each of the three grafts. The results 
  are saved in the results/Lachman subdirectory.
  
- _graft_fixation_angle_results.py_: This script prints the results 
  regarding the effect of graft fixation angle in knee laxity restrain. The 
  results are saved in the results/Lachman subdirectory.

- _single_b_vs_double_b_results.py_: This script generates the figures 
  comparing the performance of single bundle versus double bundle techniques. The results 
  are saved in the results/Lachman subdirectory.

## Models

The developed models are available in the data directory that is organized in 
the following way:

1. reference_model
   
    - Models: the reference model, with calibrated ligament parameters for the 
      cruciate ligaments.

2. single_bundle
   
    - semitendinosus/Models: A single bundle ACLR model with a semitendinosus 
      graft. We provide different model versions regarding graft radius and 
      pretension.
      
    - gracilis/Models: A single bundle ACLR model with a gracilis graft.
      We provide different model versions regarding graft radius and pretension.
    
    - patellar_tendon/Models: A single bundle ACLR model with a patellar tendon 
      graft. We provide different model versions regarding graft radius and 
      pretension.
    
3. double_bundle
    
    - semitendinosus/Models: A double bundle ACLR model with two 
      semitendinosus grafts each with a radius equal to 2.5 mm.

## Acknowledgments 

This work was supported by the Greek funded project SafeACL: : Decision 
Support Software for Anterior Cruciate Ligament Reconstruction based on 
Individualized Musculoskeletal Computer Models, Grant Agreement No. 
T1EDK-04234 [SafeACL](http://safeacl.gr/). This project was developed in the 
[Visualization and Virtual Reality Group](http://www.vvr.ece.upatras.gr/), 
University of Patras, Greece. 