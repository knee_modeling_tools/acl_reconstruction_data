File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.555319561641,0,0,0.000101114901717,0,0
2,0.108641937665,0,0,-0.00248043993175,0,0
3,-0.0525003672271,-0,-0,0.0038033147552,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.612052864096,0,0,0.000139557768509,0,0
2,0.0981472745561,0,0,-0.00273856983239,0,0
3,-0.0817424299852,-0,-0,0.0058344312614,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 3
*Time  = 0.00031
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.63866054428,0,0,0.000162952087719,0,0
2,0.0896888445706,0,0,-0.00286660112699,0,0
3,-0.0987139509405,-0,-0,0.00721400663941,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 4
*Time  = 0.00156
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.651727884727,0,0,0.000175033132344,0,0
2,0.0833028248076,0,0,-0.00294173244801,0,0
3,-0.107614368743,-0,-0,0.00786267643316,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 5
*Time  = 0.00781
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.664263141327,0,0,0.000173196200645,0,0
2,0.0814667189072,0,0,-0.00299165333671,0,0
3,-0.108762119242,-0,-0,0.00815347232886,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 6
*Time  = 0.03906
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.679839135131,0,0,0.000120145699916,0,0
2,0.0619792241695,0,0,-0.00287354291064,0,0
3,-0.0874057711538,-0,-0,0.00999054366677,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 7
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.661355433392,0,0,0.000105496850784,0,0
2,-0.0150640037881,0,0,-0.00281812924874,0,0
3,-0.0765026595288,-0,-0,0.0137167004787,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 8
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.588365144592,0,0,9.77775477424e-06,0,0
2,-0.350051588218,0,0,-0.00291032613522,0,0
3,0.0272540098386,0,0,0.0342999582412,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 9
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.54472537554,0,0,-4.99477910054e-05,0,0
2,-0.542364504555,0,0,-0.00331135986195,0,0
3,0.0755624733382,0,0,0.0470383188966,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 10
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.53629792722,0,0,-0.000105637533886,0,0
2,-0.643251882391,0,0,-0.00371208913809,0,0
3,0.100144268976,0,0,0.0545260802791,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 11
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511931427612,0,0,-0.000160108151844,0,0
2,-0.717459324078,0,0,-0.003990150023,0,0
3,0.121610819064,0,0,0.0607950527071,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 12
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.516083113219,0,0,-0.000213053428327,0,0
2,-0.765855095876,0,0,-0.00418150428039,0,0
3,0.133825377594,0,0,0.0649504106296,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 13
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511473512148,0,0,-0.000267087607814,0,0
2,-0.813438658162,0,0,-0.0043875549977,0,0
3,0.147705151656,0,0,0.0693474579403,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 14
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.482042988272,0,0,-0.000327229931317,0,0
2,-0.850762523276,0,0,-0.00460388413333,0,0
3,0.165405478552,0,0,0.0739895051897,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 15
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.499069747215,0,0,-0.000379294178245,0,0
2,-0.882792505316,0,0,-0.00470033241958,0,0
3,0.172911914042,0,0,0.0767631808226,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 16
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.500523267637,0,0,-0.000434886092442,0,0
2,-0.914773203609,0,0,-0.00483741670622,0,0
3,0.182475155001,0,0,0.0798780759665,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 17
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.494917184461,0,0,-0.000490299779292,0,0
2,-0.945796565465,0,0,-0.00496029054664,0,0
3,0.192565270471,0,0,0.0830522187763,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 18
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.49328978038,0,0,-0.000545351286094,0,0
2,-0.974370109394,0,0,-0.00508358469641,0,0
3,0.20205641277,0,0,0.0860367181448,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 19
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.492170602849,0,0,-0.000600471945982,0,0
2,-0.999640357,0,0,-0.00519650356527,0,0
3,0.210512321172,0,0,0.0887477983242,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 20
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.497854148383,0,0,-0.000657202858551,0,0
2,-1.02305251505,0,0,-0.00533425877272,0,0
3,0.218657575269,0,0,0.0912986610542,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 21
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.489203100024,0,0,-0.000714312155974,0,0
2,-1.04777321113,0,0,-0.00541580915653,0,0
3,0.227311844604,0,0,0.0939922581484,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 22
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.486623246868,0,0,-0.000771212902418,0,0
2,-1.07045694007,0,0,-0.00548242392658,0,0
3,0.234705291687,0,0,0.0963063614082,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 23
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484012891173,0,0,-0.000827674045175,0,0
2,-1.09290825437,0,0,-0.0055505316246,0,0
3,0.24226471491,0,0,0.0986116127355,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 24
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.448635715481,0,0,-0.000888597470683,0,0
2,-1.10785288758,0,0,-0.00569653409192,0,0
3,0.256678546094,0,0,0.101707538777,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 25
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465554763978,0,0,-0.00094142178281,0,0
2,-1.12406840177,0,0,-0.00566057624968,0,0
3,0.258729694096,0,0,0.103014178819,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 26
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.464042607883,0,0,-0.000994268811695,0,0
2,-1.14617751281,0,0,-0.00570646276785,0,0
3,0.26561785899,0,0,0.105221386092,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484004811702,0,0,-0.000996051507279,0,0
2,-1.12493756502,0,0,-0.00570873637765,0,0
3,0.251844278043,0,0,0.103225783334,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.48841337857,0,0,-0.00099762111736,0,0
2,-1.12543469976,0,0,-0.00570262478496,0,0
3,0.250865299662,0,0,0.103073505875,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490134605933,0,0,-0.000998086926206,0,0
2,-1.12614276102,0,0,-0.00570092474329,0,0
3,0.250467817125,0,0,0.103033645915,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491301568173,0,0,-0.000987216805045,0,0
2,-1.12702871194,0,0,-0.00570991723389,0,0
3,0.250626630343,0,0,0.103082826351,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490160606004,0,0,-0.000729827654394,0,0
2,-1.13054242209,0,0,-0.00569920902623,0,0
3,0.253884729002,0,0,0.103337173768,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484883748432,0,0,0.000764167794203,0,0
2,-1.14257790671,0,0,-0.005568255356,0,0
3,0.268763306431,0,0,0.103943291274,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491865017345,0,0,-0.00101256705397,0,0
2,-1.13613078157,0,0,-0.00575444283519,0,0
3,0.254404381684,0,0,0.104365787216,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.502934134701,0,0,-0.027654167037,0,0
2,-1.09241635069,0,0,-0.00838519324655,0,0
3,0.0746743439625,0,0,0.1115192049,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.479550954616,0,0,-0.0542319935838,0,0
2,-0.963010511893,0,0,-0.00982040678052,0,0
3,-0.14519633346,-0,-0,0.113150063113,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.450814102106,0,0,-0.0807429345912,0,0
2,-0.795818325169,0,0,-0.0105111287857,0,0
3,-0.378886601264,-0,-0,0.11432775242,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403651596883,0,0,-0.107243367799,0,0
2,-0.619140188734,0,0,-0.0110164029282,0,0
3,-0.612077635884,-0,-0,0.113068364259,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.36627427229,0,0,-0.133700264598,0,0
2,-0.418914198038,0,0,-0.011100141022,0,0
3,-0.855786019447,-0,-0,0.109726293113,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.304398271291,0,0,-0.160120904062,0,0
2,-0.203662053194,0,0,-0.0107875911065,0,0
3,-1.10190487194,-0,-0,0.104515850237,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.209103804129,0,0,-0.186519327205,0,0
2,0.0274396468217,0,0,-0.0100340635771,0,0
3,-1.34582311604,-0,-0,0.0975288299187,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0749846878325,0,0,-0.212893305303,0,0
2,0.309065059628,0,0,-0.00847788628373,0,0
3,-1.59380928204,-0,-0,0.08606350654,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0412331482308,0,0,-0.239277730972,0,0
2,0.533514710669,0,0,-0.00717346282582,0,0
3,-1.84211516417,-0,-0,0.0777779321865,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.125402125051,0,0,-0.265675904173,0,0
2,0.699253860418,0,0,-0.00656513923665,0,0
3,-2.09224072453,-0,-0,0.0725968747616,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.210030912775,0,0,-0.292095306151,0,0
2,0.8475579611,0,0,-0.00606742458551,0,0
3,-2.34292390059,-0,-0,0.0688158728958,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.298972687225,0,0,-0.318521958737,0,0
2,0.987459995473,0,0,-0.00550576546507,0,0
3,-2.59336882607,-0,-0,0.0639983707917,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.378567210752,0,0,-0.34495555189,0,0
2,1.12389992048,0,0,-0.00479957276287,0,0
3,-2.84629635716,-0,-0,0.0587491674239,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.452932709657,0,0,-0.37139862095,0,0
2,1.27267610596,0,0,-0.0040502836657,0,0
3,-3.10328450674,-0,-0,0.0532368135291,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.506522406108,0,0,-0.397873251498,0,0
2,1.4264176738,0,0,-0.00467085256418,0,0
3,-3.36528931359,-0,-0,0.0517157728997,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.579504380688,0,0,-0.424344940464,0,0
2,1.58864495758,0,0,-0.00304360679854,0,0
3,-3.62218053182,-0,-0,0.0420982821035,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.67530192764,0,0,-0.450847963867,0,0
2,1.76027711568,0,0,-0.00205680870527,0,0
3,-3.88345344309,-0,-0,0.0362158728663,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.730963023158,0,0,-0.477354163617,0,0
2,1.94395718604,0,0,-0.000688548312589,0,0
3,-4.1539694007,-0,-0,0.0308171466279,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.798960498862,0,0,-0.503869525112,0,0
2,2.13439012482,0,0,0.00100081627015,0,0
3,-4.42036876732,-0,-0,0.0256659604771,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.820351179552,0,0,-0.503886808814,0,0
2,2.15944507135,0,0,0.00295038021717,0,0
3,-4.43344870545,-0,-0,0.0187095726999,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.884341518342,0,0,-0.50333532799,0,0
2,2.42489989876,0,0,0.004722489253,0,0
3,-4.32086004352,-0,-0,-0.0464709871112,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.33992479564,0,0,-0.502667312126,0,0
2,2.69754932552,0,0,0.0207747581336,0,0
3,-4.14327930276,-0,-0,-0.147138580292,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.38957987023,0,0,-0.502208336793,0,0
2,2.91394922876,0,0,0.0200512751591,0,0
3,-4.11054292861,-0,-0,-0.175408448176,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 31
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.79413242176,0,0,-0.501604593916,0,0
2,3.04218458507,0,0,0.0309073486352,0,0
3,-4.01206530116,-0,-0,-0.254505493479,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 32
*Time  = 2.2983871
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.21323687978,0,0,-0.500737516504,0,0
2,3.21427758443,0,0,0.0546370961668,0,0
3,-3.93811005044,-0,-0,-0.340903397863,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 33
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.69281026456,0,0,-0.500969516373,0,0
2,3.19833143244,0,0,0.0434426145073,0,0
3,-4.14887306693,-0,-0,-0.275108463047,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 34
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.27522804919,0,0,-0.500148374032,0,0
2,3.31257632053,0,0,0.0564635229633,0,0
3,-3.97732665909,-0,-0,-0.358673085782,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 35
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.2790011615,0,0,-0.499365784209,0,0
2,3.45663758414,0,0,0.0598646811686,0,0
3,-3.98553348164,-0,-0,-0.375024705444,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 36
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.39143098775,0,0,-0.498493217778,0,0
2,3.56870664226,0,0,0.0641390292146,0,0
3,-3.97029155588,-0,-0,-0.394760364121,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 37
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.48401687154,0,0,-0.497597404896,0,0
2,3.68316344072,0,0,0.0681040173364,0,0
3,-3.95550315121,-0,-0,-0.411355795584,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_-5_prestrain_-15_rc_data.txt
*Step  = 38
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.56768643566,0,0,-0.496727528307,0,0
2,3.79209227678,0,0,0.071774480395,0,0
3,-3.94098260049,-0,-0,-0.425897708625,-0,-0
