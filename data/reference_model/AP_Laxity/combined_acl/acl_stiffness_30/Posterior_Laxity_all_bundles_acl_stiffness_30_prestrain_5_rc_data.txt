File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.552929381841,0,0,0.000101325436559,0,0
2,0.0950058512766,0,0,-0.00245758723644,0,0
3,-0.0507794659245,-0,-0,0.00444966633406,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.610433384996,0,0,0.000139960606464,0,0
2,0.0832566681989,0,0,-0.00272312092536,0,0
3,-0.0797765087607,-0,-0,0.0065476838274,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 3
*Time  = 0.00031
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.634978192007,0,0,0.000163710111834,0,0
2,0.0741694267149,0,0,-0.00283617400208,0,0
3,-0.097096216332,-0,-0,0.00795375483453,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 4
*Time  = 0.00156
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.648989913851,0,0,0.000176054033013,0,0
2,0.0681437204003,0,0,-0.00292125117138,0,0
3,-0.105771585853,-0,-0,0.00854117544483,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 5
*Time  = 0.00781
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.661975544701,0,0,0.000174010877338,0,0
2,0.0664923574474,0,0,-0.00297355311098,0,0
3,-0.106909796005,-0,-0,0.00881117881956,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 6
*Time  = 0.03906
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.679858619969,0,0,0.000120459567189,0,0
2,0.0574698171663,0,0,-0.00287506997729,0,0
3,-0.0870671780646,-0,-0,0.0101935034721,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 7
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.663668113973,0,0,0.000105142326268,0,0
2,-0.0151442004113,0,0,-0.00283873131226,0,0
3,-0.0764164140087,-0,-0,0.01377982713,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 8
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.587264469758,0,0,9.95845108084e-06,0,0
2,-0.349419792539,0,0,-0.00289878713763,0,0
3,0.02686615824,0,0,0.0342462260608,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 9
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.544815824276,0,0,-4.99432728521e-05,0,0
2,-0.542245442555,0,0,-0.00330692622054,0,0
3,0.0754937912944,0,0,0.0470205007463,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 10
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.536186640905,0,0,-0.000105657634725,0,0
2,-0.643529069206,0,0,-0.00371116161661,0,0
3,0.100188761048,0,0,0.0545402964123,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 11
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.512098004429,0,0,-0.000160025638578,0,0
2,-0.717640346512,0,0,-0.00399148384701,0,0
3,0.121618216039,0,0,0.0608058417312,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 12
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.515508847715,0,0,-0.000213094263506,0,0
2,-0.766383285333,0,0,-0.00418058413815,0,0
3,0.133976007527,0,0,0.0649874900565,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 13
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.512152670327,0,0,-0.000267097172515,0,0
2,-0.813115440225,0,0,-0.00438770319155,0,0
3,0.147535794717,0,0,0.0693135407943,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 14
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.482258243498,0,0,-0.000327250057728,0,0
2,-0.850517554423,0,0,-0.00460424549466,0,0
3,0.165310967531,0,0,0.0739685339331,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 15
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.498468571324,0,0,-0.000379244257058,0,0
2,-0.883345226971,0,0,-0.00470070105366,0,0
3,0.173144410961,0,0,0.0768152289957,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 16
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.503235240947,0,0,-0.00043443530263,0,0
2,-0.914557422859,0,0,-0.00486023490159,0,0
3,0.182772050099,0,0,0.0799004836942,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 17
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.494314199799,0,0,-0.000489718734875,0,0
2,-0.946280598831,0,0,-0.00496529882386,0,0
3,0.192844789229,0,0,0.0831295949604,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 18
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.493519656768,0,0,-0.000544981159147,0,0
2,-0.974254643066,0,0,-0.00508371295379,0,0
3,0.201978231459,0,0,0.0860333559575,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 19
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490588924604,0,0,-0.000600191063721,0,0
2,-1.00072636559,0,0,-0.00519019123752,0,0
3,0.210819277263,0,0,0.0888285441553,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 20
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.498176120514,0,0,-0.000656888063457,0,0
2,-1.02320543742,0,0,-0.00533237775823,0,0
3,0.218515183997,0,0,0.0912890343652,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 21
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.489819086466,0,0,-0.000714235188714,0,0
2,-1.04730379557,0,0,-0.0054155752659,0,0
3,0.227040613075,0,0,0.0939396586992,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 22
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.486636135097,0,0,-0.00077114994165,0,0
2,-1.07050057405,0,0,-0.00548178952893,0,0
3,0.23469660432,0,0,0.0963012045167,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 23
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483842850346,0,0,-0.000827618741675,0,0
2,-1.0930120026,0,0,-0.00554868697553,0,0
3,0.242260329735,0,0,0.0986101208266,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 24
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.448110570197,0,0,-0.000888446151758,0,0
2,-1.10832949481,0,0,-0.00569353356193,0,0
3,0.256770599531,0,0,0.101733453069,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 25
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465526865568,0,0,-0.000941277439767,0,0
2,-1.12429474676,0,0,-0.0056591306743,0,0
3,0.258707412582,0,0,0.103017623909,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 26
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.464411257089,0,0,-0.000994291681725,0,0
2,-1.14604503182,0,0,-0.00570628725099,0,0
3,0.265489589634,0,0,0.105197198399,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483909573523,0,0,-0.000996052568567,0,0
2,-1.12501666682,0,0,-0.00570751062579,0,0
3,0.251832250263,0,0,0.103223527556,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.488450232932,0,0,-0.000997600310525,0,0
2,-1.12547054545,0,0,-0.00570160195527,0,0
3,0.250832447571,0,0,0.103066440827,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490118397649,0,0,-0.00099804488373,0,0
2,-1.1262243514,0,0,-0.00569992407433,0,0
3,0.250458464425,0,0,0.103031597456,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491296584633,0,0,-0.000987158876483,0,0
2,-1.1271015239,0,0,-0.00570909198739,0,0
3,0.25061334384,0,0,0.103080749195,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490219241976,0,0,-0.000729794100166,0,0
2,-1.1306574953,0,0,-0.00569991469609,0,0
3,0.253903608336,0,0,0.103343871633,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484875499477,0,0,0.000764202358592,0,0
2,-1.14268270816,0,0,-0.00556902977378,0,0
3,0.268776037756,0,0,0.10395195394,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491831942391,0,0,-0.00101253662376,0,0
2,-1.13621898396,0,0,-0.00575407050151,0,0
3,0.254396462485,0,0,0.10436962476,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.502940892586,0,0,-0.0276542604024,0,0
2,-1.09254217808,0,0,-0.0083859466675,0,0
3,0.0747093043204,0,0,0.111534335921,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.479581321872,0,0,-0.0542321240124,0,0
2,-0.963007056382,0,0,-0.00981954897517,0,0
3,-0.145251354975,-0,-0,0.113160110551,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.451123017723,0,0,-0.0807427219828,0,0
2,-0.796037908109,0,0,-0.0105150327734,0,0
3,-0.378810424271,-0,-0,0.114337406088,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403783212443,0,0,-0.10724300451,0,0
2,-0.619152139839,0,0,-0.0110189572151,0,0
3,-0.612089406493,-0,-0,0.113066549673,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.366344138883,0,0,-0.133700355165,0,0
2,-0.41898457857,0,0,-0.0111043216706,0,0
3,-0.855738053489,-0,-0,0.109744414783,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.30446161262,0,0,-0.160121254166,0,0
2,-0.203915432954,0,0,-0.0107914264669,0,0
3,-1.10186699718,-0,-0,0.104556149433,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.210467363535,0,0,-0.186519952237,0,0
2,0.0263503412397,0,0,-0.0100642578847,0,0
3,-1.34568176766,-0,-0,0.097664956666,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0749543760397,0,0,-0.212893802727,0,0
2,0.307592432013,0,0,-0.00847025192471,0,0
3,-1.59401002045,-0,-0,0.0861375305638,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0250953911087,0,0,-0.239290738506,0,0
2,0.493071834608,0,0,-0.00744184522918,0,0
3,-1.84221441605,-0,-0,0.0801402503346,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.126238012506,0,0,-0.265698092973,0,0
2,0.643397317269,0,0,-0.00687602541978,0,0
3,-2.09045119062,-0,-0,0.0755521747858,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.18616068928,0,0,-0.292132830563,0,0
2,0.765666473376,0,0,-0.00666938115762,0,0
3,-2.34183625129,-0,-0,0.0735235600407,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.272482327982,0,0,-0.318571363631,0,0
2,0.888370848575,0,0,-0.00617592797176,0,0
3,-2.5930207713,-0,-0,0.0696952556815,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.338511537592,0,0,-0.345019935987,0,0
2,1.01271959854,0,0,-0.00563415942372,0,0
3,-2.84669264045,-0,-0,0.0657183863351,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.399551636892,0,0,-0.371485585287,0,0
2,1.14380108868,0,0,-0.00506585726674,0,0
3,-3.10386927778,-0,-0,0.0614994025612,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.450088975708,0,0,-0.397960020654,0,0
2,1.2837194717,0,0,-0.00435527634727,0,0
3,-3.36362837496,-0,-0,0.0564528706767,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.497437292107,0,0,-0.424464440396,0,0
2,1.43539525391,0,0,-0.00349098710815,0,0
3,-3.62701104283,-0,-0,0.0518762780352,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.536633807306,0,0,-0.451006672927,0,0
2,1.59466422233,0,0,-0.00393459352112,0,0
3,-3.89760939926,-0,-0,0.0519815329387,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.58380401339,0,0,-0.477547548454,0,0
2,1.76375133718,0,0,-0.00270988717223,0,0
3,-4.16529923729,-0,-0,0.0488896089875,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.66197164359,0,0,-0.50406071899,0,0
2,1.9405627197,0,0,0.000567634253534,0,0
3,-4.42694399748,-0,-0,0.0388724580113,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.629601244736,0,0,-0.504129954672,0,0
2,1.95951152308,0,0,-5.39811639365e-05,0,0
3,-4.45660222176,-0,-0,0.04303600063,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.706075062016,0,0,-0.503633694082,0,0
2,2.17851716232,0,0,0.00465792382506,0,0
3,-4.34614336138,-0,-0,-0.0220806304731,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.82314176706,0,0,-0.503042128248,0,0
2,2.4109108239,0,0,0.00402425915726,0,0
3,-4.2430842001,-0,-0,-0.0685990334444,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.07873851308,0,0,-0.502559279472,0,0
2,2.56259357374,0,0,0.0101926840525,0,0
3,-4.14549946938,-0,-0,-0.120776827099,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 31
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.3232036944,0,0,-0.50211982763,0,0
2,2.67612176347,0,0,0.0185504386493,0,0
3,-4.0767742673,-0,-0,-0.172784345397,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 32
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.69797211852,0,0,-0.501468973811,0,0
2,2.77225097746,0,0,0.0304482291537,0,0
3,-3.98530364658,-0,-0,-0.249556785674,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 33
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.86908322204,0,0,-0.500954411258,0,0
2,2.83543306666,0,0,0.0383621012985,0,0
3,-3.98521594193,-0,-0,-0.288930368739,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 34
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.10750378004,0,0,-0.500150184092,0,0
2,2.90351917148,0,0,0.0528273513974,0,0
3,-3.96469811644,-0,-0,-0.336294393179,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 35
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.11197622805,0,0,-0.499400546215,0,0
2,2.98178833457,0,0,0.0563621049018,0,0
3,-3.9943148241,-0,-0,-0.350996445768,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 36
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.19867346765,0,0,-0.498579753266,0,0
2,3.04533990279,0,0,0.0599939461047,0,0
3,-3.99244411091,-0,-0,-0.369248667878,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_5_rc_data.txt
*Step  = 37
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.27123096758,0,0,-0.49775938024,0,0
2,3.10755120824,0,0,0.0632347114842,0,0
3,-3.99177284276,-0,-0,-0.384250403784,-0,-0
