File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.553529137772,0,0,0.000101198392171,0,0
2,0.0981104284803,0,0,-0.00246337104769,0,0
3,-0.0511659747373,-0,-0,0.00429940714473,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.611078927094,0,0,0.000139903610945,0,0
2,0.0872905214072,0,0,-0.00272953074476,0,0
3,-0.0802806985892,-0,-0,0.00635890737021,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 3
*Time  = 0.00031
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.635767624875,0,0,0.000163585787024,0,0
2,0.077900229854,0,0,-0.00284326528266,0,0
3,-0.0975028506384,-0,-0,0.00777707646478,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 4
*Time  = 0.00156
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.649638372996,0,0,0.000175785761759,0,0
2,0.0716955610677,0,0,-0.00292535384041,0,0
3,-0.106197123863,-0,-0,0.00837968568701,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 5
*Time  = 0.00781
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.662458853898,0,0,0.000173875590246,0,0
2,0.0704220182461,0,0,-0.00297802568029,0,0
3,-0.107427107815,-0,-0,0.00863976945655,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 6
*Time  = 0.03906
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.679911459273,0,0,0.000120378279658,0,0
2,0.0586158357368,0,0,-0.0028746416438,0,0
3,-0.0871435021487,-0,-0,0.0101419170519,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 7
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.663513920643,0,0,0.00010509620064,0,0
2,-0.0151233142813,0,0,-0.00283768594543,0,0
3,-0.0763826793681,-0,-0,0.0137761114818,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 8
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.587236189795,0,0,9.88845183176e-06,0,0
2,-0.349504000613,0,0,-0.00289960608997,0,0
3,0.0269178054641,0,0,0.0342524667848,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 9
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.544683896605,0,0,-4.9902239707e-05,0,0
2,-0.542420080493,0,0,-0.00330748496536,0,0
3,0.0755448456564,0,0,0.0470327749327,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 10
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.536497755602,0,0,-0.000105628857412,0,0
2,-0.643006229777,0,0,-0.00371192966515,0,0
3,0.100048064442,0,0,0.0545065533005,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 11
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511943282245,0,0,-0.000160120253694,0,0
2,-0.717677113657,0,0,-0.00399145255677,0,0
3,0.121693128573,0,0,0.0608096256982,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 12
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.515986398091,0,0,-0.000213053645246,0,0
2,-0.765830271553,0,0,-0.00418239577912,0,0
3,0.133827002081,0,0,0.0649472593446,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 13
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511481796004,0,0,-0.000267114150318,0,0
2,-0.813460394603,0,0,-0.00438624200281,0,0
3,0.147681758819,0,0,0.0693447743868,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 14
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.48265758815,0,0,-0.000327177698146,0,0
2,-0.850603694397,0,0,-0.00460400485596,0,0
3,0.165264681375,0,0,0.0739656089364,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 15
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.499323550299,0,0,-0.000379324696684,0,0
2,-0.882416400215,0,0,-0.00470287767626,0,0
3,0.172795001059,0,0,0.0767388403575,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 16
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.499587621779,0,0,-0.000434795911502,0,0
2,-0.915520591882,0,0,-0.00483711013552,0,0
3,0.1828163929,0,0,0.0799492349533,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 17
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.495357620146,0,0,-0.000490387635168,0,0
2,-0.945519873444,0,0,-0.00495977256736,0,0
3,0.19239839365,0,0,0.0830184275253,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 18
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.493381851879,0,0,-0.000545520344637,0,0
2,-0.974224908731,0,0,-0.00508354917551,0,0
3,0.202041803905,0,0,0.08602694895,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 19
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.492135536072,0,0,-0.000600557398323,0,0
2,-0.999608213853,0,0,-0.00519667798343,0,0
3,0.21051034716,0,0,0.0887463893209,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 20
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.498196991115,0,0,-0.000657178800382,0,0
2,-1.0229876096,0,0,-0.00533601727583,0,0
3,0.21867977393,0,0,0.0912969076507,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 21
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.489239921787,0,0,-0.000714415836311,0,0
2,-1.04742865183,0,0,-0.00541699694381,0,0
3,0.227210410509,0,0,0.0939716256879,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 22
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.485200604406,0,0,-0.000771106266197,0,0
2,-1.07145359374,0,0,-0.00547728550932,0,0
3,0.235073292029,0,0,0.0963859163203,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 23
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483944215118,0,0,-0.000827576583291,0,0
2,-1.09322370824,0,0,-0.00554938080076,0,0
3,0.242284532032,0,0,0.0986289313007,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 24
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.448574591079,0,0,-0.00088853355607,0,0
2,-1.10803673919,0,0,-0.00569680405435,0,0
3,0.256689193284,0,0,0.101720676535,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 25
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465563162963,0,0,-0.000941395995004,0,0
2,-1.12416888578,0,0,-0.00566101969633,0,0
3,0.258725707477,0,0,0.103021413785,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 26
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465393230989,0,0,-0.000994744186412,0,0
2,-1.14508044614,0,0,-0.00571118453504,0,0
3,0.265242974519,0,0,0.105136916472,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.482748046305,0,0,-0.000996036495672,0,0
2,-1.12552489456,0,0,-0.00570630273879,0,0
3,0.252168160692,0,0,0.103285678171,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.488402562649,0,0,-0.000997571277907,0,0
2,-1.12554585075,0,0,-0.00570173435558,0,0
3,0.250872861798,0,0,0.103079243484,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490127237167,0,0,-0.000998070612551,0,0
2,-1.12619463537,0,0,-0.00570084194128,0,0
3,0.250465942288,0,0,0.103037276612,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491288565574,0,0,-0.000987209181588,0,0
2,-1.12707904787,0,0,-0.00571003457011,0,0
3,0.25063039899,0,0,0.103087138825,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490141506797,0,0,-0.000729827161784,0,0
2,-1.130576517,0,0,-0.00569939900883,0,0
3,0.253885939064,0,0,0.103340431889,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484816597678,0,0,0.000764167409113,0,0
2,-1.14261947308,0,0,-0.00556864012253,0,0
3,0.268769868448,0,0,0.103948323202,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491880671813,0,0,-0.0010123975328,0,0
2,-1.13612771253,0,0,-0.00575571050116,0,0
3,0.254410863395,0,0,0.104371035195,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.502956838616,0,0,-0.0276542178188,0,0
2,-1.09240640321,0,0,-0.00838524908485,0,0
3,0.0746634513242,0,0,0.111519212603,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.47950132547,0,0,-0.0542320376352,0,0
2,-0.963008967731,0,0,-0.00982044173797,0,0
3,-0.1451988241,-0,-0,0.113152812052,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.450844714074,0,0,-0.0807429637081,0,0
2,-0.795792683441,0,0,-0.0105115756491,0,0
3,-0.378893631154,-0,-0,0.114327091587,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403730471247,0,0,-0.107243354473,0,0
2,-0.619160908123,0,0,-0.0110165430496,0,0
3,-0.61208488187,-0,-0,0.113067249033,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.366215942878,0,0,-0.133700295061,0,0
2,-0.418941494488,0,0,-0.0111000062593,0,0
3,-0.855776429409,-0,-0,0.109729041406,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.304423172342,0,0,-0.160120927352,0,0
2,-0.203689195869,0,0,-0.0107878437413,0,0
3,-1.10190775805,-0,-0,0.10451835573,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.209118322285,0,0,-0.186519334841,0,0
2,0.0274153658564,0,0,-0.0100346668092,0,0
3,-1.3458283786,-0,-0,0.0975321848393,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0753913848624,0,0,-0.212893626468,0,0
2,0.307948232648,0,0,-0.00848406396026,0,0
3,-1.59380634885,-0,-0,0.0861268259817,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0297105936697,0,0,-0.239286667539,0,0
2,0.505056014802,0,0,-0.00739015458262,0,0
3,-1.84208121731,-0,-0,0.0794433644999,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.11082658253,0,0,-0.265691238968,0,0
2,0.658173175421,0,0,-0.00683680114629,0,0
3,-2.09308960768,-0,-0,0.0748939124831,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.215881563399,0,0,-0.292123284323,0,0
2,0.786970033577,0,0,-0.00639195760905,0,0
3,-2.33909832986,-0,-0,0.0717337857793,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.285709036659,0,0,-0.318557909068,0,0
2,0.917104528585,0,0,-0.00594575988509,0,0
3,-2.59233064924,-0,-0,0.0677500905031,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.353433794579,0,0,-0.345002525908,0,0
2,1.0421525416,0,0,-0.0053792523298,0,0
3,-2.84579144305,-0,-0,0.0636476102804,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.418283661992,0,0,-0.371464081976,0,0
2,1.17801707195,0,0,-0.00480328059305,0,0
3,-3.1027853413,-0,-0,0.0591696054525,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.46329204134,0,0,-0.397952642411,0,0
2,1.32037439508,0,0,-0.00551407084127,0,0
3,-3.3654462942,-0,-0,0.0585336950591,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.526118156232,0,0,-0.424428632421,0,0
2,1.47722076385,0,0,-0.002949601253,0,0
3,-3.62265359201,-0,-0,0.0468042313758,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.588090131423,0,0,-0.450943264262,0,0
2,1.63854088033,0,0,-0.00171895602684,0,0
3,-3.88926599727,-0,-0,0.0423203312213,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.613501071127,0,0,-0.477502332952,0,0
2,1.81173039561,0,0,-0.00220257739334,0,0
3,-4.16572382569,-0,-0,0.0450779995746,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.702415407638,0,0,-0.504004972778,0,0
2,1.99172450962,0,0,0.00122883999209,0,0
3,-4.42481405064,-0,-0,0.0345658369619,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.680225242624,0,0,-0.50407068551,0,0
2,2.01192686769,0,0,0.000664354013773,0,0
3,-4.45175016887,-0,-0,0.0374597168433,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.642875246927,0,0,-0.503552211058,0,0
2,2.24419140047,0,0,-0.0020313755688,0,0
3,-4.37706498131,-0,-0,-0.00385486566857,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.941230526856,0,0,-0.500708644034,0,0
2,2.43868047316,0,0,0.0191545914198,0,0
3,-4.26411444766,-0,-0,-0.0698217237568,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.1584970469,0,0,-0.500742104939,0,0
2,2.62376280861,0,0,0.0217049129483,0,0
3,-4.146159968,-0,-0,-0.128245086354,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 31
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.28141650659,0,0,-0.500635734202,0,0
2,2.76413793926,0,0,0.0239496158421,0,0
3,-4.09780300314,-0,-0,-0.162848413482,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 32
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.97724646491,0,0,-0.50107499344,0,0
2,2.84447144179,0,0,0.0479727304118,0,0
3,-3.95601530155,-0,-0,-0.284536934459,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 33
*Time  = 2.34677419
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.06008484776,0,0,-0.500896139167,0,0
2,2.92102946837,0,0,0.0574577955164,0,0
3,-4.00323171911,-0,-0,-0.31360476079,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 34
*Time  = 2.34989594
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.81832850895,0,0,-0.500320821903,0,0
2,3.50937247121,0,0,-0.0193980635392,0,0
3,-4.50034097126,-0,-0,0.0833407797349,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 35
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.73758348453,0,0,-0.500263899354,0,0
2,3.50418341406,0,0,-0.0179559202333,0,0
3,-4.4918772779,-0,-0,0.0810522747646,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 36
*Time  = 2.35822655
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.7644399237,0,0,-0.500211544836,0,0
2,3.54796383156,0,0,-0.0178594222752,0,0
3,-4.47176323743,-0,-0,0.0811701775842,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 37
*Time  = 2.39935931
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.98408303892,0,0,-0.500115121342,0,0
2,3.77326219108,0,0,-0.0186535776336,0,0
3,-4.3792332908,-0,-0,0.0861759603492,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 38
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.84149716931,0,0,-0.500054292356,0,0
2,3.69347257343,0,0,-0.018620068385,0,0
3,-4.43684866975,-0,-0,0.0851109129393,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 39
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-2.18121473392,0,0,-0.499980000502,0,0
2,3.96961782626,0,0,-0.0205771910669,0,0
3,-4.32237941663,-0,-0,0.0930328872057,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 40
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-2.28651804156,0,0,-0.499823647876,0,0
2,4.13611110299,0,0,-0.0215936658664,0,0
3,-4.27039715612,-0,-0,0.0980391256941,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-5_rc_data.txt
*Step  = 41
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-2.43152655754,0,0,-0.499686074466,0,0
2,4.27908703396,0,0,-0.0234435405346,0,0
3,-4.23487025842,-0,-0,0.104134611272,0,0
