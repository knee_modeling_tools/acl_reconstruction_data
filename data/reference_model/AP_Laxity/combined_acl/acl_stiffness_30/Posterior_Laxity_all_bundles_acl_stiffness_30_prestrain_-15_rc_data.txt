File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.554241982452,0,0,0.000101147568844,0,0
2,0.102173143457,0,0,-0.002470384509,0,0
3,-0.0516757439102,-0,-0,0.00410920882597,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.611984802158,0,0,0.000139854872238,0,0
2,0.0916248258234,0,0,-0.00273765622173,0,0
3,-0.0807669225027,-0,-0,0.00615508099095,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 3
*Time  = 0.00031
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.636521109839,0,0,0.00016333605878,0,0
2,0.0821425254631,0,0,-0.00285086998389,0,0
3,-0.097991495789,-0,-0,0.00756991782665,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 4
*Time  = 0.00156
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.650411356032,0,0,0.000175496558091,0,0
2,0.0760637797138,0,0,-0.00293119121799,0,0
3,-0.106733767632,-0,-0,0.00818371913463,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 5
*Time  = 0.00781
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.663141826569,0,0,0.000173612665799,0,0
2,0.0744469179502,0,0,-0.00298297938241,0,0
3,-0.107907879698,-0,-0,0.008461451361,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 6
*Time  = 0.03906
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.679854566191,0,0,0.000120294164301,0,0
2,0.0599391063651,0,0,-0.00287418482608,0,0
3,-0.0872585771998,-0,-0,0.0100823736517,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 7
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.661486759069,0,0,0.000105575151654,0,0
2,-0.0149648960431,0,0,-0.00282048280733,0,0
3,-0.0766304459157,-0,-0,0.0137153882846,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 8
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.588455584995,0,0,9.91586975797e-06,0,0
2,-0.349755789646,0,0,-0.00290834946741,0,0
3,0.0271212775999,0,0,0.0342789259366,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 9
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.544791160946,0,0,-4.99095245539e-05,0,0
2,-0.542318101739,0,0,-0.00331057743621,0,0
3,0.0755312899409,0,0,0.0470324080093,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 10
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.536498914704,0,0,-0.000105611315998,0,0
2,-0.642928663848,0,0,-0.00371242884894,0,0
3,0.100038618025,0,0,0.0545028357733,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 11
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511921839915,0,0,-0.000160102392045,0,0
2,-0.71753418151,0,0,-0.00399017508525,0,0
3,0.121640563554,0,0,0.0607990393,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 12
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.515648970193,0,0,-0.000212916151536,0,0
2,-0.766355133594,0,0,-0.0041824515954,0,0
3,0.133975951817,0,0,0.0649900028764,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 13
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.51255594962,0,0,-0.000267081309216,0,0
2,-0.812570577632,0,0,-0.00438754337577,0,0
3,0.147369618464,0,0,0.0692704463018,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 14
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.481449971262,0,0,-0.000327278301136,0,0
2,-0.851179093285,0,0,-0.00460317999697,0,0
3,0.165570500948,0,0,0.0740240035369,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 15
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.499166830453,0,0,-0.000379352008244,0,0
2,-0.882743994502,0,0,-0.00469920712851,0,0
3,0.172867268045,0,0,0.0767502452982,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 16
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.500676541331,0,0,-0.000434858672236,0,0
2,-0.914583962086,0,0,-0.00483894516562,0,0
3,0.182446109204,0,0,0.0798682647994,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 17
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.495583152464,0,0,-0.000490502942378,0,0
2,-0.94520543433,0,0,-0.00496113217641,0,0
3,0.192380960127,0,0,0.0830021855413,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 18
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.492987172712,0,0,-0.000545510160213,0,0
2,-0.974351362534,0,0,-0.00508176942629,0,0
3,0.202116431709,0,0,0.0860366822773,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 19
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.49206212534,0,0,-0.000600531907561,0,0
2,-0.999652469355,0,0,-0.00519660625144,0,0
3,0.210536215015,0,0,0.0887504873909,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 20
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.497544743035,0,0,-0.000657191474147,0,0
2,-1.02325863225,0,0,-0.00533321537467,0,0
3,0.218735020808,0,0,0.0913156296662,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 21
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.489768836169,0,0,-0.000714375277887,0,0
2,-1.04734495732,0,0,-0.00541764496679,0,0
3,0.227160160821,0,0,0.093957583208,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 22
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.486624270366,0,0,-0.000771294046858,0,0
2,-1.07033215716,0,0,-0.00548246956417,0,0
3,0.234687424442,0,0,0.0962981149413,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 23
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483972357075,0,0,-0.000827740716959,0,0
2,-1.09285029945,0,0,-0.00555030013742,0,0
3,0.242268611172,0,0,0.0986078568614,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 24
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.448186004708,0,0,-0.000888504754135,0,0
2,-1.10819565084,0,0,-0.00569519755651,0,0
3,0.256799056457,0,0,0.101734966772,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 25
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465561579304,0,0,-0.000941351200147,0,0
2,-1.12416200388,0,0,-0.00566033971826,0,0
3,0.258725649388,0,0,0.1030181533,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 26
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.464420104324,0,0,-0.000994363212098,0,0
2,-1.14593092478,0,0,-0.00570744972964,0,0
3,0.265510675102,0,0,0.105198896377,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483981979431,0,0,-0.000996136851687,0,0
2,-1.12489304445,0,0,-0.00570882565751,0,0
3,0.251847624597,0,0,0.103223698235,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.488458870926,0,0,-0.000997692289674,0,0
2,-1.12535020295,0,0,-0.0057027145435,0,0
3,0.250849787327,0,0,0.103067324188,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.49013614143,0,0,-0.000998150883386,0,0
2,-1.12609073795,0,0,-0.00570085466136,0,0
3,0.250464581358,0,0,0.103030116064,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.49130836673,0,0,-0.000987271706002,0,0
2,-1.12698149755,0,0,-0.00570981781766,0,0
3,0.250622120491,0,0,0.103079385507,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490155452778,0,0,-0.000729874171338,0,0
2,-1.13051050745,0,0,-0.00569906341675,0,0
3,0.253882830182,0,0,0.103334736005,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484864001541,0,0,0.000764124654332,0,0
2,-1.14255135635,0,0,-0.00556818632811,0,0
3,0.268761084754,0,0,0.10394154736,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491867071398,0,0,-0.00101260775202,0,0
2,-1.13611227702,0,0,-0.00575429634724,0,0
3,0.254401930557,0,0,0.104363848697,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.502929589956,0,0,-0.0276541664731,0,0
2,-1.09244964238,0,0,-0.00838517100126,0,0
3,0.0746844235668,0,0,0.11152114006,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.47956200677,0,0,-0.0542319963198,0,0
2,-0.963039623979,0,0,-0.00982039846496,0,0
3,-0.145195152265,-0,-0,0.113151161154,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.450818613187,0,0,-0.0807429361444,0,0
2,-0.795860843442,0,0,-0.0105109461385,0,0
3,-0.37887991382,-0,-0,0.114328647941,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403633750554,0,0,-0.107243366474,0,0
2,-0.619164732132,0,0,-0.011016481107,0,0
3,-0.612076701935,-0,-0,0.113070200638,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.366251731514,0,0,-0.133700291597,0,0
2,-0.418953639493,0,0,-0.0111008543784,0,0
3,-0.855780570251,-0,-0,0.109729724723,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.304367442529,0,0,-0.160120937375,0,0
2,-0.203840978846,0,0,-0.0107873114157,0,0
3,-1.10189232702,-0,-0,0.104530918297,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.209252436463,0,0,-0.186519355238,0,0
2,0.0273718510756,0,0,-0.0100360452761,0,0
3,-1.34585564168,-0,-0,0.0975332420579,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0755664604392,0,0,-0.212893657654,0,0
2,0.307962555402,0,0,-0.00848594611579,0,0
3,-1.59387229527,-0,-0,0.0861408210213,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0344004269445,0,0,-0.239283144933,0,0
2,0.516377545043,0,0,-0.00730769284842,0,0
3,-1.84213461749,-0,-0,0.0788180769242,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.11528876508,0,0,-0.265684914178,0,0
2,0.674316520238,0,0,-0.00675417663167,0,0
3,-2.09275214227,-0,-0,0.074053464817,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.201801666883,0,0,-0.292112672712,0,0
2,0.810285003937,0,0,-0.00632286539447,0,0
3,-2.34209158508,-0,-0,0.0708840414126,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.296359394787,0,0,-0.318543542208,0,0
2,0.943825577485,0,0,-0.00575035880171,0,0
3,-2.59196178449,-0,-0,0.0663368715811,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.363506374874,0,0,-0.344983145388,0,0
2,1.07409460112,0,0,-0.00514565665965,0,0
3,-2.84598739145,-0,-0,0.0616397290354,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.444908086526,0,0,-0.371440327918,0,0
2,1.2151287932,0,0,-0.00447424708539,0,0
3,-3.10086595321,-0,-0,0.0565021968555,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.487722708984,0,0,-0.397921697291,0,0
2,1.36273198119,0,0,-0.00522121388135,0,0
3,-3.36435438539,-0,-0,0.0556529709563,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.548382204161,0,0,-0.424409587579,0,0
2,1.52105976307,0,0,-0.00379597371404,0,0
3,-3.62283215001,-0,-0,0.0473982921448,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.615932227094,0,0,-0.450916673656,0,0
2,1.6864293796,0,0,-0.00285867669276,0,0
3,-3.88822666262,-0,-0,0.0423831684798,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.669589641792,0,0,-0.477439616967,0,0
2,1.8633765272,0,0,-0.00155282651808,0,0
3,-4.15730560835,-0,-0,0.0377830520195,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.709301542137,0,0,-0.503970951513,0,0
2,2.04999929122,0,0,-0.000599570585471,0,0
3,-4.43059367468,-0,-0,0.0365852964068,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.74698338384,0,0,-0.503990284637,0,0
2,2.06831497581,0,0,0.0016571381482,0,0
3,-4.43945501239,-0,-0,0.0280024545892,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.752173682429,0,0,-0.503445402234,0,0
2,2.30839401267,0,0,-0.00132741500384,0,0
3,-4.35243829674,-0,-0,-0.0186690193292,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.26717814663,0,0,-0.500057559822,0,0
2,2.49208965227,0,0,0.0147114414802,0,0
3,-4.17256902139,-0,-0,-0.110051393839,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.20378803957,0,0,-0.499530971826,0,0
2,2.72450334127,0,0,0.0142947280633,0,0
3,-4.12280782176,-0,-0,-0.139381414803,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 31
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.58556130276,0,0,-0.498977665727,0,0
2,2.8451287846,0,0,0.0278373547369,0,0
3,-4.01593988329,-0,-0,-0.218784065618,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 32
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.05576755488,0,0,-0.498141541029,0,0
2,2.96574967455,0,0,0.0499279168686,0,0
3,-3.9305658378,-0,-0,-0.313210198788,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 33
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.01893007291,0,0,-0.497620323264,0,0
2,3.07189849455,0,0,0.0515006718363,0,0
3,-3.97951547641,-0,-0,-0.325181479265,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 34
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.15208497539,0,0,-0.496884850048,0,0
2,3.16428453029,0,0,0.0567051477717,0,0
3,-3.96853761562,-0,-0,-0.352587723622,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 35
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.24764891223,0,0,-0.496087282135,0,0
2,3.26044794963,0,0,0.0609754103446,0,0
3,-3.95995021092,-0,-0,-0.373156130803,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 36
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.32789102849,0,0,-0.495255857418,0,0
2,3.35451505074,0,0,0.0647735257718,0,0
3,-3.95220103046,-0,-0,-0.390332431561,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_30_prestrain_-15_rc_data.txt
*Step  = 37
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.40848021135,0,0,-0.494419396494,0,0
2,3.43743265813,0,0,0.0681561190678,0,0
3,-3.94657785332,-0,-0,-0.40507004727,-0,-0
