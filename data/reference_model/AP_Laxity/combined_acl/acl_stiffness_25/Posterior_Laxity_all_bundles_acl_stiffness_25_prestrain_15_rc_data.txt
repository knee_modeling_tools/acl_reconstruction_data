File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.552522461255,0,0,0.000101389559531,0,0
2,0.09295631788,0,0,-0.00245336539429,0,0
3,-0.0505338324037,-0,-0,0.00454521278601,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.609469039305,0,0,0.000139893169643,0,0
2,0.0804192830857,0,0,-0.00271357444916,0,0
3,-0.0795392310833,-0,-0,0.00666321265483,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 3
*Time  = 0.00011
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.634803830661,0,0,0.00016414737656,0,0
2,0.0713118258196,0,0,-0.0028331490995,0,0
3,-0.0968521932823,-0,-0,0.00808147950604,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 4
*Time  = 0.00036
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.647720011308,0,0,0.000178473085602,0,0
2,0.0647821904151,0,0,-0.00292036943389,0,0
3,-0.106260639426,-0,-0,0.00864429610759,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 5
*Time  = 0.00161
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.659001862935,0,0,0.000185872520659,0,0
2,0.0615720833407,0,0,-0.00299457753922,0,0
3,-0.110636306183,-0,-0,0.00881686944217,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 6
*Time  = 0.00786
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.667872833577,0,0,0.000180526451239,0,0
2,0.0611003129861,0,0,-0.00301559002414,0,0
3,-0.109936111706,-0,-0,0.00901289369426,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 7
*Time  = 0.03911
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.684303128103,0,0,0.000124499812635,0,0
2,0.0554715915473,0,0,-0.0029015199689,0,0
3,-0.0890869698832,-0,-0,0.0102570498932,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 8
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.665429659947,0,0,0.000109331681536,0,0
2,-0.0121916865251,0,0,-0.00283904626933,0,0
3,-0.0787471953778,-0,-0,0.0135581582361,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 9
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.589885216944,0,0,1.18868025782e-05,0,0
2,-0.348125465399,0,0,-0.00289705580851,0,0
3,0.0259540974411,0,0,0.0341343235995,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 10
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.546162452496,0,0,-4.84001657954e-05,0,0
2,-0.542833125906,0,0,-0.00330612768665,0,0
3,0.0750907121941,0,0,0.0470082715761,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 11
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.536873739223,0,0,-0.000104771850812,0,0
2,-0.643933867735,0,0,-0.00371308722705,0,0
3,0.0999079698567,0,0,0.0545229314389,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 12
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511904731337,0,0,-0.000159585028811,0,0
2,-0.718084219163,0,0,-0.00399193089966,0,0
3,0.121568269824,0,0,0.0608036443016,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 13
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.515869985404,0,0,-0.000212642860701,0,0
2,-0.766662402729,0,0,-0.00418285323863,0,0
3,0.133882678207,0,0,0.064975781565,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 14
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511919912602,0,0,-0.00026681266749,0,0
2,-0.81321737849,0,0,-0.00438655822563,0,0
3,0.147458712267,0,0,0.0692949748232,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 15
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.481489570892,0,0,-0.000327064012137,0,0
2,-0.851391323196,0,0,-0.00460304055138,0,0
3,0.165565782648,0,0,0.0740191669216,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 16
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.498908926378,0,0,-0.000379218285793,0,0
2,-0.8831409008,0,0,-0.00469749743653,0,0
3,0.172957201974,0,0,0.0767613538778,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 17
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.501017392158,0,0,-0.000434732172331,0,0
2,-0.914433359457,0,0,-0.00484030898395,0,0
3,0.182322732315,0,0,0.0798413483638,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 18
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.495334865163,0,0,-0.000490359226005,0,0
2,-0.945248801238,0,0,-0.00496005354573,0,0
3,0.192382187411,0,0,0.0829937737637,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 19
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.493282898054,0,0,-0.000545430285686,0,0
2,-0.973997612898,0,0,-0.00508341763259,0,0
3,0.201991575268,0,0,0.0860015270908,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 20
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490770178187,0,0,-0.000600409288816,0,0
2,-1.00040772699,0,0,-0.00519289351055,0,0
3,0.210813344034,0,0,0.0888043307332,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 21
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.49700848529,0,0,-0.000657025885818,0,0
2,-1.02383583175,0,0,-0.00533254783246,0,0
3,0.218845033912,0,0,0.0913516327702,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 22
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.489940612952,0,0,-0.000714356436571,0,0
2,-1.047359866,0,0,-0.00541740053399,0,0
3,0.227094103135,0,0,0.0939464185884,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 23
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.486253830337,0,0,-0.000771220900869,0,0
2,-1.07064760834,0,0,-0.00548127498823,0,0
3,0.234764309469,0,0,0.0963153405718,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 24
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483987308718,0,0,-0.000827625937837,0,0
2,-1.09296251428,0,0,-0.00554966450898,0,0
3,0.242263565017,0,0,0.0986072443424,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 25
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.44857027613,0,0,-0.000888454546665,0,0
2,-1.10794078169,0,0,-0.00569652254731,0,0
3,0.256675032794,0,0,0.101706042662,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 26
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465922280585,0,0,-0.000941169997794,0,0
2,-1.12425711557,0,0,-0.00566319969118,0,0
3,0.258785882836,0,0,0.103026895583,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 27
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.463600250487,0,0,-0.000994111569501,0,0
2,-1.14619179997,0,0,-0.00570434803159,0,0
3,0.265614123677,0,0,0.105219175398,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483963023281,0,0,-0.000995911519623,0,0
2,-1.12499838147,0,0,-0.0057071858773,0,0
3,0.251846865103,0,0,0.103219263213,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.4883986328,0,0,-0.000997495840199,0,0
2,-1.12542823752,0,0,-0.00570188996813,0,0
3,0.250849370781,0,0,0.103064447855,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490093654397,0,0,-0.000997956393676,0,0
2,-1.12614750247,0,0,-0.0057003420014,0,0
3,0.250463765702,0,0,0.103026790303,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491305520491,0,0,-0.000987065737465,0,0
2,-1.12703368486,0,0,-0.00570949453759,0,0
3,0.250620842015,0,0,0.103075913681,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490107788511,0,0,-0.000729679346997,0,0
2,-1.13054719425,0,0,-0.00569872955499,0,0
3,0.253880950663,0,0,0.103331103048,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484873557477,0,0,0.000764313534362,0,0
2,-1.14257088486,0,0,-0.00556779430006,0,0
3,0.268760297177,0,0,0.103936628769,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491819984386,0,0,-0.00101224445194,0,0
2,-1.13611508167,0,0,-0.0057549107865,0,0
3,0.254414095899,0,0,0.104363900376,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.502994859439,0,0,-0.0276539111112,0,0
2,-1.09240854682,0,0,-0.00838337267961,0,0
3,0.0746072604773,0,0,0.111502562159,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.479638235876,0,0,-0.0542316124667,0,0
2,-0.962995610697,0,0,-0.00982018285337,0,0
3,-0.145226890554,-0,-0,0.113145941278,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.451395731156,0,0,-0.0807423221556,0,0
2,-0.795911832323,0,0,-0.0105126410044,0,0
3,-0.378979544725,-0,-0,0.114325581381,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403695496092,0,0,-0.107243083573,0,0
2,-0.619283795227,0,0,-0.0110159322541,0,0
3,-0.612126153575,-0,-0,0.113070606293,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.365925368805,0,0,-0.133700377345,0,0
2,-0.418806101665,0,0,-0.0110982320309,0,0
3,-0.855787392174,-0,-0,0.10971444363,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.304446947334,0,0,-0.160120928882,0,0
2,-0.203706757715,0,0,-0.010786842838,0,0
3,-1.10190458768,-0,-0,0.10451049593,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.208948076986,0,0,-0.186519376947,0,0
2,0.0275708935578,0,0,-0.0100300172103,0,0
3,-1.34581363492,-0,-0,0.0975091389179,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0753604648611,0,0,-0.212893843102,0,0
2,0.307364399228,0,0,-0.0084835375405,0,0
3,-1.59381170113,-0,-0,0.0861360629348,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0243864259699,0,0,-0.23929255275,0,0
2,0.486476268671,0,0,-0.00749651575729,0,0
3,-1.84202939544,-0,-0,0.0803337951326,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.11927967212,0,0,-0.265702188806,0,0
2,0.63194841884,0,0,-0.00696653641019,0,0
3,-2.09072915544,-0,-0,0.0763334520304,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.184197479416,0,0,-0.292138572942,0,0
2,0.752013068233,0,0,-0.006704703728,0,0
3,-2.34179712688,-0,-0,0.0740657122412,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.263738973166,0,0,-0.318580486348,0,0
2,0.869619340362,0,0,-0.00632686889751,0,0
3,-2.59339173776,-0,-0,0.0707865163121,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.336778213905,0,0,-0.345033860038,0,0
2,0.992958699093,0,0,-0.0057703297611,0,0
3,-2.84575111993,-0,-0,0.066737488354,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.385039538913,0,0,-0.371500499258,0,0
2,1.12119401915,0,0,-0.00530639467175,0,0
3,-3.10498551874,-0,-0,0.0628037969992,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.433837128328,0,0,-0.397980205608,0,0
2,1.25846045007,0,0,-0.00466188730511,0,0
3,-3.36420993616,-0,-0,0.0582949017149,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.47961379705,0,0,-0.424490540476,0,0
2,1.40719715523,0,0,-0.00379214899892,0,0
3,-3.62774152795,-0,-0,0.0540842832892,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.511680806779,0,0,-0.451034968716,0,0
2,1.56470591275,0,0,-0.00411239687696,0,0
3,-3.90003291282,-0,-0,0.0546858632539,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.557980884287,0,0,-0.477582304589,0,0
2,1.73227667859,0,0,-0.00318908491718,0,0
3,-4.16719034056,-0,-0,0.0521280736945,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.626154310996,0,0,-0.504102807126,0,0
2,1.90503745715,0,0,5.63195378559e-05,0,0
3,-4.4307809322,-0,-0,0.0432414497891,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.595751166321,0,0,-0.504172887738,0,0
2,1.92300554,0,0,-0.000571630549535,0,0
3,-4.45969931962,-0,-0,0.0470342876653,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.495980300287,0,0,-0.503705803799,0,0
2,2.14289514546,0,0,-0.00154573355779,0,0
3,-4.40671503239,-0,-0,0.0141751326481,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.671946108197,0,0,-0.50306535712,0,0
2,2.38089859624,0,0,0.00335934906935,0,0
3,-4.25346507255,-0,-0,-0.0594262922073,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.01872351271,0,0,-0.502615705649,0,0
2,2.49849415901,0,0,0.00725968754964,0,0
3,-4.15419298456,-0,-0,-0.106884227782,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 31
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.26825525314,0,0,-0.502182290752,0,0
2,2.61114931926,0,0,0.0169997249793,0,0
3,-4.08173051859,-0,-0,-0.162296730616,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 32
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.67088779276,0,0,-0.50155149055,0,0
2,2.70441127689,0,0,0.0312824073479,0,0
3,-3.98487832852,-0,-0,-0.244883698766,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 33
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.80889489713,0,0,-0.501082633726,0,0
2,2.76471139487,0,0,0.0388498618272,0,0
3,-3.9940994024,-0,-0,-0.279904195317,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 34
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.04911574241,0,0,-0.500276976381,0,0
2,2.82188774402,0,0,0.0523991843351,0,0
3,-3.9730874,-0,-0,-0.328372526333,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 35
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.05151986623,0,0,-0.499613745044,0,0
2,2.89660103795,0,0,0.0546844529838,0,0
3,-3.99791322852,-0,-0,-0.341752450179,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 36
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.15394221909,0,0,-0.498793152849,0,0
2,2.95131090028,0,0,0.0585616708722,0,0
3,-3.9952891652,-0,-0,-0.361660116545,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_25_prestrain_15_rc_data.txt
*Step  = 37
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.22275654383,0,0,-0.497981756203,0,0
2,3.00712223851,0,0,0.0617901451614,0,0
3,-3.99522963444,-0,-0,-0.376764490125,-0,-0
