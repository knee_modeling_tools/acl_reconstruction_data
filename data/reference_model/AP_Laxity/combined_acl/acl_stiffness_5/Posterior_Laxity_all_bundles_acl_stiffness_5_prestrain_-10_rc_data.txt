File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.5545781146,0,0,0.000101150762891,0,0
2,0.104410997345,0,0,-0.00247341996118,0,0
3,-0.0519689674887,-0,-0,0.00400341187834,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.61116982177,0,0,0.000139746936348,0,0
2,0.093481527702,0,0,-0.0027307295136,0,0
3,-0.0811916737145,-0,-0,0.0060575665601,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 3
*Time  = 0.00031
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.637806920794,0,0,0.000163178558183,0,0
2,0.0848611263785,0,0,-0.00285807887919,0,0
3,-0.0981569154323,-0,-0,0.00744528663354,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 4
*Time  = 0.00156
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.650984611457,0,0,0.000175351472678,0,0
2,0.0786152847885,0,0,-0.00293668675141,0,0
3,-0.107021011001,-0,-0,0.00807651664591,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 5
*Time  = 0.00781
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.663504400752,0,0,0.000173469054655,0,0
2,0.0766824818472,0,0,-0.00298621107407,0,0
3,-0.108180501412,-0,-0,0.00836397892187,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 6
*Time  = 0.03906
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.679825423953,0,0,0.000120233517147,0,0
2,0.0605763730526,0,0,-0.00287422009268,0,0
3,-0.0872990199096,-0,-0,0.0100542289309,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 7
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.661455414893,0,0,0.000105541145729,0,0
2,-0.0149391429898,0,0,-0.00282004998926,0,0
3,-0.0765905944022,-0,-0,0.013714453709,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 8
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.588448993321,0,0,9.87099001762e-06,0,0
2,-0.349887346912,0,0,-0.00290938979033,0,0
3,0.0271742979919,0,0,0.0342885239818,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 9
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.544750247372,0,0,-4.99346896247e-05,0,0
2,-0.542349120027,0,0,-0.00331077877807,0,0
3,0.07554170924,0,0,0.0470356031288,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 10
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.538953767928,0,0,-0.000105565662071,0,0
2,-0.638141044387,0,0,-0.00371379896319,0,0
3,0.098728673356,0,0,0.054175040159,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 11
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.510527263275,0,0,-0.000160228116812,0,0
2,-0.718809850758,0,0,-0.00398952668702,0,0
3,0.12229185144,0,0,0.0609099784602,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 12
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.515468394902,0,0,-0.000213061553733,0,0
2,-0.766300351159,0,0,-0.00418071121266,0,0
3,0.134004228317,0,0,0.0649854108592,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 13
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.51167727039,0,0,-0.000267062350991,0,0
2,-0.813396250139,0,0,-0.00438706404242,0,0
3,0.147656040929,0,0,0.0693394777775,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 14
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.482202459599,0,0,-0.000327314055258,0,0
2,-0.850564809696,0,0,-0.00460242592395,0,0
3,0.165317911089,0,0,0.0739674810562,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 15
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.499018124149,0,0,-0.000379268347171,0,0
2,-0.882854922603,0,0,-0.00470123385412,0,0
3,0.172964647606,0,0,0.0767722383953,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 16
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.500720824602,0,0,-0.000434253125589,0,0
2,-0.914869149784,0,0,-0.00484817265205,0,0
3,0.182730066019,0,0,0.0799252244282,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 17
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.495129928529,0,0,-0.00049034819696,0,0
2,-0.945348743066,0,0,-0.00495837558934,0,0
3,0.192346411133,0,0,0.0830110534327,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 18
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.494527451384,0,0,-0.000545582513819,0,0
2,-0.973177623183,0,0,-0.00508603953677,0,0
3,0.201684497861,0,0,0.0859405501271,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 19
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.492160159626,0,0,-0.000600578064787,0,0
2,-0.9992926562,0,0,-0.00519829397779,0,0
3,0.210496931632,0,0,0.0887306330384,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 20
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.497695246815,0,0,-0.000657212816427,0,0
2,-1.02311004461,0,0,-0.00533535909889,0,0
3,0.218800470243,0,0,0.0913151740437,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 21
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.489293132836,0,0,-0.000714468849,0,0
2,-1.04724248139,0,0,-0.00541691837354,0,0
3,0.227186914517,0,0,0.0939602307486,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 22
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.486580235412,0,0,-0.000771345369335,0,0
2,-1.07022582102,0,0,-0.00548212611315,0,0
3,0.2346943396,0,0,0.0962933772479,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 23
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.4839401097,0,0,-0.000827768998517,0,0
2,-1.09277233493,0,0,-0.0055503224593,0,0
3,0.242273764017,0,0,0.0986057159835,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 24
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.448231825369,0,0,-0.000888521794949,0,0
2,-1.10810951813,0,0,-0.00569544607244,0,0
3,0.25679256246,0,0,0.101730414494,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 25
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465525812923,0,0,-0.000941365967336,0,0
2,-1.12412322052,0,0,-0.00566037879034,0,0
3,0.258734398521,0,0,0.103017649628,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 26
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.46440224821,0,0,-0.000994346367811,0,0
2,-1.14588782598,0,0,-0.00570817327231,0,0
3,0.265518297497,0,0,0.105200808695,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484013883542,0,0,-0.000996166614235,0,0
2,-1.12483600159,0,0,-0.00570875487221,0,0
3,0.251840746216,0,0,0.103219743607,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.488445774327,0,0,-0.000997722580891,0,0
2,-1.12530185284,0,0,-0.00570280040177,0,0
3,0.2508515847,0,0,0.103065384098,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490125948259,0,0,-0.000998177650978,0,0
2,-1.12604640753,0,0,-0.00570094317024,0,0
3,0.25046702754,0,0,0.103028405938,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491314244437,0,0,-0.00098729015028,0,0
2,-1.12693959033,0,0,-0.00570996230447,0,0
3,0.250623698013,0,0,0.103077701145,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490147311761,0,0,-0.000729893547345,0,0
2,-1.13046816252,0,0,-0.00569918992421,0,0
3,0.253884246429,0,0,0.103333185508,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.4848691586,0,0,0.000764106282241,0,0
2,-1.14250903531,0,0,-0.0055682872916,0,0
3,0.268762650613,0,0,0.103939789439,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491857686499,0,0,-0.0010126291326,0,0
2,-1.13606993501,0,0,-0.00575439121229,0,0
3,0.254403352274,0,0,0.10436230051,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.502893398586,0,0,-0.0276541733173,0,0
2,-1.09244871178,0,0,-0.00838513402462,0,0
3,0.0746926072168,0,0,0.111521920899,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.479531091081,0,0,-0.0542320494685,0,0
2,-0.963027349072,0,0,-0.00982042891918,0,0
3,-0.145191179337,-0,-0,0.113150952961,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.450824285903,0,0,-0.0807429735204,0,0
2,-0.795829560541,0,0,-0.010511262718,0,0
3,-0.378883986268,-0,-0,0.114328038362,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403670586745,0,0,-0.10724338027,0,0
2,-0.619167512425,0,0,-0.0110164286208,0,0
3,-0.612077468516,-0,-0,0.113069331817,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.366195747866,0,0,-0.133700330948,0,0
2,-0.419026334798,0,0,-0.011099786745,0,0
3,-0.855765043374,-0,-0,0.109734977578,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.304402622074,0,0,-0.160120999719,0,0
2,-0.203948405856,0,0,-0.0107931560806,0,0
3,-1.1018773433,-0,-0,0.104538298517,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.216141610567,0,0,-0.186525904594,0,0
2,0.0183798451105,0,0,-0.0103380377884,0,0
3,-1.34558408865,-0,-0,0.0985141028025,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0762447362138,0,0,-0.212894936405,0,0
2,0.30518329287,0,0,-0.00859368754368,0,0
3,-1.59436164118,-0,-0,0.0862758161055,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0359013025039,0,0,-0.239281279122,0,0
2,0.52268983891,0,0,-0.00726111492836,0,0
3,-1.84245046964,-0,-0,0.0783080277927,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.120102252098,0,0,-0.265681775305,0,0
2,0.683011716481,0,0,-0.0066716152253,0,0
3,-2.09237076255,-0,-0,0.0734950864643,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.207684483378,0,0,-0.292107447764,0,0
2,0.823159679926,0,0,-0.00622912147933,0,0
3,-2.34187331936,-0,-0,0.0700447297191,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.280103389807,0,0,-0.318534688214,0,0
2,0.958298156388,0,0,-0.00572102661099,0,0
3,-2.59490885302,-0,-0,0.0658394063119,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.362368537147,0,0,-0.344972931119,0,0
2,1.08987562908,0,0,-0.00507927623527,0,0
3,-2.84712148092,-0,-0,0.0609070713883,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.431317640457,0,0,-0.371422712381,0,0
2,1.23379686439,0,0,-0.00441460439323,0,0
3,-3.10424710603,-0,-0,0.0559372886014,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.483263677029,0,0,-0.397901878729,0,0
2,1.38344504349,0,0,-0.00508179587088,0,0
3,-3.36669347645,-0,-0,0.0546378639574,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.579344370415,0,0,-0.424390554497,0,0
2,1.54175487024,0,0,-0.00371224011368,0,0
3,-3.61911695527,-0,-0,0.0456957519514,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.63182112495,0,0,-0.4508923055,0,0
2,1.71093018355,0,0,-0.00251688243214,0,0
3,-3.88708722033,-0,-0,0.0401085847685,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.704888593317,0,0,-0.477413971426,0,0
2,1.88992792025,0,0,-0.00116757593582,0,0
3,-4.15315058985,-0,-0,0.0345566557954,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.746211930918,0,0,-0.503936939369,0,0
2,2.07816881908,0,0,-0.000104683093612,0,0
3,-4.42573918556,-0,-0,0.0322560706863,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.773355567268,0,0,-0.503956611422,0,0
2,2.09871238716,0,0,0.00208450460152,0,0
3,-4.43698172479,-0,-0,0.0244038071786,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.919321400005,0,0,-0.503415070365,0,0
2,2.34529711759,0,0,0.00803246687705,0,0
3,-4.30810310935,-0,-0,-0.0524864020856,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.04312403328,0,0,-0.502817759617,0,0
2,2.60060370801,0,0,0.00685395538117,0,0
3,-4.21058515327,-0,-0,-0.0972966890159,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.23367021537,0,0,-0.502372909812,0,0
2,2.77273179873,0,0,0.0149848948855,0,0
3,-4.13208286309,-0,-0,-0.145018811889,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 31
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.77332498934,0,0,-0.501710643156,0,0
2,2.91761306537,0,0,0.031657490936,0,0
3,-3.99579823802,-0,-0,-0.247216294037,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 32
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.11861642561,0,0,-0.500894237314,0,0
2,3.0685926643,0,0,0.0513704935338,0,0
3,-3.9470535909,-0,-0,-0.325201287653,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 33
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.08618339528,0,0,-0.50032249146,0,0
2,3.17294630849,0,0,0.0529873531581,0,0
3,-4.00076800378,-0,-0,-0.337072396009,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 34
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.21688296609,0,0,-0.499555854316,0,0
2,3.27304472931,0,0,0.057939115203,0,0
3,-3.98867524926,-0,-0,-0.36309518897,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 35
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.30703740343,0,0,-0.498731413978,0,0
2,3.37509838769,0,0,0.0619133646666,0,0
3,-3.98036786994,-0,-0,-0.382360682113,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 36
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.40379524871,0,0,-0.49786494754,0,0
2,3.47024071907,0,0,0.0657500214527,0,0
3,-3.97086678621,-0,-0,-0.399482568382,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-10_rc_data.txt
*Step  = 37
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.47788839326,0,0,-0.497003348138,0,0
2,3.56149718272,0,0,0.0690693079553,0,0
3,-3.96386520812,-0,-0,-0.41374293214,-0,-0
