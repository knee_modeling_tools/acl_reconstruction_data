File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.554259438973,0,0,0.000101146521336,0,0
2,0.1022760265,0,0,-0.00247055104835,0,0
3,-0.0516886917252,-0,-0,0.00410437654655,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.612005712201,0,0,0.000139852123262,0,0
2,0.0917328384016,0,0,-0.00273783522685,0,0
3,-0.080779371277,-0,-0,0.00614996938474,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 3
*Time  = 0.00031
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.636543672373,0,0,0.00016333057693,0,0
2,0.0822535608581,0,0,-0.00285109527933,0,0
3,-0.0980036019868,-0,-0,0.00756467650652,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 4
*Time  = 0.00156
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.650429946981,0,0,0.000175489802799,0,0
2,0.0761779665742,0,0,-0.00293135004093,0,0
3,-0.106748051709,-0,-0,0.00817866000706,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 5
*Time  = 0.00781
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.663159819603,0,0,0.000173605655618,0,0
2,0.0745502260525,0,0,-0.00298310554919,0,0
3,-0.107919985452,-0,-0,0.00845689618613,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 6
*Time  = 0.03906
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.679853489403,0,0,0.000120291955462,0,0
2,0.0599714088432,0,0,-0.00287417253868,0,0
3,-0.0872611334346,-0,-0,0.0100809156152,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 7
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.661483559583,0,0,0.000105573622218,0,0
2,-0.0149679272427,0,0,-0.00282043849028,0,0
3,-0.0766282213108,-0,-0,0.0137154528822,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 8
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.588454121651,0,0,9.91390128345e-06,0,0
2,-0.349760012437,0,0,-0.00290837240357,0,0
3,0.0271231674286,0,0,0.0342792142402,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 9
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.544790699546,0,0,-4.99101289389e-05,0,0
2,-0.542317503955,0,0,-0.00331058731662,0,0
3,0.0755314352251,0,0,0.0470324111246,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 10
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.536498764303,0,0,-0.0001056113832,0,0
2,-0.642927968522,0,0,-0.00371242849778,0,0
3,0.100038672376,0,0,0.054502813874,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 11
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511921230423,0,0,-0.000160102555504,0,0
2,-0.717533387165,0,0,-0.00399017203526,0,0
3,0.121640530506,0,0,0.0607990079141,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 12
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.51564881609,0,0,-0.000212916216114,0,0
2,-0.766354594218,0,0,-0.00418244956378,0,0
3,0.133975948157,0,0,0.0649899800578,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 13
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.512555968325,0,0,-0.000267081320831,0,0
2,-0.812570177853,0,0,-0.00438754246426,0,0
3,0.147369621064,0,0,0.0692704281398,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 14
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.481449704001,0,0,-0.000327278259098,0,0
2,-0.851178660608,0,0,-0.00460317858336,0,0
3,0.165570462781,0,0,0.0740239849025,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 15
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.49916667046,0,0,-0.000379351974335,0,0
2,-0.8827437218,0,0,-0.00469920694115,0,0
3,0.172867259664,0,0,0.0767502362423,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 16
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.500676385306,0,0,-0.000434858604909,0,0
2,-0.91458375308,0,0,-0.0048389452531,0,0
3,0.182446092904,0,0,0.0798682591878,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 17
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.495583048228,0,0,-0.000490502856465,0,0
2,-0.945205294546,0,0,-0.0049611330256,0,0
3,0.192380946929,0,0,0.0830021840846,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 18
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.492987168436,0,0,-0.000545510043924,0,0
2,-0.974351264671,0,0,-0.00508176969477,0,0
3,0.202116408169,0,0,0.0860366792117,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 19
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.492062370336,0,0,-0.000600531803384,0,0
2,-0.999652401153,0,0,-0.00519660524789,0,0
3,0.21053620661,0,0,0.088750480387,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 20
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.497544589765,0,0,-0.000657191416767,0,0
2,-1.0232585645,0,0,-0.0053332156217,0,0
3,0.218735011584,0,0,0.0913156310576,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 21
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.489768789876,0,0,-0.000714375219514,0,0
2,-1.04734491747,0,0,-0.00541764508652,0,0
3,0.227160148734,0,0,0.0939575827735,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 22
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.486624218015,0,0,-0.000771293985322,0,0
2,-1.07033213832,0,0,-0.00548246989823,0,0
3,0.234687412454,0,0,0.0962981155616,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 23
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483972427355,0,0,-0.000827740603312,0,0
2,-1.09285027861,0,0,-0.00555029977236,0,0
3,0.242268598989,0,0,0.0986078535013,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 24
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.448186327625,0,0,-0.000888504553884,0,0
2,-1.10819557725,0,0,-0.00569519672194,0,0
3,0.256798998289,0,0,0.101734953391,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 25
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465561714009,0,0,-0.00094135100078,0,0
2,-1.12416196365,0,0,-0.00566033896929,0,0
3,0.258725633309,0,0,0.103018146031,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 26
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.464420187431,0,0,-0.000994363008617,0,0
2,-1.14593087889,0,0,-0.00570744905077,0,0
3,0.265510665062,0,0,0.105198889713,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483981975728,0,0,-0.000996136671295,0,0
2,-1.12489298956,0,0,-0.00570882534958,0,0
3,0.251847617872,0,0,0.103223693952,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.488458891531,0,0,-0.000997692117586,0,0
2,-1.12535015121,0,0,-0.00570271426157,0,0
3,0.250849779601,0,0,0.103067319825,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490136156597,0,0,-0.000998150723418,0,0
2,-1.126090692,0,0,-0.00570085445357,0,0
3,0.250464575478,0,0,0.1030301123,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491308374126,0,0,-0.000987271554135,0,0
2,-1.12698145437,0,0,-0.0057098176394,0,0
3,0.250622115066,0,0,0.103079382084,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490155479459,0,0,-0.000729874038984,0,0
2,-1.13051048219,0,0,-0.00569906374533,0,0
3,0.253882835865,0,0,0.103334735536,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484864324,0,0,0.000764124779413,0,0
2,-1.14255125436,0,0,-0.00556818533306,0,0
3,0.268761067878,0,0,0.103941534549,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491867096261,0,0,-0.00101260765912,0,0
2,-1.13611216992,0,0,-0.00575429589465,0,0
3,0.254401929516,0,0,0.104363842077,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.50292929387,0,0,-0.0276541663931,0,0
2,-1.09244944331,0,0,-0.00838516980271,0,0
3,0.0746844301609,0,0,0.111521133767,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.479561861505,0,0,-0.0542319962554,0,0
2,-0.963039393648,0,0,-0.0098203976566,0,0
3,-0.145195131991,-0,-0,0.113151149462,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.450818610495,0,0,-0.0807429360078,0,0
2,-0.79586059872,0,0,-0.0105109448024,0,0
3,-0.378879959851,-0,-0,0.114328638275,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403633525027,0,0,-0.10724336658,0,0
2,-0.619164436337,0,0,-0.0110164794441,0,0
3,-0.612076680135,-0,-0,0.113070188837,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.366252113263,0,0,-0.133700291366,0,0
2,-0.418953404374,0,0,-0.0111008539478,0,0
3,-0.855780605965,-0,-0,0.109729698636,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.304367440167,0,0,-0.160120937277,0,0
2,-0.203840828996,0,0,-0.0107873091541,0,0
3,-1.10189225945,-0,-0,0.10453089638,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.209252050506,0,0,-0.186519355202,0,0
2,0.0273722554633,0,0,-0.0100360394855,0,0
3,-1.34585558235,-0,-0,0.0975331985635,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0755628791265,0,0,-0.212893654606,0,0
2,0.307973213968,0,0,-0.0084858796733,0,0
3,-1.59387231868,-0,-0,0.086140224907,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0345129576228,0,0,-0.239283057198,0,0
2,0.516656899526,0,0,-0.0073055866134,0,0
3,-1.84213503519,-0,-0,0.0788017848849,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.115454083941,0,0,-0.26568476346,0,0
2,0.674728191046,0,0,-0.00675080927721,0,0
3,-2.09274610086,-0,-0,0.0740292144617,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.202074386264,0,0,-0.292112405135,0,0
2,0.810886678253,0,0,-0.00631769416898,0,0
3,-2.34208232997,-0,-0,0.0708461420987,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.296667632945,0,0,-0.318543227692,0,0
2,0.944516400706,0,0,-0.00574411782182,0,0
3,-2.59194905405,-0,-0,0.0662959556671,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.363893763515,0,0,-0.34498272538,0,0
2,1.07487928223,0,0,-0.00513883258733,0,0
3,-2.84597282603,-0,-0,0.0615912375075,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.445404225419,0,0,-0.371439764919,0,0
2,1.21603315246,0,0,-0.00446647518123,0,0
3,-3.10084005118,-0,-0,0.0564392188956,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.48815207722,0,0,-0.397920685911,0,0
2,1.36364090888,0,0,-0.00520782882723,0,0
3,-3.36440086972,-0,-0,0.0555587347402,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.549312905313,0,0,-0.424408651223,0,0
2,1.52218595787,0,0,-0.00377630645031,0,0
3,-3.62272611206,-0,-0,0.0472758242739,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.616892733685,0,0,-0.450915662587,0,0
2,1.68762736031,0,0,-0.00285436500798,0,0
3,-3.88813968543,-0,-0,0.0422800339724,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.670412921665,0,0,-0.477438369999,0,0
2,1.86469272271,0,0,-0.00155522412744,0,0
3,-4.15729275089,-0,-0,0.0377125228161,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.710703785652,0,0,-0.503969287848,0,0
2,2.05135527093,0,0,-0.000572174503112,0,0
3,-4.43045080777,-0,-0,0.0364080546168,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.748416198362,0,0,-0.503988601757,0,0
2,2.06971054772,0,0,0.00167748675823,0,0
3,-4.43926542923,-0,-0,0.027807800358,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.755755338058,0,0,-0.503441542943,0,0
2,2.30991441411,0,0,-0.00133187340712,0,0
3,-4.35158644767,-0,-0,-0.0191486459558,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.339268281821,0,0,-0.500714209753,0,0
2,2.6662983466,0,0,-0.00626073869836,0,0
3,-4.46697456045,-0,-0,0.00860236229972,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.50985156273,0,0,-0.497633007697,0,0
2,2.61654874523,0,0,0.0447971976942,0,0
3,-4.02561881263,-0,-0,-0.237118874018,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 31
*Time  = 2.2483871
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.99748671843,0,0,-0.500453441876,0,0
2,2.85104947294,0,0,0.0407247261191,0,0
3,-4.18273353918,-0,-0,-0.223390214999,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 32
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.323880550408,0,0,-0.500820740617,0,0
2,3.09031811008,0,0,-0.00399828936067,-0,-0
3,-4.5441880731,-0,-0,0.0852203530984,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 33
*Time  = 2.2983871
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.10622508556,0,0,-0.500232918209,0,0
2,2.9675089605,0,0,0.0548363721131,0,0
3,-4.26082859209,-0,-0,-0.26161134774,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 34
*Time  = 2.29953174
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.00304290626,0,0,-0.500347275091,0,0
2,3.50143849034,0,0,-0.0122583050391,0,0
3,-4.50299201355,-0,-0,0.0659219147608,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 35
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.97042234901,0,0,-0.500299099539,0,0
2,3.48661720668,0,0,-0.0112650978484,0,0
3,-4.50851525467,-0,-0,0.0616459048122,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 36
*Time  = 2.30319939
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.932337923247,0,0,-0.500271895156,0,0
2,3.50768207817,0,0,-0.0107398246791,0,0
3,-4.48863817824,-0,-0,0.0597297341582,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 37
*Time  = 2.31919635
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.02841388483,0,0,-0.500211635746,0,0
2,3.60355108391,0,0,-0.0105418946011,0,0
3,-4.45734746048,-0,-0,0.0609507106079,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 38
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.20734721061,0,0,-0.500155557838,0,0
2,3.76060973035,0,0,-0.0121091465743,0,0
3,-4.41481267112,-0,-0,0.0667123459914,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 39
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.41605521981,0,0,-0.500003344164,0,0
2,4.00921343688,0,0,-0.0114752099644,0,0
3,-4.33869394025,-0,-0,0.0712626596033,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 40
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.56187519662,0,0,-0.499830602395,0,0
2,4.1955301758,0,0,-0.0114513204796,0,0
3,-4.29715059255,-0,-0,0.0767973677227,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 41
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.83184224692,0,0,-0.499803216964,0,0
2,4.36562670995,0,0,-0.018140575271,0,0
3,-4.26558424725,-0,-0,0.0894803808337,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-5_rc_data.txt
*Step  = 42
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-1.91022613587,0,0,-0.499628460452,0,0
2,4.51859638421,0,0,-0.0183423712906,0,0
3,-4.22063078855,-0,-0,0.0925737971321,0,0
