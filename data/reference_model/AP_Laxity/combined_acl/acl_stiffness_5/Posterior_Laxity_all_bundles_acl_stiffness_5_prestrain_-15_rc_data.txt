File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 1
*Time  = 1e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.554955683308,0,0,0.000101128390833,0,0
2,0.106500759361,0,0,-0.00247692311058,0,0
3,-0.0522292742297,-0,-0,0.00390429482656,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 2
*Time  = 6e-05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.611631325789,0,0,0.000139658844765,0,0
2,0.0958898081456,0,0,-0.00273497184029,0,0
3,-0.0814717257261,-0,-0,0.00594275379677,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 3
*Time  = 0.00031
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.638274489324,0,0,0.00016306043351,0,0
2,0.0873657768037,0,0,-0.00286288413774,0,0
3,-0.0984396506648,-0,-0,0.00732593719919,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 4
*Time  = 0.00156
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.651297879091,0,0,0.000175184955721,0,0
2,0.0809878239981,0,0,-0.00293872088886,0,0
3,-0.107338834819,-0,-0,0.0079663868798,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 5
*Time  = 0.00781
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.663912604569,0,0,0.000173321307779,0,0
2,0.07910678963,0,0,-0.00298896559552,0,0
3,-0.108471133798,-0,-0,0.00825707425396,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 6
*Time  = 0.03906
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.679825154266,0,0,0.000120189402116,0,0
2,0.0612669851779,0,0,-0.0028737888762,0,0
3,-0.0873516956713,-0,-0,0.01002269929,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 7
*Time  = 0.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.661377969802,0,0,0.000105507050252,0,0
2,-0.0150188869504,0,0,-0.00281894635211,0,0
3,-0.0765395522345,-0,-0,0.01371629721,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 8
*Time  = 0.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.588439725635,0,0,9.82843282387e-06,0,0
2,-0.349921427078,0,0,-0.0029099041557,0,0
3,0.0272021580002,0,0,0.0342913267534,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 9
*Time  = 0.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.544800918017,0,0,-4.99500826884e-05,0,0
2,-0.542272039625,0,0,-0.0033111529536,0,0
3,0.075535676121,0,0,0.0470324549791,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 10
*Time  = 0.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.536226560231,0,0,-0.000105638440927,0,0
2,-0.643374222536,0,0,-0.00371208723774,0,0
3,0.100176193075,0,0,0.0545344358411,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 11
*Time  = 0.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.511993914097,0,0,-0.000160092312041,0,0
2,-0.717580237676,0,0,-0.00399174516628,0,0
3,0.121657616638,0,0,0.0608043084834,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 12
*Time  = 0.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.516098641169,0,0,-0.000213042256388,0,0
2,-0.765874907717,0,0,-0.00418226958579,0,0
3,0.133830505357,0,0,0.0649500950227,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 13
*Time  = 0.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.512166610494,0,0,-0.000267059305717,0,0
2,-0.812763552532,0,0,-0.00438752760775,0,0
3,0.147451107705,0,0,0.0692911285261,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 14
*Time  = 0.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.481387993095,0,0,-0.000327318082412,0,0
2,-0.8511721164,0,0,-0.00460190214859,0,0
3,0.165563347738,0,0,0.0740209621819,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 15
*Time  = 0.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.49871268702,0,0,-0.000379292890818,0,0
2,-0.883146416579,0,0,-0.0046985318184,0,0
3,0.173009157789,0,0,0.0767861441056,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 16
*Time  = 0.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.500703066376,0,0,-0.000434827712349,0,0
2,-0.914766745988,0,0,-0.00483810093394,0,0
3,0.182441685453,0,0,0.0798759070961,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 17
*Time  = 0.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.494601924683,0,0,-0.000490414837977,0,0
2,-0.946090501917,0,0,-0.0049580051514,0,0
3,0.192643305379,0,0,0.0830664658841,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 18
*Time  = 0.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.4942339366,0,0,-0.000545520392482,0,0
2,-0.973536556717,0,0,-0.00508424197579,0,0
3,0.201732564938,0,0,0.0859617495043,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 19
*Time  = 0.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.492280555463,0,0,-0.000600679390064,0,0
2,-0.999407024149,0,0,-0.00519669110906,0,0
3,0.210442092848,0,0,0.0887237705935,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 20
*Time  = 0.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.497607715395,0,0,-0.000657235092276,0,0
2,-1.02300882902,0,0,-0.00533468504939,0,0
3,0.21871195057,0,0,0.09130444799,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 21
*Time  = 0.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.488972867242,0,0,-0.000714313534032,0,0
2,-1.0478931738,0,0,-0.00541508811521,0,0
3,0.227375880727,0,0,0.0940039026947,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 22
*Time  = 0.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.48660356164,0,0,-0.000771221583541,0,0
2,-1.07044910316,0,0,-0.00548227162798,0,0
3,0.23470870679,0,0,0.0963060942047,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 23
*Time  = 0.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483970649469,0,0,-0.000827666652168,0,0
2,-1.09292034645,0,0,-0.00555042488247,0,0
3,0.242269353925,0,0,0.0986132650751,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 24
*Time  = 0.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.448549102588,0,0,-0.000888574950034,0,0
2,-1.10790978235,0,0,-0.00569646113498,0,0
3,0.256697231382,0,0,0.101713340175,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 25
*Time  = 0.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.465536005452,0,0,-0.000941407208181,0,0
2,-1.12409635152,0,0,-0.00566056474819,0,0
3,0.258732710561,0,0,0.103016647406,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 26
*Time  = 0.99999
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.464033482422,0,0,-0.000994260648299,0,0
2,-1.14619300837,0,0,-0.00570654921143,0,0
3,0.265617066456,0,0,0.105222967163,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 1
*Time  = 1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.483991656549,0,0,-0.000996047642867,0,0
2,-1.1249561427,0,0,-0.00570878725638,0,0
3,0.251845486426,0,0,0.103227542938,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 2
*Time  = 1.00005
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.488398938589,0,0,-0.000997616093451,0,0
2,-1.12545207369,0,0,-0.0057026812093,0,0
3,0.250866503743,0,0,0.103075250975,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 3
*Time  = 1.0003
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490117811386,0,0,-0.000998082463083,0,0
2,-1.12616302232,0,0,-0.00570095793199,0,0
3,0.250470309844,0,0,0.103035573789,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 4
*Time  = 1.00155
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491285666161,0,0,-0.000987212574919,0,0
2,-1.12704719648,0,0,-0.00570996352207,0,0
3,0.250628482529,0,0,0.103084641541,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 5
*Time  = 1.0078
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.490147966154,0,0,-0.00072982427353,0,0
2,-1.1305608137,0,0,-0.00569925565514,0,0
3,0.253886155511,0,0,0.103338862955,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 6
*Time  = 1.03905
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.484858792023,0,0,0.000764171788146,0,0
2,-1.14259893529,0,0,-0.00556833711798,0,0
3,0.26876511521,0,0,0.1039453627,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 7
*Time  = 1.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.491851169571,0,0,-0.00101256019318,0,0
2,-1.13615217954,0,0,-0.0057545187164,0,0
3,0.254406021507,0,0,0.104367722085,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 8
*Time  = 1.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.502951961462,0,0,-0.0276541633958,0,0
2,-1.09241207339,0,0,-0.00838542473756,0,0
3,0.0746711338568,0,0,0.111519906539,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 9
*Time  = 1.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.479556242563,0,0,-0.0542319757165,0,0
2,-0.963005481412,0,0,-0.00982056783671,0,0
3,-0.145199659682,-0,-0,0.113151119933,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 10
*Time  = 1.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.450823295686,0,0,-0.0807429276149,0,0
2,-0.795794753921,0,0,-0.0105113369096,0,0
3,-0.378892815554,-0,-0,0.114327402931,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 11
*Time  = 1.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.403679187741,0,0,-0.107243357591,0,0
2,-0.619148170145,0,0,-0.0110163798434,0,0
3,-0.612078744943,-0,-0,0.113068200029,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 12
*Time  = 1.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.366253977576,0,0,-0.133700266724,0,0
2,-0.418925006658,0,0,-0.0111000261746,0,0
3,-0.855782293152,-0,-0,0.109727157653,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 13
*Time  = 1.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.304426587459,0,0,-0.160120910783,0,0
2,-0.203689468497,0,0,-0.0107876784844,0,0
3,-1.10190414964,-0,-0,0.104517453654,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 14
*Time  = 1.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.209095342697,0,0,-0.186519322933,0,0
2,0.0274424344394,0,0,-0.0100342083172,0,0
3,-1.34582800886,-0,-0,0.0975292266671,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 15
*Time  = 1.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,-0.0750364000587,0,0,-0.212893357276,0,0
2,0.308887410201,0,0,-0.008477845637,0,0
3,-1.59380964102,-0,-0,0.0860733784225,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 16
*Time  = 1.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.0392627705597,0,0,-0.239279274747,0,0
2,0.528575961186,0,0,-0.00721108970404,0,0
3,-1.84210345769,-0,-0,0.0780671640904,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 17
*Time  = 1.55
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.122219067511,0,0,-0.265678661972,0,0
2,0.691506272176,0,0,-0.0066124369775,0,0
3,-2.09240625512,-0,-0,0.0730466249686,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 18
*Time  = 1.6
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.203426305615,0,0,-0.292100502565,0,0
2,0.835078020761,0,0,-0.00617598250978,0,0
3,-2.34322551984,-0,-0,0.0695797970612,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 19
*Time  = 1.65
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.292744441015,0,0,-0.318528221502,0,0
2,0.973213400461,0,0,-0.00556350242109,0,0
3,-2.59369536514,-0,-0,0.0648184352126,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 20
*Time  = 1.7
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.371422656476,0,0,-0.344963909479,0,0
2,1.10733200011,0,0,-0.00492887704824,0,0
3,-2.84656611552,-0,-0,0.0597055928773,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 21
*Time  = 1.75
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.442099602065,0,0,-0.371410451774,0,0
2,1.25363283167,0,0,-0.00422910933318,0,0
3,-3.10373541271,-0,-0,0.0545572086554,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 22
*Time  = 1.8
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.496485625759,0,0,-0.39788692721,0,0
2,1.40541647062,0,0,-0.00481630062772,0,0
3,-3.36564539268,-0,-0,0.0529376253959,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 23
*Time  = 1.85
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.593832102152,0,0,-0.42437094859,0,0
2,1.56520996561,0,0,-0.00339049429393,0,0
3,-3.61837253618,-0,-0,0.0436444431267,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 24
*Time  = 1.9
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.648229328142,0,0,-0.450868795503,0,0
2,1.7367105032,0,0,-0.00222376688347,0,0
3,-3.88590382962,-0,-0,0.03784646198,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 25
*Time  = 1.95
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.727475280442,0,0,-0.477386287178,0,0
2,1.91715665849,0,0,-0.000864494825845,0,0
3,-4.15182319111,-0,-0,0.0322549218274,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 26
*Time  = 2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.775342164351,0,0,-0.503902567532,0,0
2,2.10711279675,0,0,0.000546741486355,0,0
3,-4.42244998414,-0,-0,0.0282290810023,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 27
*Time  = 2.05
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.795773842856,0,0,-0.503922540526,0,0
2,2.1302240677,0,0,0.00243603906702,0,0
3,-4.43562174193,-0,-0,0.0214518614258,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 28
*Time  = 2.1
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,0.865588796223,0,0,-0.50346257593,0,0
2,2.33087098781,0,0,0.00314548727181,0,0
3,-4.344744558,-0,-0,-0.0299712862821,0,0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 29
*Time  = 2.15
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.292253451,0,0,-0.502700724866,0,0
2,2.6557506394,0,0,0.0199806925454,0,0
3,-4.1443143186,-0,-0,-0.141766844357,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 30
*Time  = 2.2
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.31455782381,0,0,-0.502273217505,0,0
2,2.86424149408,0,0,0.0176252984753,0,0
3,-4.12139023339,-0,-0,-0.163675505898,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 31
*Time  = 2.25
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,1.74892346332,0,0,-0.501688961027,0,0
2,2.98348725949,0,0,0.0304636544957,0,0
3,-4.01541352402,-0,-0,-0.246944726291,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 32
*Time  = 2.3
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.18445653199,0,0,-0.500797496976,0,0
2,3.14521210755,0,0,0.0537866851246,0,0
3,-3.93710032789,-0,-0,-0.335910153676,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 33
*Time  = 2.35
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.12235250486,0,0,-0.500211348116,0,0
2,3.25998616276,0,0,0.0542757402212,0,0
3,-3.99869734423,-0,-0,-0.344797155728,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 34
*Time  = 2.4
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.26542867206,0,0,-0.499434129694,0,0
2,3.36336676655,0,0,0.0592189731831,0,0
3,-3.98240858197,-0,-0,-0.370365684496,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 35
*Time  = 2.45
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.34365764466,0,0,-0.498599739339,0,0
2,3.47723128821,0,0,0.0630714743379,0,0
3,-3.97496226969,-0,-0,-0.388729410136,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 36
*Time  = 2.5
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.44232585992,0,0,-0.497721261158,0,0
2,3.57853590039,0,0,0.0669556111854,0,0
3,-3.96322694303,-0,-0,-0.405675833389,-0,-0
File = Posterior_Laxity_all_bundles_acl_stiffness_5_prestrain_-15_rc_data.txt
*Step  = 37
*Time  = 2.54998
*Data  = RCx;RCy;RCz;RCthx;RCthy;RCthz
1,2.52281380529,0,0,-0.496854939982,0,0
2,3.67843043807,0,0,0.0704455142049,0,0
3,-3.95275718972,-0,-0,-0.420178598957,-0,-0
