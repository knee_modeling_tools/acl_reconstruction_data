# -*- coding: utf-8 -*-
# Author : Konstantinos Risvas
# Date : 2/25/2021 
# Email: krisvas@ece.upatras.gr 
"""
Description: This scripts performs the anterior - posterior laxity test,
useful to change the properties of ACL and PCL based on the openknee data.
Knee flexion up tp 30 deg and anterior - posterior force of 0 to +- 100N.

The laxity_processing_2 script was acquired from the OpenKnee project
repository.
"""
import os
from utils import read_febio_output, average_data, cut_data, \
    get_kinetics_kinematics, get_desired_kinetics, find_average_data, \
    crop_data, find_hold_indices, apply_offsets, change_kinematics_reporting, \
    change_kinetics_reporting, plot_groups, kinetics_tibia_to_femur, \
    get_offsets, find_model_offsets, tdms_contents
import numpy as np
import copy
import matplotlib.pyplot as plt
from matplotlib import cm, rc
import natsort
from natsort import ns
from collections import OrderedDict
import matplotlib.gridspec as gridspec


def laxity_processing_2(groups, experiment_offsets, model_offsets,
                        tdms_directory, show=False):

    """use the desired kinematics channels to filter the kinematics and
    kinetics data"""

    kinetics_group, kinematics_group = get_kinetics_kinematics(groups)
    desired_kinetics_group = get_desired_kinetics(groups)

    # find the average flexion angle in the data
    flexion_angle = find_average_data(kinematics_group, 3)
    rounded_flexion = int(round(flexion_angle, -1))  # round to the nearest
    # 10 - this is the 'intended' flexion, use for naming files

    # plot_groups("Laxity_{}deg_Kinetics_desired_raw".format(
    # rounded_flexion), desired_kinetics_group, 'Time (ms)', tdms_directory,
    # show_plot=False)

    # save the raw data as csv and png file
    # plot_groups("Laxity_{}deg_Kinematics_raw".format(rounded_flexion),
    #             kinematics_group, 'Time (ms)', tdms_directory,show_plot=False)
    # plot_groups("Laxity_{}deg_Kinetics_raw".format(rounded_flexion),
    #             kinetics_group, 'Time (ms)', tdms_directory,show_plot=False)

    Loading_channels = [1]
    channel_nickname = ['AP']
    kinetics_list = []
    kinematics_list = []

    # # plot raw data
    # plt.figure()
    # plt.plot(kinematics_group.data[1],
    #          desired_kinetics_group.data[1])
    # plt.show()
    for n, chan in enumerate(Loading_channels):

        # use the desired kinetics to find the indices of the data points at
        # the end of the "flat" regions
        # where forces were held steady

        pos_index, neg_index = find_hold_indices(desired_kinetics_group, chan)

        for c, index in enumerate([pos_index, neg_index]):
            # make copies of the data so we don't make changes to the
            # original data at every loop
            kinetics_group_copy = copy.deepcopy(kinetics_group)
            kinematics_group_copy = copy.deepcopy(kinematics_group)

            # extract the data from the kinematics and kinetics
            crop_data(kinetics_group_copy, index)
            crop_data(kinematics_group_copy, index)

            # set the x axis to the applied load (actual)
            kinetics_group_copy.time = [kinetics_group_copy.data[chan]] * \
                                       len(kinetics_group_copy.data)
            kinematics_group_copy.time = [kinetics_group_copy.data[chan]] * \
                                         len(kinematics_group_copy.data)

            channel_units = kinetics_group_copy.units[chan]

            # these are the results that have undergone some processing and
            # will be pushlished for data representation step plot
            plot_groups('Laxity_{}deg_'.format(rounded_flexion) +
                        channel_nickname[n] + str(c + 1)
                        + '_TibiaKinetics_in_TibiaCS_experiment',
                        kinetics_group_copy,
                        'Applied Load (' + channel_units + ')',
                        tdms_directory, subdir='AP_Laxity', show_plot=False)
            plot_groups('Laxity_{}deg_'.format(rounded_flexion) +
                        channel_nickname[n] + str(c + 1) +
                        '_kinematics_in_JCS_experiment', kinematics_group_copy,
                        'Applied Load (' + channel_units + ')', tdms_directory,
                        subdir='AP_Laxity', show_plot=False)

            # continue with remaining processing steps for our team's workflow
            # apply experiment offsets to the kinematics data
            apply_offsets(kinematics_group_copy, experiment_offsets)

            # report the axes in the right handed coordinate system we defined.
            change_kinematics_reporting(kinematics_group_copy)
            change_kinetics_reporting(kinetics_group_copy)  # this is external
            # loads on tibia in tibia cs

            # report the kinetics as external loads applied to femur in tibia
            # coordinate system
            kinetics_tibia_to_femur(kinetics_group_copy,
                                    kinematics_group_copy, chan)

            # apply model offsets - Note this is done AFTER changing the
            # signs of the data to register with model outputs.
            apply_offsets(kinematics_group_copy, -model_offsets)

            # processed data this will be used to generate Models replicating
            # experiment
            plot_groups('Laxity_{}deg_'.format(rounded_flexion) +
                        channel_nickname[n] + str(c + 1)
                        + '_kinetics_in_TibiaCS', kinetics_group_copy,
                        'Applied Load (' + channel_units + ')',
                        tdms_directory, subdir='AP_Laxity', show_plot=show)
            plot_groups('Laxity_{}deg_'.format(rounded_flexion) +
                        channel_nickname[n] + str(c + 1) +
                        '_kinematics_in_JCS', kinematics_group_copy,
                        'Applied Load (' + channel_units + ')', tdms_directory,
                        subdir='AP_Laxity', show_plot=show)

            kinetics_list.append(kinetics_group_copy)
            kinematics_list.append(kinematics_group_copy)

        return kinematics_list, kinetics_list


def model_output_processing(ligament_name):
    """
    Function that processes FeBio output and stores data that were produced
    from cruciate ligament sensitivity analysis.

    Parameters
    ----------
    ligament_name [string] 'acl' or 'pcl'

    Returns
    -------
    parameter of interest
    """

    mse = {}
    total_translations = []

    if 'acl' in ligament_name:
        subdir = 'acl_stiffness_'
        model_results_dir = data_directory + '/combined_acl/'
    else:
        subdir = 'pcl_stiffness_'
        model_results_dir = data_directory + '/combined_pcl/'

    model_rc_files = []

    for dir in os.listdir(model_results_dir):
        for file in os.listdir(os.path.join(model_results_dir, dir)):
            if "rc" in file:
                model_rc_files.append(
                    os.path.abspath(os.path.join(model_results_dir, dir + '/' +
                                                 file)))

    model_rc_files_sorted = natsort.natsorted(model_rc_files, alg=ns.REAL)
    ap_translations = []
    model_names = []

    for file in model_rc_files_sorted:
        # filename = os.path.join(data_directory, file)
        data, vars, step, time = read_febio_output(file)
        time = np.round(np.array(time), 3)
        # times.append(time)
        # rigid connector 1
        rc1_translation = data['1']['RCx']
        rc1_rotation = np.rad2deg(data['1']['RCthx'])
        # rc1_rotation = -np.rad2deg(rc1_rotation)
        # rigid connector 2
        rc2_translation = data['2']['RCx']
        rc2_rotation = np.rad2deg(data['2']['RCthx'])
        # rc2_rotation = np.rad2deg(rc2_rotation)
        # rigid connector 3
        rc3_translation = data['3']['RCx']
        rc3_rotation = np.rad2deg(data['3']['RCthx'])
        # rc3_rotation = np.rad2deg(rc3_rotation)

        # find the time where the anterior - posterior force is not zero,
        # time = 2.0
        ap_laxity_start_idx = np.where(time == 2.05)[0][0]
        # keep the indices that correspond to increasing the value of Femur
        # force
        # the time step we set in FeBio was 0.05
        ideal_time = np.round(np.linspace(2.05, round(time[-1], 2), 11,
                                          dtype=float), 2)
        actual_time = np.round(time[ap_laxity_start_idx:], 2)
        time_indices = []
        for i in actual_time:
            for j in ideal_time:
                if j not in time_indices and i == j:
                    time_indices.append(np.where(time == i)[0][0])

        time_indices = list(OrderedDict.fromkeys(time_indices))
        ap_translation = rc2_translation[time_indices]

        ap_translations.append(ap_translation)

        if ligament_name == 'acl':
            name = os.path.basename(file.strip('rc_data.txt'))[len(
                'Posterior_Laxity_all_bundles_'):]
        else:
            name = os.path.basename(file.strip('rc_data.txt'))[len(
                'Anterior_Laxity_all_bundles_'):]
        model_names.append(name)

        total_translations.append(ap_translation)
        if 'stiffness_0_prestrain_0' in name:
            initial_guess = ap_translation

    if ligament_name == 'acl':
        for tr in range(len(ap_translations)):
            ms_error = np.square(np.subtract(kinematics[0].data[1],
                                             ap_translations[tr])).mean()
            mse[model_names[tr]] = ms_error
        initial_guess_error = np.square(np.subtract(kinematics[0].data[1],
                                                    initial_guess)).mean()
    else:
        for tr in range(len(ap_translations)):
            # estimate mean square error
            ms_error = np.square(np.subtract(kinematics[1].data[1],
                                             ap_translations[tr])).mean()
            mse[model_names[tr]] = ms_error
        initial_guess_error = np.square(np.subtract(kinematics[1].data[1],
                                                    initial_guess)).mean()

    # estimate and print minimum error
    errors = list(mse.values())
    keys = list(mse.keys())
    k = 10
    k_best_indices = np.argpartition(errors, k)
    best_fit = sorted([errors[i] for i in k_best_indices[:k]])
    for fit in best_fit:
        fit_idx = errors.index(fit)
        fit_key = keys[fit_idx]
        print("{} with an error of: {}".format(fit_key, fit))

    # print the initial guess error
    print("initial guess error : {} mm".format(initial_guess_error))

    return total_translations, initial_guess, best_fit, errors, model_names


################################################################################
subject = 'oks003'
# get the femur and tibia cs axes
probed_landmarks = os.path.abspath(
    "../data/{}_Probed_Anatomical_landmarks.xml".format(subject))
anatomical_landmarks = os.path.abspath(
    "../data/{}_Anatomical_landmarks.xml".format(subject))

results_dir = '../results/AP_Laxity/'
data_directory = os.path.abspath('../data/reference_model/AP_Laxity')
files = os.listdir(data_directory)

tdms_files = []
State_file = None
for file in os.listdir(data_directory):
    if file.endswith('.cfg'):
        State_file = os.path.abspath(os.path.join(data_directory, file))
    elif file.endswith('.tdms'):
        tdms_files.append(os.path.abspath(os.path.join(data_directory, file)))

for file in tdms_files:
    groups = tdms_contents(file)

    experiment_offsets = get_offsets(State_file)
    model_offsets = find_model_offsets(probed_landmarks, subject)

    if 'laxity' in file.lower():
        kinematics, kinetics = laxity_processing_2(groups, experiment_offsets,
                                                   model_offsets,
                                                   data_directory)

# acl data processing
acl_translations, acl_init_guess, acl_best_fit, acl_errors, acl_names = \
    model_output_processing('acl')

print("------------------------------------")

# pcl data processing
pcl_translations, pcl_init_guess, pcl_best_fit, pcl_errors, pcl_names = \
    model_output_processing('pcl')

# plot data in a single plot
rc('font', weight='bold', size='18')
nr = 1
ncol = 2
handles = []
labels = []
colormap = cm.get_cmap('viridis', len(acl_best_fit))
colors = colormap.colors
fig1 = plt.figure(constrained_layout=True, figsize=(18, 8))
spec1 = gridspec.GridSpec(ncols=ncol, nrows=nr, figure=fig1)
ax1 = fig1.add_subplot(spec1[0, 0])

for idx, fit in enumerate(acl_best_fit):
    i = acl_errors.index(fit)
    label = acl_names[i].split('_')
    name = '{}: {}%, {}: {}%'.format(
        label[1][:1], label[2], label[3][:1], label[4], round(acl_errors[i], 4))
    line, = ax1.plot(kinetics[0].data[1],
                     acl_translations[i],
                     color=colors[idx])
    handles.append(line)
    labels.append(name)

open0, = ax1.plot(kinetics[0].data[1], kinematics[0].data[1],
                  '--', label="OpenKnee",
                  color='red')

initial, = ax1.plot(kinetics[0].data[1],
                    acl_init_guess,
                    '-.',
                    color='blue')

labels.append("PCA Guess")
handles.append(initial)
ax1.set_ylabel('Knee JCS Posterior Translation (mm)', fontsize=22,
               fontweight='bold')
ax1.set_xlabel('Femur Load (N)', fontsize=22, fontweight='bold')
ax1.grid(True)

ax1.set_title('ACL', fontsize=24, fontweight='bold')
handles.append(open0)
labels.append('OpenKnee')
lg1 = ax1.legend(handles, labels, prop={'weight': 'bold', 'size': 15},
                 loc='upper right', handletextpad=0.4, handlelength=0.5)
for line in lg1.get_lines():
    line.set_linewidth(8)

handles = []
labels = []
ax2 = fig1.add_subplot(spec1[0, 1])

for idx, fit in enumerate(pcl_best_fit):
    i = pcl_errors.index(fit)
    label = pcl_names[i].split('_')
    name = '{}: {}%, {}: {}%'.format(
        label[1][:1], label[2], label[3][:1], label[4], round(pcl_errors[i], 4))
    line, = ax2.plot(kinetics[1].data[1], pcl_translations[i],
                     color=colors[idx])
    handles.append(line)
    labels.append(name)

open0, = ax2.plot(kinetics[1].data[1], kinematics[1].data[1], '--',
                  label="OpenKnee",
                  color='red')

initial, = ax2.plot(kinetics[1].data[1], pcl_init_guess,
                    '-.',
                    color='blue')

labels.append("PCA Guess")
handles.append(initial)
ax2.set_ylabel('Knee JCS Anterior Translation (mm)', fontsize=22,
               fontweight='bold')
ax2.set_xlabel('Femur Load (N)', fontsize=22, fontweight='bold')
ax2.grid(True)
ax2.set_title('PCL', fontsize=24, fontweight='bold')
handles.append(open0)
labels.append('OpenKnee')
lg2 = ax2.legend(handles, labels, prop={'weight': 'bold', 'size': 15},
                 loc='upper right', handletextpad=0.4, handlelength=0.5)

for line in lg2.get_lines():
    line.set_linewidth(8)
fig1.savefig(results_dir + 'stiffness_acl_pcl.pdf',
             format='pdf',
             dpi=1200)
plt.show()