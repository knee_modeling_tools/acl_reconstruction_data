# Author : Konstantinos Risvas
# Date: 30/6/2021
# E-mail: krisvas@ece.upatras.gr
"""
Single vs Double Bundle Results
"""

import os
from utils import read_febio_output
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, rc
import natsort
from natsort import realsorted, ns
from more_itertools import take


def find_diff(y1, y2):
    """
    Function that estimates the max absolute difference between two vectors and
    the index where this occurs

    Parameters
    ----------
    y1: vector1
    y2:

    Returns
    -------
    max difference and index of it

    """
    # print(y1, y2)
    difference = np.abs(np.subtract(y1, y2))
    print(difference)
    max_d = np.amax(difference)
    max_d_idx = np.where(difference == max_d)

    return round(max_d, 3), max_d_idx


def single_vs_double(graft='semitendinosus'):
    """
    Estimates the translation for the single bundle and double bundle
    simulations

    Parameters
    ----------
    graft: the graft type defines the subdirectory where the simulation data
    are stored

    Returns
    -------
    single_trans: single bundle translations
    single_err: absolute difference between model and reference model
    double_trans: double bundle translations
    double_err: absolute difference between model and reference model
    native_abs_trans: reference model threshold
    """

    data_dir = \
        "../data/single_bundle/{}/Single_vs_Double_Tensioning".format(graft)

    model_rb_files = []
    double_model_rb_files = []
    for file in os.listdir(data_dir):
        if "force" in file:
            double_model_rb_files.append(
                os.path.abspath(os.path.join(data_dir, file)))

        elif "radius" in file:
            model_rb_files.append(os.path.abspath(os.path.join(data_dir,
                                                               file)))

    model_rb_files_sorted = natsort.natsorted(model_rb_files, alg=ns.REAL)
    single_posterior_translations = []
    single_model_names = []
    double_model_rb_files_sorted = natsort.natsorted(double_model_rb_files,
                                                     alg=ns.REAL)
    double_posterior_translations = []
    double_model_names = []

    native_file = data_dir + '/new_native_rb_com_data.txt'
    native_data, native_vars, native_step, native_time = \
        read_febio_output(native_file)
    native_time = np.round(np.array(native_time), 3)
    native_lachman_start_idx = np.where(native_time == 1.0)[0][0]
    native_posterior_translation = native_data['1']['y'][
                                   native_lachman_start_idx:-1]
    native_abs_trans = np.abs(native_posterior_translation[-1] -
                              native_posterior_translation[0])

    # mse = {}
    single_max_translation = {}
    single_mse = {}
    double_max_translation = {}
    double_mse = {}

    for file in model_rb_files_sorted:
        filename = os.path.join(data_dir, file)
        data, vars, step, time = read_febio_output(filename, ['x', 'y', 'z'])
        time = np.round(np.array(time), 3)

        if "_0_" in file:
            lachman_start_idx = np.where(time == 1.0)[0][0]
        else:
            lachman_start_idx = np.where(time == 4.0)[0][0]

        femur_com_y_translation = data['1']['y'][lachman_start_idx:-1]
        single_posterior_translations.append(femur_com_y_translation)
        name = os.path.basename(file.strip(
            '_rb_com_data.txt'))
        single_model_names.append(name)
        max_t = np.abs(femur_com_y_translation[-1] - femur_com_y_translation[0])
        single_max_translation[name] = max_t

        ms_error = np.abs(native_abs_trans - max_t)
        single_mse[name] = ms_error

    single_max_sorted = {k: v for k, v in sorted(single_mse.items(),
                                                 key=lambda item: item[1])}
    for key, value in take(10, single_max_sorted.items()):
        print("{} with an error of: {}".format(key, round(value, 3)))

    for file in double_model_rb_files_sorted:
        filename = os.path.join(data_dir, file)
        data, vars, step, time = read_febio_output(filename, ['x', 'y', 'z'])
        time = np.round(np.array(time), 3)

        if "_0_" in file:
            lachman_start_idx = np.where(time == 1.0)[0][0]
        else:
            lachman_start_idx = np.where(time == 4.0)[0][0]

        femur_com_y_translation = data['1']['y'][lachman_start_idx:-1]
        double_posterior_translations.append(femur_com_y_translation)
        name = 'double_' + os.path.basename(file.strip('_rb_com_data.txt'))
        double_model_names.append(name)
        max_t = np.abs(femur_com_y_translation[-1] - femur_com_y_translation[0])
        double_max_translation[name] = max_t

        ms_error = np.abs(native_abs_trans - max_t)
        double_mse[name] = ms_error

    double_max_sorted = {k: v for k, v in sorted(double_mse.items(),
                                                 key=lambda item: item[1])}
    for key, value in take(10, double_max_sorted.items()):
        print("{} with an error of: {}".format(key, round(value, 3)))

    single_trans = np.array(list(single_max_translation.values()))
    double_trans = np.array(list(double_max_translation.values()))
    single_err = list(single_mse.values())
    double_err = list(double_mse.values())

    return single_trans, single_err, double_trans, double_err, native_abs_trans
################################################################################


results_dir = '../results/Lachman/'

single_translations, single_errors, double_translations, double_errors, \
    native_abs_translation = single_vs_double()
force = np.linspace(0, 134, num=len(single_translations))
tension_levels = np.linspace(0, 180, num=len(single_translations))

nr = 1
ncol = 1
width = 4
handles = []
labels = []
rc('font', weight='bold', size='16')

fig2, ax2 = plt.subplots(nrows=nr, ncols=ncol, figsize=(12, 6))
bar_double = ax2.bar(tension_levels + width / 2, double_translations,
                     width=width,
                     color='red',
                     label='Double Channel', linewidth=2)
bar_single = ax2.bar(tension_levels - width / 2, single_translations,
                     width=width,
                     color='blue',
                     label='Single Channel', linewidth=2)
ax2.plot(tension_levels, double_translations,
         '--', color='red',
         label='Double Bundle Tension Level Curve')
ax2.plot(tension_levels, single_translations,
         '--', color='blue',
         label='Single Bundle Tension Level Curve')
ax2.set_xlabel('Graft Tension Level (N)', fontsize=20, fontweight='bold')
ax2.set_ylabel('Relative Knee Displacement (mm)', fontsize=20,
               fontweight='bold')
ax2.set_ylim(1.0, np.ceil(max(max(single_translations),
                              max(double_translations))))
ax2.set_xticks(tension_levels)
ax2.axhline(y=native_abs_translation,
            xmin=tension_levels[0], xmax=tension_levels[-1], color='green',
            ls='--', label='Healthy RM Threshold', linewidth=2)
ax2.fill_between(tension_levels,
                 single_translations,
                 double_translations,
                 color='green',
                 alpha=0.2, edgecolor='black', hatch='//')

max_diff, max_diff_idx = find_diff(single_translations,
                                   double_translations)

ax2.annotate('min difference: {} mm'.format(round(min(single_errors), 3)),
             fontweight='bold',
             fontsize=18,
             color='blue',
             xy=(tension_levels[4],
                 single_translations[4]),
             xycoords='data',
             xytext=(0.7, 0.4),
             textcoords='axes fraction',
             arrowprops=dict(color='blue', width=1, headwidth=6),
             horizontalalignment='center', verticalalignment='bottom')

ax2.annotate('min difference: {} mm'.format(round(min(double_errors), 3)),
             fontweight='bold',
             fontsize=18,
             color='red',
             xy=(tension_levels[3], double_translations[3]),
             xycoords='data',
             xytext=(0.6, 0.55),
             textcoords='axes fraction',
             arrowprops=dict(color='red', width=1, headwidth=6),
             horizontalalignment='center', verticalalignment='top')

ax2.legend(loc='upper right', fontsize=14)
ax2.grid(True)
plt.tight_layout()

fig2.savefig(
    results_dir + 'sb_vs_db.pdf',
    format='pdf',
    dpi=1200)
plt.show()
