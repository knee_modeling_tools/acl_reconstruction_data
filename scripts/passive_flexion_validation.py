# -*- coding: utf-8 -*-
# Author : Konstantinos Risvas
# Date : 2/22/2021
# Email: krisvas@ece.upatras.gr
"""
Description:

Script to compare OpenKnee and Model Passive Flexion results. The passive
flexion processing script was acquired from the OpenKnee project repository.
"""
import numpy as np
import matplotlib.pyplot as plt
import os
from utils import read_febio_output, \
    cut_data, get_kinetics_kinematics, apply_offsets, \
    change_kinematics_reporting, change_kinetics_reporting, plot_groups,\
    kinetics_tibia_to_femur, get_offsets, find_model_offsets, tdms_contents,\
    resample_data, resampling_index, crop_data
from matplotlib import cm, rc


def find_diff(y1, y2):
    # print(y1, y2)
    difference = np.abs(np.subtract(y1, y2))
    print(difference)
    max_diff = np.amax(difference)
    max_diff_idx = np.where(difference == max_diff)

    return round(max_diff, 3), max_diff_idx


def passive_flexion_processing_2(groups, experiment_offsets, model_offsets,
                                 tdms_directory):
    kinetics_group, kinematics_group = get_kinetics_kinematics(groups)

    plot_groups("Passive_Flexion_Kinematics_Raw", kinematics_group,
                'Flexion Angle (deg)',
                tdms_directory, subdir='Passive_Flexion', show_plot=False)
    plot_groups("Passive_Flexion_Kinetics_Raw", kinetics_group,
                'Flexion Angle (deg)',
                tdms_directory, subdir='Passive_Flexion', show_plot=False)

    # separate flexion and extension data
    # find max data point, anything before is flexion, anything after is
    # extension
    flexion_kinematics = kinematics_group.data[3]
    flexion_kinematics = flexion_kinematics[:][~np.isnan(flexion_kinematics[:])]
    max_flex_idx = np.argmax(flexion_kinematics)

    flex_crop_idx = np.arange(0, max_flex_idx)
    # ext_crop_idx = np.arange(max_flex_idx, len(flexion_kinematics)) # in
    # case we need the extension points for something

    # use only the flexion data
    crop_data(kinetics_group, flex_crop_idx)
    crop_data(kinematics_group, flex_crop_idx)

    # resample at 5 degree increments by averageing each channel where
    # flexion angle is within range degrees
    resampling_channel = 3
    increments = 5.0
    range = 0.5

    resamp_idx, resamp_intervals = resampling_index(kinematics_group,
                                                    resampling_channel,
                                                    increments, range)
    resample_data(kinematics_group, resamp_idx, resamp_intervals)
    resample_data(kinetics_group, resamp_idx, resamp_intervals)

    # files for data representation
    plot_groups("Passive_Flexion_Kinematics_in_JCS_experiment",
                kinematics_group, 'Flexion Angle (deg)',
                tdms_directory, subdir='Passive_Flexion', show_plot=False)
    plot_groups("Passive_Flexion_TibiaKinetics_in_TibiaCS_experiment",
                kinetics_group, 'Flexion Angle (deg)',
                tdms_directory, subdir='Passive_Flexion', show_plot=False)

    # continue processing for our workflow
    # apply experiment offsets to the kinematics data (add)
    apply_offsets(kinematics_group, experiment_offsets)

    # report the axes in the same direction as the model reporting :
    # positive directions are - extension, adduction (varus), external
    change_kinematics_reporting(kinematics_group)
    change_kinetics_reporting(kinetics_group)

    # report the kinetics as external loads applied to femur in tibia
    # coordinate system
    kinetics_tibia_to_femur(kinetics_group, kinematics_group)

    # apply model offsets (subtract) - Note this is done AFTER changing the
    # signs of the data to register with model outputs.
    apply_offsets(kinematics_group, -model_offsets)

    # save the data as csv and png files - this is what will be used to model
    # experimental conditions
    plot_groups("Passive_Flexion_Kinematics_in_JCS", kinematics_group,
                'Flexion Angle (deg)', tdms_directory, subdir='Passive_Flexion',
                show_plot=False)
    plot_groups("Passive_Flexion_Kinetics_in_TibiaCS", kinetics_group,
                'Flexion Angle (deg)', tdms_directory, subdir='Passive_Flexion',
                show_plot=False)

    return kinematics_group, kinetics_group


################################################################################
subject = 'oks003'
# get the femur and tibia cs axes
probed_landmarks = os.path.abspath(
    "../data/{}_Probed_Anatomical_landmarks.xml".format(subject))
anatomical_landmarks = os.path.abspath(
    "../data/{}_Anatomical_landmarks.xml".format(subject))

results_dir = '../results/Passive_Flexion/'
data_directory = os.path.abspath('../data/reference_model/Passive_Flexion')
files = os.listdir(data_directory)

model_files = []
tdms_files = []
State_file = None

for file in os.listdir(data_directory):
    if file.endswith('.cfg'):
        State_file = os.path.abspath(os.path.join(data_directory, file))
    elif file.endswith('.tdms'):
        tdms_files.append(os.path.abspath(os.path.join(data_directory, file)))
    elif 'reference_' in file:
        model_files.append(os.path.abspath(os.path.join(data_directory, file)))
    else:
        pass

experiment_offsets = get_offsets(State_file)
model_offsets = find_model_offsets(probed_landmarks, subject)

for file in tdms_files:
    groups = tdms_contents(file)
    if 'passive flexion' in file.lower():
        kinematics, kinetics = \
            passive_flexion_processing_2(groups,
                                         experiment_offsets,
                                         model_offsets,
                                         data_directory)

model_names = []
total_medial = []
total_posterior = []
total_superior = []
total_flexion = []
total_adduction = []
total_external = []

for file in model_files:
    data, step, vars, time = read_febio_output(file)
    # rigid connector 1
    rc1_translation = data['1']['RCx']
    rc1_rotation = data['1']['RCthx']
    rc1_rotation = -np.rad2deg(rc1_rotation)
    # rigid connector 2
    rc2_translation = data['2']['RCx']
    rc2_rotation = data['2']['RCthx']
    rc2_rotation = np.rad2deg(rc2_rotation)
    # rigid connector 3
    rc3_translation = data['3']['RCx']
    rc3_rotation = data['3']['RCthx']
    rc3_rotation = np.rad2deg(rc3_rotation)
    if rc1_rotation[-1] < 85.0:
        rc1_rotation[-1] += 1

    # keep only flexion step data
    max_flexion_angle = np.max(rc1_rotation)
    max_flexion_angle_idx = np.argmax(rc1_rotation)
    zero_flexion_angle_idx = np.argwhere(rc1_rotation[
                                         :max_flexion_angle_idx] <
                                         0.0 + 1e-3)[-1][0] + 1
    # # max_flexion_angle_idx += 1
    # flexion_angles = rc1_rotation[
    #                  zero_flexion_angle_idx:max_flexion_angle_idx]

    model_medial_translation = cut_data(rc1_translation,
                                        zero_flexion_angle_idx,
                                        max_flexion_angle_idx)
    model_posterior_translation = cut_data(rc2_translation,
                                           zero_flexion_angle_idx,
                                           max_flexion_angle_idx)
    model_superior_translation = cut_data(rc3_translation,
                                          zero_flexion_angle_idx,
                                          max_flexion_angle_idx)

    model_flexion_extension = cut_data(rc1_rotation, zero_flexion_angle_idx,
                                       max_flexion_angle_idx)
    # model_flexion_extension[0] = 0
    model_adduction_abduction = cut_data(rc2_rotation,
                                         zero_flexion_angle_idx,
                                         max_flexion_angle_idx)
    model_internal_external = cut_data(rc3_rotation, zero_flexion_angle_idx,
                                       max_flexion_angle_idx)

    steps = np.arange(0, max_flexion_angle + 5, 5)
    indices = []
    for i in steps:
        difference = np.abs(model_flexion_extension - i)
        idx = np.argwhere(difference < 5)[0][0]
        if idx not in indices:
            indices.append(idx)

    model_medial_translation = model_medial_translation[indices]
    model_posterior_translation = -model_posterior_translation[indices]
    model_superior_translation = model_superior_translation[indices]
    model_flexion_extension = model_flexion_extension[indices]
    model_adduction_abduction = model_adduction_abduction[indices]
    model_internal_external = model_internal_external[indices]

    total_medial.append(model_medial_translation)
    total_posterior.append(model_posterior_translation)
    total_superior.append(model_superior_translation)
    total_flexion.append(model_flexion_extension)
    total_adduction.append(model_adduction_abduction)
    total_external.append(model_internal_external)
    # name = os.path.basename(file.strip(
    #     'native_test_rc_data.txt'))
    name = os.path.basename(file.strip(
        'new_rc_data.txt'))
    # model_names.append(name)
    model_names.append('Proposed FE Model')


# plot data
rc('font', weight='bold')
handles = []
labels = []
colormap = cm.get_cmap('viridis', len(total_medial))
# colors = colormap.colors
colors = ['red']
fig, ax = plt.subplots(3, 2, figsize=(10, 6))

for idx, i in enumerate(total_medial):
    line, = ax[0][0].plot(model_flexion_extension, i - i[0],
                          marker='^',
                          color=colors[idx], label='Proposed FE Model')
    handles.append(line)
    labels.append('{}'.format(model_names[idx]))

line, = ax[0][0].plot(model_flexion_extension,
                      (kinematics.data[0] - kinematics.data[0][0]),
                      marker='x', color='blue', label='OpenKnee')
handles.append(line)
labels.append('OpenKnee')

ax[0][0].set_title('Medial Translation', fontweight='bold')
ax[0][0].set_xlabel('Flexion Angle (deg)', fontweight='bold')
ax[0][0].set_ylabel('Translation (mm)', fontweight='bold')
ax[0][0].grid(True)
ax[0][0].set_yticks(np.linspace(round(min(model_medial_translation), 2),
                                round(max(kinematics.data[0]), 2),
                                5))
ax[0][0].fill_between(model_flexion_extension,
                      kinematics.data[0] - kinematics.data[0][0],
                      total_medial[0] - total_medial[0][0], alpha=0.2)

max_diff1, max_diff1_idx = find_diff(
    kinematics.data[0] - kinematics.data[0][0],
    total_medial[0] - total_medial[0][0])

ax[0][0].annotate('max difference: {} mm'.format(max_diff1),
                  xy=(model_flexion_extension[max_diff1_idx], max_diff1),
                  # fontweight='bold',
                  fontsize=10,
                  xycoords='data',
                  xytext=(0.55, 0.95),
                  textcoords='axes fraction',
                  arrowprops=dict(color='red', width=1, headwidth=3),
                  horizontalalignment='right', verticalalignment='top')

# Posterior Translation
for idx, i in enumerate(total_posterior):
    line, = ax[1][0].plot(model_flexion_extension, i - i[0],
                          marker='^',
                          color=colors[idx],
                          # color='red',
                          label='{}'.format(model_names[idx]))

ax[1][0].plot(model_flexion_extension,
              -(kinematics.data[1] - kinematics.data[1][0]),
              marker='x',
              color='blue',
              label='OpenKnee')

max_diff2, max_diff2_idx = find_diff(
    -(kinematics.data[1] - kinematics.data[1][0]),
    total_posterior[0] - total_posterior[0][0])

ax[1][0].fill_between(model_flexion_extension,
                      -(kinematics.data[1] - kinematics.data[1][0]),
                      total_posterior[0] - total_posterior[0][0], alpha=0.2)

ax[1][0].annotate('max difference: {} mm'.format(max_diff2),
                  # fontweight='bold',
                  fontsize=10,
                  xy=(model_flexion_extension[max_diff2_idx], -12.5),
                  xycoords='data',
                  xytext=(0.55, 0.3),
                  textcoords='axes fraction',
                  arrowprops=dict(color='red', width=1, headwidth=3),
                  horizontalalignment='right', verticalalignment='top')

ax[1][0].set_title('Posterior Translation', fontweight='bold')
ax[1][0].set_xlabel('Flexion Angle (deg)', fontweight='bold')
ax[1][0].set_ylabel('Translation (mm)', fontweight='bold')
ax[1][0].grid(True)
ax[1][0].set_yticks(np.linspace(round(min(model_posterior_translation), 2),
                                round(max(model_posterior_translation), 2),
                                5))

# Superior Translation
for idx, i in enumerate(total_superior):
    line, = ax[2][0].plot(model_flexion_extension, i - i[0],
                          marker='^',
                          color=colors[idx],
                          # color='green',
                          label='{}'.format(model_names[idx]))

ax[2][0].plot(model_flexion_extension,
              kinematics.data[2] - kinematics.data[2][0],
              marker='x',
              color='blue', label='OpenKnee')

ax[2][0].set_title('Superior Translation', fontweight='bold')
ax[2][0].set_xlabel('Flexion Angle (deg)', fontweight='bold')
ax[2][0].set_ylabel('Translation (mm)', fontweight='bold')
ax[2][0].grid(True)
ax[2][0].set_yticks(np.linspace(round(min(model_superior_translation), 2),
                                round(max(model_superior_translation), 2),
                                5))

max_diff3, max_diff3_idx = find_diff(
    kinematics.data[2] - kinematics.data[2][0],
    total_superior[0] - total_superior[0][0])

ax[2][0].fill_between(model_flexion_extension,
                      kinematics.data[2] - kinematics.data[2][0],
                      total_superior[0] - total_superior[0][0], alpha=0.2)

ax[2][0].annotate('max difference: {} mm'.format(max_diff3),
                  xy=(model_flexion_extension[max_diff3_idx],
                      total_superior[0][max_diff3_idx] + 0.5),
                  fontweight='bold',
                  fontsize=10,
                  xycoords='data',
                  xytext=(0.85, 0.9),
                  textcoords='axes fraction',
                  arrowprops=dict(color='red', width=1, headwidth=6),
                  horizontalalignment='right', verticalalignment='top')


############################# Rotations ########################################
# handles = []
# labels = []
# fig, ax = plt.subplots(3, 1, figsize=(10, 6))
# Flexion Extension
for idx, i in enumerate(total_flexion):
    line, = ax[0][1].plot(model_flexion_extension, i - i[0],
                          marker='^',
                          color=colors[idx], label='Proposed FE Model')
    handles.append(line)
    labels.append(model_names[idx])

line, = ax[0][1].plot(model_flexion_extension, -kinematics.data[3],
                      marker='x', color='blue', label='OpenKnee')

handles.append(line)
labels.append('OpenKnee')

ax[0][1].set_title('Flexion', fontweight='bold')
ax[0][1].set_xlabel('Flexion Angle (deg)', fontweight='bold')
ax[0][1].set_ylabel('Rotation (deg)', fontweight='bold')
ax[0][1].grid(True)
ax[0][1].set_yticks(np.linspace(round(max(-kinematics.data[3]), 2),
                                round(min(-kinematics.data[3]), 2),
                                5))

max_diff1, max_diff1_idx = find_diff(
    -kinematics.data[3] - kinematics.data[3][0],
    total_flexion[0] - total_flexion[0][0])

ax[0][1].fill_between(model_flexion_extension,
                      -kinematics.data[3] + kinematics.data[3][0],
                      total_flexion[0] - total_flexion[0][0], alpha=0.2)

ax[0][1].annotate('max difference: {} deg'.format(max_diff2),
                  fontweight='bold',
                  fontsize=10,
                  xy=(model_flexion_extension[max_diff1_idx],
                      total_flexion[0][max_diff1_idx]),
                  xycoords='data',
                  xytext=(0.9, 0.2),
                  textcoords='axes fraction',
                  arrowprops=dict(color='red', width=1, headwidth=6),
                  horizontalalignment='right', verticalalignment='top')

# Varus Valgus
for idx, i in enumerate(total_adduction):
    line, ax[1][1].plot(model_flexion_extension, i - i[0],
                        marker='^', color=colors[idx],
                        label='Proposed FE Model')

ax[1][1].plot(model_flexion_extension,
              kinematics.data[4] - kinematics.data[4][0],
              marker='x',
              color='blue',
              label='OpenKnee')

ax[1][1].set_title('Valgus Rotation', fontweight='bold')
ax[1][1].set_xlabel('Flexion Angle (deg)', fontweight='bold')
ax[1][1].set_ylabel('Rotation (deg)', fontweight='bold')
ax[1][1].grid(True)
ax[1][1].set_yticks(np.linspace(round(min(kinematics.data[4])),
                                round(max(model_adduction_abduction)),
                                5))

max_diff2, max_diff2_idx = find_diff(
    kinematics.data[4] - kinematics.data[4][0],
    total_adduction[0] - total_adduction[0][0])

ax[1][1].fill_between(model_flexion_extension,
                      kinematics.data[4] - kinematics.data[4][0],
                      total_adduction[0] - total_adduction[0][0], alpha=0.2)

ax[1][1].annotate('max difference: {} deg'.format(max_diff2),
                  fontweight='bold',
                  fontsize=10,
                  xy=(model_flexion_extension[max_diff2_idx],
                      max_diff2),
                  xycoords='data',
                  xytext=(0.7, 0.8),
                  textcoords='axes fraction',
                  arrowprops=dict(color='red', width=1, headwidth=6),
                  horizontalalignment='right', verticalalignment='top')

# Internal External
for idx, i in enumerate(total_external):
    line, = ax[2][1].plot(model_flexion_extension, i - i[0],
                          marker='^',
                          color=colors[idx], label='Proposed FE Model')

ax[2][1].plot(model_flexion_extension,
              kinematics.data[5] - kinematics.data[5][0],
              marker='x',
              color='blue', label='OpenKnee')
ax[2][1].set_title('Internal Rotation', fontweight='bold')
ax[2][1].set_xlabel('Flexion Angle (deg)', fontweight='bold')
ax[2][1].set_ylabel('Rotation (deg)', fontweight='bold')
ax[2][1].grid(True)
ax[2][1].set_yticks(np.linspace(round(max(model_internal_external), 2),
                                round(min(kinematics.data[5]), 2), 5))

max_diff3, max_diff3_idx = find_diff(
    kinematics.data[5] - kinematics.data[5][0],
    total_external[0] - total_external[0][0])

ax[2][1].fill_between(model_flexion_extension,
                      kinematics.data[5] - kinematics.data[5][0],
                      total_external[0] - total_external[0][0], alpha=0.2)

ax[2][1].annotate('max difference: {} deg'.format(max_diff3),
                  fontweight='bold',
                  fontsize=10,
                  xy=(model_flexion_extension[max_diff3_idx],
                      max_diff3-2),
                  xycoords='data',
                  xytext=(1, 0.2),
                  textcoords='axes fraction',
                  arrowprops=dict(color='red', width=1, headwidth=6),
                  horizontalalignment='right', verticalalignment='top')

handles, labels = ax[0][0].get_legend_handles_labels()
fig.legend(handles, labels, loc='lower center', ncol=2,
           prop={'weight': 'bold', 'size': 12})
plt.tight_layout()
fig.subplots_adjust(top=0.9, bottom=0.15)

fig.align_ylabels(ax[:, :])

fig.savefig(results_dir + 'passive_translation_rotations.pdf',
            format='pdf',
            dpi=1200)
plt.show()

print("test")
