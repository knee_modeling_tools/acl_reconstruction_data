# -*- coding: utf-8 -*-
# Author : Konstantinos Risvas
# Date : 1/20/2021 
"""
Contains scripts useful for reading and plotting data

The functions required to process tdms files are acquired by the OpenKnee
project repository
"""
from nptdms import TdmsFile
import matplotlib.pyplot as plt
import math
import numpy as np
import copy
from lxml import etree as et
import configparser as cp
import os
import pandas as pd


class Group:
    def __init__(self, name, channels, data, units, time, processed = False):
        self.name = name
        self.channels = channels
        self.data = data
        self.units = units
        self.time = time
        self.processed = processed


class rigid_body:
    def __init__(self, mat_name, mat_id, center_of_mass):
        self.name = mat_name
        self.id = int(mat_id)
        self.center_of_mass = center_of_mass


class Cylindrical_Joint:

    def __init__(self, name, axis, origin, body_a, body_b, joint_number):
        self.name = name
        self.body_a = body_a
        self.body_b = body_b
        self.axis = axis
        self.origin = origin
        self.joint_number = joint_number


def read_febio_output(filename, data_labels=None):

    with open(filename, 'r') as file:
        lines = file.read().splitlines()

    steps = []
    variables = []
    data = []
    time = []

    for line in lines:
        if "Step" in line:
            steps.append(line)
        elif "Data" in line:
            variables.append(line)
        elif "Time" in line:
            time.append(line)
        elif "File" not in line:
            line_data = line.split(',')
            data.append(line_data)

    # Get unique variables
    if not data_labels:
        variables = list(map(lambda variable: variable.split('= ')[1],
                             variables))[0].split(';')
        unique_variables = []
        for i in variables:
            if i not in unique_variables:
                unique_variables.append(i)
            else:
                continue
        variables = unique_variables
    else:
        variables = data_labels

    # Get unique rigid body ids
    rigid_ids = [data[i][0] for i in range(len(data))]
    unique_ids = []
    for i in rigid_ids:
        if i not in unique_ids:
            unique_ids.append(i)
        else:
            continue
    rigid_ids = unique_ids

    # Get time steps
    time = list(map(lambda timestep: float(timestep.split('= ')[1]),
                    time))
    # time = [float(i) for i in time]

    # # Get simulation steps
    # steps = len(steps)

    # Get raw data without the first element which corresponds to the id
    data = [data[i][1:] for i in range(len(data))]

    data_dict = {}

    for id in rigid_ids:
        id_data = []
        id_dict = {}
        for i in range(rigid_ids.index(id), len(data), len(rigid_ids)):
            id_data.append(data[i])
        id_data = np.array(id_data, dtype=float)
        for i in range(len(variables)):
            id_dict[variables[i]] = id_data[:, i]

        data_dict[str(id)] = id_dict

    return data_dict, variables, steps, time


def quaternions_to_Euler(q0, q1, q2, q3):
    rlist = []
    plist = []
    ylist = []
    for i in range(len(q0)):
        r = math.atan2((2 * (q0[i] * q1[i] + q2[i] * q3[i])),
                       (1 - 2 * (q1[i] * q1[i] + q2[i] * q2[i])))
        rlist.append(r)

        sinp = 2 * (q0[i] * q2[i] - q3[i] * q1[i])
        if sinp > 1:
            sinp = 1
        elif sinp < -1:
            sinp = -1
        p = math.asin(sinp)
        plist.append(p)

        y = math.atan2((2 * (q0[i] * q3[i] + q1[i] * q2[i])),
                       1 - 2 * (q2[i] * q2[i] + q3[i] * q3[i]))
        ylist.append(y)

    return rlist, plist, ylist


def get_joint_axes(rigid_bodies, rot_quat_data, constraint_info):
    """ get the axes of the joints at each time step"""

    fixed_joints = {}
    # in the constraint definitions
    for name in constraint_info.keys():
        # patellofemoral axes
        if "Patellar" in name:
            if "Flexion" in name:
                fixed_joints[name] = 'FMB'
                pat_flex_name = name
            elif "Tilt" in name:
                fixed_joints[name] = 'PTB'
                pat_tilt_name = name
            elif "Rotation" in name:
                pat_rot_name = name
        else:
            if "medial_lateral" in name:
                fixed_joints[name] = 'femur_material'
                flex_ext_name = name
            elif "superior_inferior" in name:
                fixed_joints[name] = 'tibia_material'
                ext_int_name = name
            elif "anterior_posterior" in name:
                abd_add_name = name

    joint_axes = {}  # we will save all the joint axes by their name,
    # and a matrix representing the axis at every time point

    for ax_name, rb_name in iter(fixed_joints.items()):
        joint_info = constraint_info[ax_name].axis
        rigid_body_id = rigid_bodies[rb_name].id
        rot_quat = rot_quat_data[rigid_body_id]
        axes = fixed_axis(rot_quat, joint_info)
        joint_axes[ax_name] = axes

    try:
        joint_axes[abd_add_name] = np.cross(joint_axes[ext_int_name],
                                            joint_axes[flex_ext_name])
    except:
        pass
    try:
        joint_axes[pat_rot_name] = np.cross(joint_axes[pat_tilt_name],
                                            joint_axes[pat_flex_name])
    except:
        pass

    return joint_axes


def fixed_axis(rb_rotation_quaternion, fix_ax):
    # get the initial axis of the joint
    initial_axis = copy.deepcopy(fix_ax)

    # find the rotation of the rigid body connected to the fixed axis
    rotation = rotation_matrix_from_quaternion(rb_rotation_quaternion)

    # apply the rotation matrix to the initial axis to get the direction of
    # the axis at each time point
    initial_axis = np.asarray(initial_axis)
    joint_axis = np.matmul(rotation, initial_axis)

    # normalize, just in case
    norms = np.linalg.norm(joint_axis, axis=1)
    joint_axis = np.divide(joint_axis, np.reshape(norms, (len(norms), 1)))

    return joint_axis


def rotation_matrix_from_quaternion(q):
    """ calculate rotation matrix/ matrices from the quaternion data"""
    R = np.zeros((len(q) ,3 ,3))

    qi = q[:, 0]
    qj = q[:, 1]
    qk = q[:, 2]
    qr = q[:, 3]

    s = np.sqrt(
        np.power(qi, 2) + np.power(qj, 2) + np.power(qk, 2) + np.power(qr, 2))
    R[:, 0, 0] = 1 - (2 * s * (np.power(qj, 2) + np.power(qk, 2)))
    R[:, 0, 1] = 2 * s * (np.multiply(qi, qj) - np.multiply(qk, qr))
    R[:, 0, 2] = 2 * s * (np.multiply(qi, qk) + np.multiply(qj, qr))
    R[:, 1, 0] = 2 * s * (np.multiply(qi, qj) + np.multiply(qk, qr))
    R[:, 1, 1] = 1 - (2 * s * (np.power(qi, 2) + np.power(qk, 2)))
    R[:, 1, 2] = 2 * s * (np.multiply(qj, qk) - np.multiply(qi, qr))
    R[:, 2, 0] = 2 * s * (np.multiply(qi, qk) - np.multiply(qj, qr))
    R[:, 2, 1] = 2 * s * (np.multiply(qj, qk) + np.multiply(qi, qr))
    R[:, 2, 2] = 1 - (2 * s * (np.power(qi, 2) + np.power(qj, 2)))

    return R


def decompose_transformation(t):
    betta = np.arcsin(t[:, 0, 2])
    sb = np.sin(betta)
    cb = np.cos(betta)
    # divide -sinacosb and cosacosb
    alpha = np.arctan2(-t[:, 1, 2], t[:, 2, 2])
    sa = np.sin(alpha)
    ca = np.cos(alpha)
    # divide -cosbsinc and -cosbcosc
    gamma = np.arctan2(-t[:, 0, 1], t[:, 0, 0])
    sg = np.sin(gamma)
    cg = np.cos(gamma)

    # compute new translations exploring the last column
    # add row 2*cosa and row3*sina
    b = np.multiply(t[:, 1, 3], ca) + np.multiply(t[:, 2, 3], sa)
    # c ((row3cosa) - (row2*sina))/cosb
    c = np.divide((np.multiply(t[:, 2, 3], ca) - np.multiply(t[:, 1, 3], sa)),
                  cb)
    # a now is estimated by first row
    a = t[:, 0, 3] - np.multiply(c, sb)

    return a, b, c, alpha, betta, gamma


def BoneinWorld_Transform(all_bone_axes, rotation_quaternion_data,
                          rigid_bodies, bone_name, com_data):
    """create the transfromation matrix to transform a vector from world
    coordinates to femur coordinates at each time step"""

    # initial bone coordinate system defined in world coordinates
    bone_axes = all_bone_axes[bone_name]
    Bone_com = com_data[rigid_bodies[bone_name].id]

    # rotation matrix at each step representing the rotation of the femur in
    # world coordinates
    rotation_matrix = rotation_matrix_from_quaternion(
        rotation_quaternion_data[rigid_bodies[bone_name].id])

    # apply rotation matrix to the transpose of the bone axes to get the
    # orientation of the bone axes at every time step
    M = np.transpose(bone_axes)
    Bone_rot_in_World = np.matmul(rotation_matrix, M)

    # create the full transformation from bone axes,and center of mass
    # location at each time step
    Bone_in_World = np.zeros((len(Bone_rot_in_World),4,4))
    Bone_in_World[:,3,3] = 1
    Bone_in_World[:,0:3,0:3] = Bone_rot_in_World
    Bone_in_World[:,0:3,3] = Bone_com

    return Bone_in_World


def get_bone_axes(model_properties_xml, subject):
    """ find the axes of the bones in the model properties file"""

    ModelProperties_tree = et.parse(model_properties_xml)
    ModelProperties = ModelProperties_tree.getroot()
    landmarks = ModelProperties.find(subject)

    bone_axes = {}

    def extract_axes(bone_first_letter):
        x = landmarks.find('X{}_axis'.format(bone_first_letter)).text.split(',')
        y = landmarks.find('Y{}_axis'.format(bone_first_letter)).text.split(',')
        z = landmarks.find('Z{}_axis'.format(bone_first_letter)).text.split(',')

        axes = [x, y, z]
        axes = [[float(i) for i in a] for a in axes]

        return axes

    try:
        tibia = extract_axes('t')
        bone_axes['tibia_material'] = tibia
    except:
        pass
    try:
        femur = extract_axes('f')
        bone_axes['femur_material'] = femur
    except:
        pass

    return bone_axes


def GetConstraintInfo(feb_filename):
    """ extract the rigid bodies, axis, origin, for each of the cylindrical
    joints in the febio file"""

    Febio_tree = et.parse(feb_filename)
    Febio_spec_root = Febio_tree.getroot()
    Constraint_Section =Febio_spec_root.find("Constraints")
    counter = 0
    constraint_info = {}

    for constraint in Constraint_Section:

        try:
            constraint_type = constraint.attrib["type"]
            if constraint_type == "rigid cylindrical joint":
                constraint_name = constraint.attrib["name"]

                joint_axis_str = constraint.find("joint_axis").text
                joint_axis_list = joint_axis_str.split(',')
                axis = [float(x) for x in joint_axis_list]

                joint_origin_str = constraint.find("joint_origin").text
                joint_origin_list = joint_origin_str.split(',')
                origin = [float(x) for x in joint_origin_list]

                body_a_id = int(constraint.find("body_a").text)

                body_b_id = int(constraint.find("body_b").text)

                counter += 1
                cylindrical_joint = Cylindrical_Joint(constraint_name, axis,
                                                      origin, body_a_id,
                                                      body_b_id, counter)
                constraint_info[constraint_name] = cylindrical_joint

            else:
                counter += 1  # count the constraint number but ignore otherwise

        except:
            pass

    return constraint_info


def find_rigid_bodies(log_filename):
    """ find and store the rigid body information in a dictionary {
    material_name:rigid_body_object}"""
    rigid_bodies = {}
    # get rigid body information
    # use material data section not rigid body section because names are not
    # included in rigid body section
    with open(log_filename) as f:
        for line in f:
            if 'MATERIAL DATA' in line:
                next(f)
                l = next(f)
                while len(l.strip()) != 0:  # keep going until the end of the
                    # material data section
                    if 'rigid body' in l:
                        l = l.split('-')
                        mat_id = int(l[0])
                        mat_name = l[1].split('(')[
                            0].strip()  # get rid of any leading and trailing
                        # whitespace around name
                        while 'center_of_mass' not in l:
                            l = next(f)
                        com_strings = l.split(':')[-1].split(',')
                        com = []
                        for i in com_strings:
                            com.append(float(i))
                        rigid_bodies[mat_name] = rigid_body(mat_name, mat_id,
                                                            com)
                    l = next(f)
                break

    return rigid_bodies


def collect_data(log_filename):
    """ data stored in nested dictionaries.  {data_name:{
    body_id:data_as_array}} """
    time_steps = [0] # zero is just a placeholder, will remove later
    all_data = {}
    #
    with open(log_filename) as f:
        for line in f:
            if 'Data Record' in line:
                data_record_num = int(line.split('#')[-1])
                next(f) # ===== line
                next(f) # step number line
                time_line = next(f)
                time = float(time_line.split('=')[-1])
                if data_record_num == 1: # its its a new set of data records. ie a new solved time step
                    time_steps.append(time)
                data_name_line = next(f)
                data_name = data_name_line.split('=')[-1].strip()
                l = next(f)

                # check if this type of data has been added. if not add it
                if data_name not in all_data.keys():
                    all_data[data_name] = {}

                # keep going until the end of this data record
                while len(l.strip()) != 0:
                    line_as_list = l.split(',')
                    try:
                        body_id = int(line_as_list.pop(0))
                    except: break # in case there's a line of string

                    # check if this body has been added, if not add it
                    if body_id not in all_data[data_name].keys():
                        all_data[data_name][body_id] = []
                    all_data[data_name][body_id].append([float(x) for x in line_as_list])
                    l = next(f)

    time_steps = np.asarray(time_steps[1:]) # remove placeholder and turn into array

    # turn data lists into arrays
    for data_name, data_dict in iter(all_data.items()):
        try:
            for body_id, data_set in iter(data_dict.items()):
                all_data[data_name][body_id] = np.asarray(data_set)
        except:
            try:
                np.asarray(data_dict)
            except:
                pass

    return all_data, time_steps


def add_initial_values(all_data, time_steps, rigid_bodies):

    # add t=0 zero to the time_steps array
    time_steps = np.insert(time_steps, 0, [0])

    # add the initial_position of the rigid_bodies to the COM data
    com_data = all_data['center_of_mass']
    for rb in rigid_bodies.values():
        rb_id = rb.id
        rb_com_initial = rb.center_of_mass
        rb_com_data = com_data[rb_id]
        com_data[rb_id] = np.insert(rb_com_data, 0, rb_com_initial, axis=0)

    # add the initial rotation_quaterion to the initial time step
    rot_quat_data = all_data['rotation_quaternion']
    rot_quat_initial = [0, 0, 0, 1]
    for rb_id, rot_quat in iter(rot_quat_data.items()):
        rot_quat_data[rb_id] = np.insert(rot_quat, 0, rot_quat_initial, axis=0)

    return all_data, time_steps


def cut_data(data, min_index, max_index):

    # drop nan
    data = data[~np.isnan(data[:])]
    data = data[min_index: max_index+1]
    return data


def average_data(data, indices):

    data = np.asarray(data)
    averaged_data = []
    for idx in indices:
        data_i = data[idx]
        data_i_avg = np.average(data_i)
        averaged_data.append(data_i_avg)

    averaged_data = np.asarray(averaged_data)
    return averaged_data


def get_offsets(state_config):
    """ extract the offsets form the configuration file and convert to mm,
    deg from m,rad"""

    config = cp.RawConfigParser()
    if not config.read(state_config):
        raise IOError("Cannot load configuration file... Check path.")

    Knee_offsets = config.get('Knee JCS', 'Position Offset (m,rad)')
    # Knee_offsets = config.get('JCS', 'Position Offset (m,rad)')

    # convert string into list of 6 floats
    Knee_offsets = Knee_offsets.replace('"', '')
    Knee_offsets = Knee_offsets.split(" ")[1:]
    Knee_offsets = list(map(float, Knee_offsets))

    Knee_offsets = np.asarray(Knee_offsets)

    # to convert to mm and deg
    Knee_offsets[0:3] = Knee_offsets[0:3] *1000

    Knee_offsets[3:6] = Knee_offsets[3:6] * 180.0/np.pi

    # pull in the headers for the kinematics data from the state file,
    # give the same headers in the offsets file
    headers = []
    for i in range(6):
        chan_name = config.get('Knee JCS', 'Channel Names {}'.format(i))
        chan_name = chan_name.replace('"', '')
        chan_unit = config.get('Knee JCS', 'Channel Units {}'.format(i))
        chan_unit = chan_unit.replace('"', '')
        headers.append('Knee JCS '+ chan_name + ' [' + chan_unit + ']')

    return Knee_offsets


def apply_offsets(group, offsets):
    """ apply the offsets to the data in each channel"""

    data = np.asarray(group.data)
    offset_data = data.T + offsets
    offset_data = offset_data.T


def ProcessAndPlotJointKinematics(com_data, rot_quat_data, time_steps,
                                  constraint_info, joint_axes, bone_axes,
                                  rigid_bodies):


    all_kinematics = joint_kinematics_2(bone_axes, rigid_bodies, rot_quat_data,
                                        com_data, constraint_info)

    tibiofemoral_translations = {}
    tibiofemoral_rotations = {}
    patellofemoral_translations = {}
    patellofemoral_rotations = {}

    for name, kin in iter(all_kinematics.items()):
        if "Patellar" in name:
            patellofemoral_translations[name] = kin[0]
            patellofemoral_rotations[name] = kin[1]
        else:
            tibiofemoral_translations[name] = kin[0]
            tibiofemoral_rotations[name] = kin[1]

    try:
        # tibiofemoral joint
        fig = plt.figure(figsize=(12.8, 7.2))
        fig.suptitle("Kinematics of Tibiofemoral cylindrical joints")

        data_list = [("time_steps", time_steps)]

        plt.subplot(1, 2, 1)
        for label, trans in iter(tibiofemoral_translations.items()):
            plt.plot(time_steps, trans, label=label + '_axis')
            data_list.append((label + '_Translation [mm]', trans))
        plt.legend(loc='upper left')
        plt.title('Translations')
        plt.xlabel('Time')
        plt.ylabel('mm')

        plt.subplot(1, 2, 2)
        for label, rot in iter(tibiofemoral_rotations.items()):
            plt.plot(time_steps, rot, label=label + '_axis')
            data_list.append((label + '_Rotation [deg]', rot))
        plt.legend(loc='upper left')
        plt.title('Rotations')
        plt.xlabel('Time')
        plt.ylabel('deg')

        plt.savefig('Tibiofemoral_Kinematics.png')
        # plt.show()

        save_to_csv(data_list, "Tibiofemoral_Kinematics.csv")

        plt.close()

        # patellofemoral joint
        # plot joint kinematics
        fig = plt.figure(figsize=(12.8, 7.2))
        fig.suptitle("Kinematics of patellofemoral cylindrical joints")
        data_list = [("time_steps", time_steps)]

        plt.subplot(1, 2, 1)
        for label, trans in iter(patellofemoral_translations.items()):
            plt.plot(time_steps, trans, label=label + '_axis')
            data_list.append((label + '_Translation [mm]', trans))
        plt.legend(loc='upper left')
        plt.title('Translations')
        plt.xlabel('Time')
        plt.ylabel('mm')

        plt.subplot(1, 2, 2)
        for label, rot in iter(patellofemoral_rotations.items()):
            plt.plot(time_steps, rot, label=label + '_axis')
            data_list.append((label + '_Rotation [deg]', rot))
        plt.legend(loc='upper left')
        plt.title('Rotations')
        plt.xlabel('Time')
        plt.ylabel('deg')

        plt.savefig('Patellofemoral_Kinematics.png')
        # plt.show()

        save_to_csv(data_list, "Patellofemoral_Kinematics.csv")
        plt.close()
    except:
        print('failed creating images, trying to create csv file only')

        data_list = [("time_steps", time_steps)]
        for label, trans in iter(tibiofemoral_translations.items()):
            data_list.append((label + '_Translation [mm]', trans))
        for label, rot in iter(tibiofemoral_rotations.items()):
            data_list.append((label + '_Rotation [deg]', rot))

        save_to_csv(data_list, "Tibiofemoral_Kinematics.csv")

        data_list = [("time_steps", time_steps)]
        for label, trans in iter(patellofemoral_translations.items()):
            data_list.append((label + '_Translation [mm]', trans))
        for label, rot in iter(patellofemoral_rotations.items()):
            data_list.append((label + '_Rotation [deg]', rot))

        save_to_csv(data_list, "Patellofemoral_Kinematics.csv")


def save_to_csv(data_list, title):
    """data contains a list of tuples of the (headers,data) for each column of the file """

    header_string = ''
    all_data = np.zeros((len(data_list[0][1]),len(data_list)))

    for i,tup in enumerate(data_list):
        header_string += tup[0] + ','
        all_data[:,i] = tup[1]

    np.savetxt(title, all_data, delimiter=",",header=header_string)


def joint_kinematics_2(bone_axes, rigid_bodies, rotation_quaternion_data,
                       com_data, constraint_info):

    Fem_in_world = BoneinWorld_Transform(bone_axes, rotation_quaternion_data,
                                         rigid_bodies, 'femur_material',
                                         com_data)
    Tib_in_world = BoneinWorld_Transform(bone_axes, rotation_quaternion_data,
                                         rigid_bodies, 'tibia_material',
                                         com_data)
    world_in_Fem = np.linalg.inv(Fem_in_world)
    T_in_F = np.matmul(world_in_Fem, Tib_in_world)
    a, b, c, alpha, beta, gamma = kinematics_from_transform(T_in_F)

    all_kinematics = {}
    for joint_info in constraint_info.values():
        if 'medial' in joint_info.name:
            all_kinematics[joint_info.name] = (a-a[0], np.degrees(
                alpha-alpha[0]))
        elif 'anterior' in joint_info.name:
            all_kinematics[joint_info.name]= (b-b[0], np.degrees(beta-beta[0]))
        elif 'superior' in joint_info.name:
            all_kinematics[joint_info.name] = (c-c[0], np.degrees(
                gamma-gamma[0]))
        else:
            pass

    return all_kinematics


def kinematics_from_transform(T):
    # extract the rotations and translations along the joints axes from the transformation matrix
    # use: https://simtk.org/plugins/moinmoin/openknee/Infrastructure/ExperimentationMechanics?action=AttachFile&do=view&target=Knee+Coordinate+Systems.pdf
    # page 6

    beta = np.arcsin(T[:, 0, 2])
    alpha = np.arctan2(-T[:, 1, 2], T[:, 2, 2])
    gamma = np.arctan2(-T[:, 0, 1], T[:, 0, 0])

    ca = np.cos(alpha)
    sa = np.sin(alpha)
    cb = np.cos(beta)
    sb = np.sin(beta)

    b = np.multiply(T[:, 1, 3], ca) + np.multiply(T[:, 2, 3], sa)
    c = np.divide(np.multiply(T[:, 2, 3], ca) - np.multiply(T[:, 1, 3], sa), cb)
    a = T[:, 0, 3] - np.multiply(c, sb)

    return a,b,c,alpha,beta,gamma


def get_desired_kinetics(groups):

    desired_kinetics_group = None
    for g in groups:
        if g.name == 'Kinetics.JCS.Desired':
            desired_kinetics_group = g
            break
        else:
            pass

    return desired_kinetics_group


def get_kinetics_kinematics(groups):
    """ extracts the relevant groups for kinetics and kinematics processing"""

    kinetics_group = None
    kinematics_group = None

    for g in groups:
        if g.name == 'State.JCS Load':
            kinetics_group = g
        elif g.name == 'State.Knee JCS':
            kinematics_group = g
        # elif g.name == 'State.JCS':
        #     kinematics_group = g
        else:
            pass

    return kinetics_group, kinematics_group


def crop_data(group, cropping_index):
    """ crop all the data in the group by the cropping index"""
    cropped_data = []
    cropped_time = []

    for D in np.asarray(group.data):
        cd = D[cropping_index]
        cropped_data.append(cd)

    for T in np.asarray(group.time):
        ct = T[cropping_index]
        cropped_time.append(ct)

    # make changes to the group
    group.data = cropped_data
    group.time = cropped_time
    group.processed = True


def find_average_data(group, channel):

    dt = np.asarray(group.data[channel])
    ave = np.nanmean(dt)

    return ave

def find_hold_indices(group, channel):

    """ find the indices where the desired kinetics load
     was held steady on
    the channel"""

    data = np.asarray(group.data[channel])
    time = group.time[channel]
    tol = 1e-4

    change = np.abs(data[:-1] - data[1:])
    small_changes = np.where(change < tol)[0] # where the data is stable - ie force was held
    increments =np.abs(small_changes[:-1] - small_changes[1:]) # how many indices between the stable points
    large_increments = np.where(increments > 20)[0] # where the increment jumps

    # this gives the index for the data point at the start and end of each flat region
    index_end = small_changes[large_increments] + 1
    index_start = small_changes[large_increments+1] + 1

    # final "start" point is not relevant - it is when the curve returns to zero after loading is completed
    index_start = index_start[:-1]

    # create a "fake" start point for the first end point - this is the zero load
    # check length of other hold period
    other_len = index_end[1] - index_start[0]
    index_start = np.insert(index_start,0,index_end[0]-other_len)

    # # plot to check results
    # fig = plt.figure()
    # plt.plot(time, data, color='blue')
    # plt.plot(time[index_start],data[index_start],'o',color = 'red')
    # plt.plot(time[index_end],data[index_end],'o', color='yellow')
    # plt.show()

    # split the indices in half - positive and negative loading
    idx_split= int(len(index_end)/2)
    pos_end = index_end[:idx_split]
    neg_end  = index_end[idx_split:]
    pos_start = index_start[:idx_split]
    neg_start = index_start[idx_split:]

    pos_tuples = list(zip(pos_start, pos_end))
    neg_tuples = list(zip(neg_start,neg_end))

    # return just the end points.
    # to return the start and end points for each flat zone,
    # return the tuples instead
    return pos_end, neg_end


def change_kinematics_reporting(group):
    """flip the channels which are reported opposite to the model"""
    data = np.asarray(group.data)
    channels = group.channels

    updated_data = copy.deepcopy(data)
    updated_channels = copy.deepcopy(channels)

    # changing the names and directions of the channels
    # to align with our JCS definitions

    # expeirment=  "Medial"
    # model= medial translation (dont need to flip)
    updated_channels[0] = 'Knee JCS Medial Translation'

    # experiment= "Posterior"
    # model= anterior translation (need to flip)
    updated_data[1] = -data[1]
    updated_channels[1] = 'Knee JCS Anterior Translation'

    # experiment= "Superior"
    # model = superior translation (dont need to flip)
    updated_channels[2] = 'Knee JCS Superior Translation'

    # epxeriment  =  "Flexion"
    # model = extension (need to flip)
    updated_data[3] = -data[3]
    updated_channels[3] = 'Knee JCS Extension Rotation'

    # experiment = "Valgus"
    # model = abduction = valgus (dont need to flip)
    updated_channels[4] = 'Knee JCS Abduction Rotation'

    # experiment = "Internal Rotation"
    # model = External (need to flip)
    updated_data[5] = -data[5]
    updated_channels[5] = 'Knee JCS External Rotation'


    group.data = updated_data
    group.channels = updated_channels
    group.processed = True


def change_kinetics_reporting(group, loading_channel=None):
    """flip the channels which are reported opposite to the model."""

    data = np.asarray(group.data)
    channels = group.channels

    updated_data = copy.deepcopy(data)
    updated_channels = copy.deepcopy(channels)

    # changing the names and directios of the channels so the force
    # descriptions are clearly defined in our coordinate systems

    #experiment = "Lateral Drawer"
    #model = medial (need to flip)
    updated_channels[0] = 'External Tibia_x Load'
    updated_data[0] = -data[0]

    #experiment = "Anterior Drawer"
    # model = anterior
    updated_channels[1] = 'External Tibia_y Load'

    #experiment = "Distraction"
    #model = superior (need to flip - distractiong is a force in the inferior direction)
    updated_channels [2] = 'External Tibia_z Load'
    updated_data[2] = -data[2]

    #experiment = "Extension Torque"
    #model = extension
    updated_channels[3] = 'External Tibia_x Moment'

    # experiment = "Varus Torque"
    # model = valgus (need to flip)
    updated_channels[4]  = 'External Tibia_y Moment'
    updated_data[4] = -data[4]

    # experiment ="External Rotation Torque"
    # model = "external"
    updated_channels[5] = 'External Tibia_z Moment'

    group.data = updated_data
    group.channels = updated_channels
    group.processed = True

    # change the "time" to be a function of loading
    if loading_channel is not None:
        loading_data = updated_data[loading_channel]
        group.time = [loading_data] *6 # repeat for every channel


def plot_groups(title, group, x_label, tdms_directory, subdir='',
                show_plot=True):

    point = ['s', 'D', 'o', 's', 'D', 'o']

    fig = plt.figure(figsize=(12, 8))
    fig.suptitle(title, fontsize=16)

    # Creates two graphs one figure
    ax1 = plt.subplot(121)
    ax2 = plt.subplot(122)
    subplots = [ax1, ax2]

    # assign the first 3 channels to ax1, and the last 3 to ax2
    axes = [ax1, ax1, ax1, ax2, ax2, ax2]

    for i in range(6):
        ax = axes[i]
        ax.set_ylabel(group.units[i], fontsize=12)

        # mask to ignore missing data points
        dt = np.asarray(group.data[i])
        dt = dt.astype(np.double)
        dt_mask = np.isfinite(dt)

        if dt.size == 0: # no data, just create an empty graph so we know its empty
            # ax.plot(group.time[i], dt, point[i], label=group.channels[i])
            ax.set_xlabel(x_label, fontsize=12)
        elif group.processed == True:
            ax.plot(group.time[i][dt_mask], dt[dt_mask], point[i], label=group.channels[i])
            ax.set_xlabel(x_label, fontsize=12)
            if np.nanmean(group.time[i]) < 0.0: # for cases where applied load is negative, plot starting at 0 on x axis
                ax.invert_xaxis()
        else:
            ax.plot(group.time[i][dt_mask], dt[dt_mask], label=group.channels[i])
            ax.set_xlabel('Time (ms)', fontsize=12)

    for subplot in subplots:
        subplot.legend(loc='best')
        subplot.set_title(group.name, fontsize=12)
        subplot.grid(True)

    if show_plot:
        plt.show()

    # try:
    #     os.chdir('Processed_Data')
    # except:
    #     os.mkdir('Processed_Data')
    #     os.chdir('Processed_Data')

    # # Saves files as .png
    # fig.savefig(title + '.png',
    #                 format="png", dpi=100)
    #
    #
    plt.close(fig)
    #
    # save data in csv
    df = pd.DataFrame()
    df[x_label] = group.time[0]
    for i in range(6):
        df[group.channels[i] + ' [' +  group.units[i]+ ']'] = group.data[i]

    df.to_csv(os.path.abspath('../results/{}/{}.csv'.format(subdir, title)))

    # go back to the directory containing the files
    # os.chdir(tdms_directory)


def kinetics_tibia_to_femur(kinetics_group, kinematics_group,
                            loading_channel=None):
    """ convert the kinetics channel to report loads applied to femur in the
    tibia coordinate system.
    if a loading channel is given, the 'time' in the group will be updated
    too"""

    # initially, loads are reported as external tibia loads in the tibia
    # coordinate system.
    # (a 'lateral' load is really along the tibia x axis not the JCS lateral
    # axis)
    tibia_kinetics_data_at_tibia_origin = np.asarray(kinetics_group.data)

    # to report the external forces applied to the femur, we need to invert
    # the forces applied to the tibia
    femur_kinetics_data_at_tibia_origin = -tibia_kinetics_data_at_tibia_origin

    # now we need to translate the forces and moments so they are applied at
    # the femur origin instead of the tibia origin

    # find a,b,c,alpha,beta,gamma
    # note: the kinematics data was given in deg, mm. need to convert to rad, m
    kinematics_data = np.asarray(kinematics_group.data)
    a = kinematics_data[0]/1000.0
    b = kinematics_data[1]/1000.0
    c = kinematics_data[2]/1000.0

    alpha = np.radians(kinematics_data[3])
    beta = np.radians(kinematics_data[4])
    gamma = np.radians(kinematics_data[5])

    # find the transformation of tibia in femur coordinate for each time point
    T = T_tib_in_fem(a, b, c, alpha, beta, gamma)

    # invert to get the position of femur in tibia CS
    T_fem_in_tib = np.linalg.inv(T)

    # vector from tibia origin to femur origin in tibia coordinate system at
    # each time point
    vec_fmo = T_fem_in_tib[:, 0:3, 3] # units m
    vec_fmo = vec_fmo.T # transpose so it will be in the same shape as the
    # data ie (axis, time point)

    # the moments at the femur origin are the moments at the tibia origin
    # plus the forces at the tibia origin
    # cross with the moment arm (vector from femur origin to tibia origin in
    # tibia cs, so negative of vec_fmo)

    loads = femur_kinetics_data_at_tibia_origin[0:3,:] # units N
    torques = femur_kinetics_data_at_tibia_origin[3:6,:] # units Nm, or Nmm

    # check the units of loads and torques
    torque_units = kinetics_group.units[3]

    if 'Nmm' in torque_units:
        vec_fmo = vec_fmo * 1000 # convert vector to mm, results will be in Nmm

    # load_moments = np.multiply(loads, vec_fmo) # units same as initial
    # kinetics torques
    load_moments = np.cross(-vec_fmo.T, loads.T).T
    torques = torques + load_moments # units same as intial kinetics torques

    # store all the results back in the data, channels
    femur_kinetics_data_at_femur_origin = np.zeros(np.shape(
        femur_kinetics_data_at_tibia_origin))
    femur_kinetics_data_at_femur_origin[0:3, :] = loads
    femur_kinetics_data_at_femur_origin[3:6, :] = torques

    kinetics_group.data = femur_kinetics_data_at_femur_origin
    kinetics_group.channels = ['External Femur_x Load',
                               'External Femur_y Load',
                               'External Femur_z Load',
                               'External Femur_x Moment',
                               'External Femur_y Moment',
                               'External Femur_z Moment']
    kinetics_group.processed = True
    if loading_channel is not None:
        loading_data = femur_kinetics_data_at_femur_origin[loading_channel]
        kinetics_group.time = [loading_data] * 6 # repeat for every channel
        kinematics_group.time = [loading_data] * 6


def T_tib_in_fem(a, b, c, alpha, beta, gamma):
    # this transformation matrix will give the position of the tibia in the
    # femur  coordinate system for each data point

    T_fem_tib = np.zeros((len(a),4,4))

    ca = np.cos(alpha)
    cb = np.cos(beta)
    cg = np.cos(gamma)
    sa = np.sin(alpha)
    sb = np.sin(beta)
    sg = np.sin(gamma)

    T_fem_tib[:,0,0] = np.multiply(cb,cg)
    T_fem_tib[:, 0, 1] = np.multiply(-cb,sg)
    T_fem_tib[:, 0, 2] = sb
    T_fem_tib[:, 0, 3] = np.multiply(c,sb) + a

    T_fem_tib[:, 1, 0] = np.multiply(np.multiply(sa,sb),cg) + np.multiply(ca,sg)
    T_fem_tib[:, 1, 1] = -np.multiply(np.multiply(sa,sb),sg) + np.multiply(ca, cg)
    T_fem_tib[:, 1, 2] = -np.multiply(sa, cb)
    T_fem_tib[:, 1, 3] = -np.multiply(c,np.multiply(sa, cb))+ np.multiply(b,ca)

    T_fem_tib[:, 2, 0] = -np.multiply(np.multiply(ca, sb), cg) + np.multiply(sa,sg)
    T_fem_tib[:, 2, 1] = np.multiply(np.multiply(ca,sb),sg) + np.multiply(sa,cg)
    T_fem_tib[:, 2, 2] = np.multiply(ca, cb)
    T_fem_tib[:, 2, 3] = np.multiply(c,np.multiply(ca, cb))+ np.multiply(b,sa)

    T_fem_tib[:, 3, 3] = 1.0

    return T_fem_tib


def find_model_offsets(ModelPropertiesXml, subject):
    """ find the initial offsets in the tibiofemoral joint
     of the model"""

    # extract the origins and axes of the tibia and
    # femur from the model properties file
    model_properties = et.parse(ModelPropertiesXml)
    Landmarks = model_properties.getroot()
    Landmarks = Landmarks.find(subject)
    FMO = np.array(Landmarks.find("FMO").text.split(",")).astype(np.float)
    Xf_axis = np.array(Landmarks.find("Xf_axis").text.split(",")).astype(np.float)
    Yf_axis = np.array(Landmarks.find("Yf_axis").text.split(",")).astype(np.float)
    Zf_axis= np.array(Landmarks.find("Zf_axis").text.split(",")).astype(np.float)
    TBO =np.array( Landmarks.find("TBO").text.split(",")).astype(np.float)
    Xt_axis=np.array( Landmarks.find("Xt_axis").text.split(",")).astype(np.float)
    Yt_axis= np.array(Landmarks.find("Yt_axis").text.split(",")).astype(np.float)
    Zt_axis = np.array(Landmarks.find("Zt_axis").text.split(",")).astype(np.float)

    femur_axes = [Xf_axis,Yf_axis,Zf_axis]
    tibia_axes = [Xt_axis,Yt_axis,Zt_axis]

    # get the transformatrion matrix to get the tibia in the femur coordinate system
    T_in_F = Transform_A_in_B( TBO, tibia_axes, FMO, femur_axes) # this gives the transformation of the femur relative to the tibia coordnte system

    # extract the rotations and translations along the joints axes from the transformation matrix
    # use: https://simtk.org/plugins/moinmoin/openknee/Infrastructure/ExperimentationMechanics?action=AttachFile&do=view&target=Knee+Coordinate+Systems.pdf
    # page 6

    beta = np.arcsin(T_in_F[ 0, 2])
    alpha = np.arctan2(-T_in_F[ 1, 2], T_in_F[ 2, 2])
    gamma = np.arctan2(-T_in_F[ 0, 1], T_in_F[ 0, 0])

    ca = np.cos(alpha)
    sa = np.sin(alpha)
    cb = np.cos(beta)
    sb = np.sin(beta)

    b = (T_in_F[ 1, 3]*ca) + (T_in_F[ 2, 3]*sa)
    c = ((T_in_F[ 2, 3]*ca) - (T_in_F[ 1, 3]*sa))/ cb
    a = T_in_F[ 0, 3] - (c*sb)

    model_offsets = [a, b, c, alpha, beta, gamma]

    # convert alpha, beta, gamma to degrees
    model_offsets[3:6] = np.degrees(model_offsets[3:6])
    model_offsets = np.array(model_offsets)

    return model_offsets


def Transform_A_in_B(origin_a, axes_a, origin_b, axes_b):

    A_in_world = Tranform_axis_in_world(origin_a, axes_a)
    B_in_world = Tranform_axis_in_world(origin_b, axes_b)

    world_in_B = np.linalg.inv(B_in_world)

    A_in_B = np.matmul(world_in_B, A_in_world)

    return A_in_B


def Tranform_axis_in_world(origin, axes):
    """given the origin and axes of a coordinate system, find the matrix to
    transfrom from world to that coordinate system"""

    axes = np.asarray(axes)

    RM = np.eye(4)
    RM[:3,3] = origin
    RM[:3, :3] = axes.T

    return RM


def tdms_contents(file):
    """
    This function extracts all the information and data from the tdms file.
    returns the data as a list of Group objects
    """

    tdms_file = TdmsFile.read(file)
    all_groups = tdms_file.groups()

    # prints list of groups and extracts channels from tdms file

    # collect groups objects in list
    groups = []

    for grp in all_groups:
        # grp = tdms_file[gn.name]
        channels = grp.channels()

        # creates empty lists to place information later
        channel_list = []
        channel_data_list = []
        channel_unit_list = []
        channel_time_list = []

        # extracts information from each channel
        for channel in channels:
            try:
                channel_label = channel.properties["NI_ChannelName"]
                channel_units = channel.properties['NI_UnitDescription']
                channel_data = channel.data
                channel_time = channel.time_track()

            # creates lists for plotting
                channel_data_list.append(channel_data)
                channel_list.append(channel_label)
                channel_unit_list.append(channel_units)
                channel_time_list.append(channel_time)

                new_group = Group(grp.name, channel_list, channel_data_list,
                                  channel_unit_list, channel_time_list)
                groups.append(new_group)
            except:
                break

    return groups


def resampling_index(group, resampling_channel, increment, range, start = 0.0):

    # chan_idx = group.channels.index(resampling_channel)
    chan_idx = resampling_channel
    chan_data = group.data[chan_idx]

    max_value = np.max(chan_data)
    if np.isnan(max_value):
        chan_data = chan_data[:][~np.isnan(chan_data[:])]

    if increment > 0.0:
        print(np.max(chan_data)+increment)
        resampling_increments = np.arange(start, np.max(chan_data),
                                          increment)
    else:
        resampling_increments = np.arange(start, np.min(chan_data) +
                                          increment, increment)

    indices = []

    for a in resampling_increments:
        a_ind = np.where(np.abs(chan_data - a) < range)[0]

        indices.append(a_ind)

    return indices, resampling_increments


def resample_data(group, resamp_idx, resamp_intervals):
    """ given the indices of the data points included for each resampled
    point, take the average of the data points.
    add the resampled data to the group and return the new group"""

    data_resampled = []
    data = np.asarray(group.data)
    for i in resamp_idx:
        di = data[:, i]
        di_ave = np.average(di, axis=1)
        data_resampled.append(di_ave)

    data_resampled = np.asarray(data_resampled)
    data_resampled = data_resampled.T # to get it in the same format as the initial data list
    data_resampled = data_resampled.tolist()

    group.data = data_resampled
    group.time = [resamp_intervals]*len(data)
    group.processed = True


def plot_ax(ax, z_data, graft, cmap, norm, x, y):
    """

    Parameters
    ----------
    ax
    z_data
    graft
    cmap
    norm

    Returns
    -------

    """
    radii = [1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5]
    tension = [i for i in range(0, 200, 20)]
    levels = [0.01, 0.5, 1.0, 3.0, 5.0, 7.0, 9.0, 11]
    c = ax.contourf(x, y, z_data, levels, cmap=cmap, norm=norm)
    cs = ax.contour(c, colors='black')
    csl = ax.clabel(cs, inline=True, fontsize=12, colors='black')
    for it in csl:
        it.set_fontweight('normal')

    ax.set_xlabel('Graft Radius (mm)', fontsize=17, fontweight='bold')
    ax.set_xticks(radii)
    ax.set_ylabel('Graft Tension Level (N)', fontsize=17,
                  fontweight='bold')
    ax.set_yticks(tension)
    # ax3.legend()
    ax.grid(True)
    ax.set_title(graft.title(),
                 fontsize=20, fontweight='bold')
