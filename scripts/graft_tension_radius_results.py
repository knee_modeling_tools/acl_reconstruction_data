# Author : Konstantinos Risvas
# Date: 30/6/2021
# E-mail: krisvas@ece.upatras.gr
"""
Effect of graft radius and graft tension on restraining knee laxity
"""

from utils import read_febio_output
import natsort
from more_itertools import take
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc


def tension_analysis(graft='semitendinosus', rad=4):
    """

    Parameters
    ----------
    graft: the graft type defines the subdirectory where the simulation data
    are stored
    rad: graft radius

    Returns
    -------
    tension_levels: the assessed graft pretension levels
    translations: relative knee displacements
    native_posterior_translation: translation of reference model
    max_sorted: sorted absolute difference in knee displacement between each
        aclr model and the reference model
    """
    data_dir = \
        "../data/single_bundle/{}/Tension_Level/radius_{}".format(graft, rad)

    model_rb_files = []
    for file in os.listdir(data_dir):
        if "rb" and "radius" in file:
            model_rb_files.append(os.path.abspath(os.path.join(data_dir,
                                                               file)))

    model_rb_files_sorted = natsort.natsorted(model_rb_files,
                                              alg=natsort.ns.REAL)
    posterior_translations = []
    model_names = []

    # native_file = data_directory + '/native_free_femur_rb_com_data.txt'
    native_file = data_dir + '/new_native_rb_com_data.txt'
    native_data, native_vars, native_step, native_time = \
        read_febio_output(native_file)
    native_time = np.round(np.array(native_time), 3)
    native_lachman_start_idx = np.where(native_time == 1.0)[0][0]
    native_posterior_translation = native_data['1']['y'][
                                   native_lachman_start_idx:-1]

    native_abs_translation = np.abs(native_posterior_translation[-1] -
                                    native_posterior_translation[0])

    mse = {}
    max_translation = {}
    for file in model_rb_files_sorted:
        filename = os.path.join(data_dir, file)
        data, vars, step, time = read_febio_output(filename, ['x', 'y', 'z'])
        time = np.round(np.array(time), 3)

        if "_0_" in file:
            lachman_start_idx = np.where(time == 1.0)[0][0]
        else:
            lachman_start_idx = np.where(time == 4.0)[0][0]

        femur_com_y_translation = data['1']['y'][lachman_start_idx:-1]
        posterior_translations.append(femur_com_y_translation)
        name = os.path.basename(file.strip(
            '_rb_com_data.txt'))
        model_names.append(name)

        max_t = np.abs(femur_com_y_translation[-1] - femur_com_y_translation[0])
        max_translation[name] = max_t

        ms_error = np.abs(native_abs_translation - max_t)
        mse[name] = ms_error

    max_sorted = {k: v for k, v in sorted(mse.items(),
                                          key=lambda item: item[1])}
    for key, value in take(15, max_sorted.items()):
        print("{} with an error of: {}".format(key, round(value, 3)))

    translations = np.array(list(max_translation.values()))
    tension_levels = [i for i in range(0, 200, 20)]

    return tension_levels, translations, native_posterior_translation, \
        max_sorted


def radius_analysis(graft='semitendinosus', pretension_force=80):
    """

    Parameters
    ----------
    graft: the graft type defines the subdirectory where the results are stored
    pretension_force: applied pretension force on each model

    Returns
    -------
    graft_radii: the assessed graft radii
    translations: relative knee displacements
    native_posterior_translation: translation of reference model
    max_sorted: sorted absolute difference in knee displacement between each
        aclr model and the reference model
    """

    data_dir = \
        "../data/single_bundle/{}/Graft_Radius_{}".format(graft,
                                                          pretension_force)

    model_rb_files = []
    for file in os.listdir(data_dir):
        if "rb" and "radius" in file:
            model_rb_files.append(os.path.abspath(os.path.join(data_dir,
                                                               file)))

    model_rb_files_sorted = natsort.natsorted(model_rb_files,
                                              alg=natsort.ns.REAL)
    posterior_translations = []
    model_names = []

    native_file = data_dir + '/new_native_rb_com_data.txt'
    # native_file = data_directory + '/native_acl_rb_com_data.txt'
    native_data, native_vars, native_step, native_time = \
        read_febio_output(native_file)
    native_time = np.round(np.array(native_time), 3)
    native_lachman_start_idx = np.where(native_time == 1.0)[0][0]
    native_posterior_translation = native_data['1']['y'][
                                   native_lachman_start_idx:-1]
    native_abs_translation = np.abs(native_posterior_translation[-1] -
                                    native_posterior_translation[0])

    max_translation = {}
    mse = {}
    for file in model_rb_files_sorted:
        filename = os.path.join(data_dir, file)
        data, vars, step, time = read_febio_output(filename, ['x', 'y', 'z'])
        time = np.round(np.array(time), 3)

        lachman_start_idx = np.where(time == 4.0)[0][0]
        femur_com_y_translation = data['1']['y'][lachman_start_idx:-1]
        posterior_translations.append(femur_com_y_translation)
        name = os.path.basename(file.strip(
            '_80_rb_com_data.txt'))
        model_names.append(name)
        max_t = np.abs(femur_com_y_translation[-1] - femur_com_y_translation[0])
        max_translation[name] = max_t

        ms_error = np.abs(native_abs_translation - max_t)
        mse[name] = ms_error

    max_sorted = {k: v for k, v in sorted(mse.items(),
                                          key=lambda item: item[1])}
    for key, value in take(15, max_sorted.items()):
        print("{} with an error of: {}".format(key, round(value, 3)))

    graft_radii = np.linspace(1.5, 5.5, 9)
    translations = np.array(list(max_translation.values()))

    return graft_radii, translations, native_posterior_translation, \
        max_sorted

################################################################################

subject = 'oks003'

results_dir = '../results/Lachman/'

radius, rad_translations, rad_native_translation, rad_errors = \
    radius_analysis(subject)
print("---------------")
tension, ten_translations, ten_native_translation, ten_errors =  \
    tension_analysis(subject)

rc('font', weight='bold', size='16')

nr = 1
ncol = 2
handles = []
labels = []

# fig, ax = plt.subplots(nrows=nr, ncols=ncol, figsize=(12, 8))
fig1 = plt.figure(constrained_layout=True, figsize=(14, 6))
spec1 = gridspec.GridSpec(ncols=ncol, nrows=nr, figure=fig1)
ax1 = fig1.add_subplot(spec1[0, 0])
ax2 = fig1.add_subplot(spec1[0, 1])

ax1.bar(radius, rad_translations, width=0.1)
ax1.plot(radius, rad_translations, '--', color='red', label='Graft '
                                                            'Radius Curve')
ax1.axhline(y=np.abs(rad_native_translation[-1] -
                     rad_native_translation[0]),
            color='green',
            ls='--', label='Healthy RM Threshold', linewidth=2)
ax1.set_xlabel('Graft Radius (mm)', fontsize=18, fontweight='bold')
ax1.set_ylabel('Relative Knee Displacement (mm)', fontsize=20,
               fontweight='bold')
ax1.set_ylim(1.0, np.ceil(max(rad_translations)))
ax1.set_xticks(radius)
ax1.set_title('Graft Tension Force: 80 N', fontsize=20,
              fontweight='bold')
ax1.legend(prop={'weight': 'bold', 'size': 20}, loc='upper right')
ax1.grid(True)
ax1.fill_between(radius,
                 rad_translations,
                 np.abs(rad_native_translation[-1] -
                        rad_native_translation[0]),
                 color='red',
                 alpha=.2, edgecolor='black', hatch='/')

rad_er_values = list(rad_errors.values())
ax1.annotate('min difference: {} mm'.format(round(rad_er_values[0], 3)),
             fontweight='bold',
             fontsize=18,
             xy=(radius[5],
                 rad_translations[5]),
             xycoords='data',
             xytext=(0.7, 0.3),
             textcoords='axes fraction',
             arrowprops=dict(color='red', width=1, headwidth=6),
             horizontalalignment='center', verticalalignment='bottom')

ax2.bar(tension, ten_translations, width=5)
ax2.plot(tension, ten_translations, '--', color='red',
         label='Graft Tension Curve', linewidth=2)
ax2.set_xlabel('Graft Tension Level (N)', fontsize=18, fontweight='bold')
ax2.set_ylabel('Relative Knee Displacement (mm)', fontsize=20,
               fontweight='bold')
ax2.set_ylim(1.0, np.ceil(max(ten_translations)) + 1)
ax2.set_xticks(tension)
ax2.axhline(y=np.abs(ten_native_translation[-1] -
                     ten_native_translation[0]),
            xmin=tension[0], xmax=tension[-1], color='green',
            ls='--', label='Healthy RM Threshold', linewidth=2)

ax2.fill_between(tension,
                 ten_translations,
                 np.abs(ten_native_translation[-1] -
                        ten_native_translation[0]),
                 color='red',
                 alpha=.2, edgecolor='black', hatch='/')
ten_er_values = list(ten_errors.values())
ax2.annotate('min difference: {} mm'.format(round(ten_er_values[0], 3)),
             fontweight='bold',
             fontsize=18,
             xy=(tension[4],
                 ten_translations[4]),
             xycoords='data',
             xytext=(109, 4.5),
             textcoords='data',
             arrowprops=dict(color='red', width=1, headwidth=6),
             horizontalalignment='center', verticalalignment='bottom')
ax2.legend(prop={'weight': 'bold', 'size': 20}, loc='upper right')
ax2.grid(True)
ax2.set_title('Graft Radius: 4 mm', fontsize=20,
              fontweight='bold')

fig1.savefig(results_dir + 'tension_radius_results.pdf',
             format='pdf',
             dpi=1200)
plt.show()
