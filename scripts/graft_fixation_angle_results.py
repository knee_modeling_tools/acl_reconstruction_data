# Author : Konstantinos Risvas
# Date: 30/6/2021
# E-mail: krisvas@ece.upatras.gr

"""
Graft Fixation Angle Results.
"""

import os
from utils import read_febio_output
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, rc
import natsort
from natsort import realsorted, ns
from more_itertools import take


def fix_angle(subdir='Radius_4', graft='semitendinosus'):
    """

    Parameters
    ----------
    subdir: subdirectory inside the Fixation_Angle directory
    graft:  the graft type defines the subdirectory where the simulation data
    are stored

    Returns
    -------
    max_trans: femur absolute translation
    max_sort: sorted difference between FE model and reference in ascending
    order
    native_abs_trans: reference model femoral translation
    """

    data_dir = \
        "../data/single_bundle/{}/Fixation_Angle/{}".format(graft, subdir)

    model_rb_files = []
    for file in os.listdir(data_dir):
        if "fix" in file:
            model_rb_files.append(os.path.abspath(os.path.join(data_dir,
                                                               file)))

    model_rb_files_sorted = natsort.natsorted(model_rb_files, alg=ns.REAL)
    posterior_translations = []
    model_names = []

    native_file = data_dir + '/new_native_rb_com_data.txt'
    native_data, native_vars, native_step, native_time = \
        read_febio_output(native_file)
    native_time = np.round(np.array(native_time), 3)
    native_lachman_start_idx = np.where(native_time == 1.0)[0][0]
    native_posterior_translation = native_data['1']['y'][
                                   native_lachman_start_idx:-1]
    native_abs_trans = np.abs(native_posterior_translation[-1] -
                              native_posterior_translation[0])
    max_trans = {}
    mse = {}
    for file in model_rb_files_sorted:
        filename = os.path.join(data_dir, file)
        data, vars, step, time = read_febio_output(filename, ['x', 'y', 'z'])
        time = np.round(np.array(time), 3)

        if "_0_" in file:
            lachman_start_idx = np.where(time > 1.99)[0][0]
        else:
            lachman_start_idx = np.where(time == 4.0)[0][0]

        femur_com_y_translation = data['1']['y'][lachman_start_idx:-1]
        posterior_translations.append(femur_com_y_translation)
        name = os.path.basename(file.strip(
            '_rb_com_data.txt'))
        model_names.append(name)

        max_t = np.abs(femur_com_y_translation[-1] - femur_com_y_translation[0])
        max_trans[name] = max_t

        ms_error = np.abs(native_abs_trans - max_t)
        mse[name] = ms_error

    max_sort = {k: v for k, v in sorted(mse.items(),
                                        key=lambda item: item[1])}
    for key, value in take(15, max_sort.items()):
        print("{} with an error of: {}".format(key, round(value, 3)))

    return max_trans, max_sort, native_abs_trans
################################################################################


results_dir = '../results/Lachman/'
handles = []
labels = []
rc('font', weight='bold', size='16')

nr = 1
ncol = 1
fig1, ax1 = plt.subplots(nrows=nr, ncols=ncol, figsize=(12, 6))
width = 4

max_translation, max_sorted, native_abs_translation = fix_angle()

fixation_angles = np.linspace(0, 70, 15)
translations = np.array(list(max_translation.values()))

ax1.bar(fixation_angles, translations, width=1.2)
ax1.plot(fixation_angles, translations,
         '--', color='red', label='Fixation Angle Curve', linewidth=2)
ax1.set_xlabel('Graft Fixation Angle (deg)', fontsize=20, fontweight='bold')
ax1.set_ylabel('Relative Knee Displacement (mm)', fontsize=20,
               fontweight='bold')
ax1.set_ylim(2.0, np.ceil(max(translations)))
ax1.set_xticks(fixation_angles)
ax1.axhline(y=native_abs_translation,
            xmin=fixation_angles[0], xmax=fixation_angles[-1], color='green',
            ls='--', label='Healthy RM threshold', linewidth=2)

ax1.fill_between(fixation_angles,
                 translations,
                 native_abs_translation,
                 color='green',
                 alpha=0.2, edgecolor='black', hatch='//')

er_values = list(max_sorted.values())
ax1.annotate('',
             fontweight='bold',
             fontsize=18,
             xy=(fixation_angles[3],
                 translations[3]),
             xycoords='data',
             xytext=(0.12, 0.55),
             textcoords='axes fraction',
             arrowprops=dict(color='red', width=1, headwidth=6),
             horizontalalignment='right', verticalalignment='top')
ax1.text(0.2, 0.5, 'difference: {} mm'.format(round(er_values[0], 3)),
         transform=ax1.transAxes,
         rotation=35)

ax1.annotate('',
             fontweight='bold',
             fontsize=18,
             xy=(fixation_angles[4],
                 translations[4]),
             xycoords='data',
             xytext=(0.35, 0.7),
             textcoords='axes fraction',
             arrowprops=dict(color='red', width=1, headwidth=6),
             horizontalalignment='right', verticalalignment='bottom')

ax1.text(0.02, 0.45, 'difference: {} mm'.format(round(er_values[1], 3)),
         transform=ax1.transAxes,
         rotation=35)

ax1.legend(loc='upper left', prop={'weight': 'bold', 'size': 16})
ax1.grid(True)
plt.tight_layout()
fig1.savefig(
    results_dir + 'fixation.pdf',
    format='pdf',
    dpi=1200)
plt.show()
