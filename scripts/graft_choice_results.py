# Author : Konstantinos Risvas
# Date: 30/6/2021
# E-mail: krisvas@ece.upatras.gr
"""
Effect of graft choice in knee laxity restoration. This script prints the
effect of three different graft tissues on knee laxity. It visualizes all
combinations of graft pretension and radius values for each of the three grafts.
"""

import os
from utils import read_febio_output, plot_ax
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import natsort
from matplotlib import cm, rc
from more_itertools import take


def radius_tension_combination(graft):
    """

    Parameters
    ----------
    graft: the graft type defines the subdirectory where the simulation data
    are stored

    Returns
    -------

    """

    data_dir = \
        "../data/single_bundle/{}/Radius_Tension_combined".format(graft)

    radii = [1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5]
    tension = [i for i in range(0, 200, 20)]

    model_rb_files = []
    for file in os.listdir(data_dir):
        if "rb" and "radius" in file:
            model_rb_files.append(os.path.abspath(os.path.join(data_dir,
                                                               file)))

    model_rb_files_sorted = natsort.natsorted(model_rb_files,
                                              alg=natsort.ns.REAL)
    posterior_translations = []
    model_names = []

    # native_file = data_directory + '/Tensioning_free_femur_rb_com_data.txt'
    native_file = data_dir + '/new_native_rb_com_data.txt'
    native_data, native_vars, native_step, native_time = \
        read_febio_output(native_file)
    native_time = np.round(np.array(native_time), 3)
    native_lachman_start_idx = np.where(native_time == 1.0)[0][0]
    native_posterior_translation = native_data['1']['y'][
                                   native_lachman_start_idx:-1]
    native_abs_translation = np.abs(native_posterior_translation[-1] - \
                                    native_posterior_translation[0])

    translations = []
    max_translation = {}
    mse = {}
    for file in model_rb_files_sorted:
        filename = os.path.join(data_dir, file)
        data, vars, step, time = read_febio_output(filename, ['x', 'y', 'z'])
        time = np.round(np.array(time), 3)

        if "_0_" in file:
            lachman_start_idx = np.where(time == 1.0)[0][0]
        else:
            lachman_start_idx = np.where(time == 4.0)[0][0]
        femur_com_y_translation = data['1']['y'][lachman_start_idx:-1]
        posterior_translations.append(femur_com_y_translation)
        name = os.path.basename(file.strip(
            'rb_com_data.txt'))
        model_names.append(name)

        max_t = np.abs(femur_com_y_translation[-1] - femur_com_y_translation[0])
        max_translation[name] = max_t
        translations.append(max_t)

        ms_error = np.abs(native_abs_translation - max_t)
        mse[name] = ms_error

    max_sorted = {k: v for k, v in sorted(mse.items(),
                                          key=lambda item: item[1])}
    for key, value in take(10, max_sorted.items()):
        print("{} with an error of: {}".format(key, round(value, 3)))

    mse_errors = list(mse.values())
    radius_results = []
    current_idx = len(tension)
    for i in range(0, len(mse_errors), len(tension)):
        radius_results.append(mse_errors[i:current_idx])
        current_idx += len(tension)

    tension_results = []
    for i in range(0, len(radii) + 1, 1):
        tension_results.append(mse_errors[i::len(radii) + 1])

    errors = np.array(tension_results)

    return errors
################################################################################
results_dir = '../results/Lachman/'

print('\n-----------------semitendinosus results---------------------')
semitendinosus_z = radius_tension_combination('semitendinosus')

print('\n-----------------patellar tendon results---------------------')
patellar_z = radius_tension_combination('patellar_tendon')

print('\n-----------------gracilis results---------------------')
gracilis_z = radius_tension_combination('gracilis')

rc('font', weight='bold', size='15')
X = np.arange(1.5, 6, 0.5)
Y = np.arange(0, 200, 20)
X, Y = np.meshgrid(X, Y)
fig2 = plt.figure(figsize=(14, 5), constrained_layout=True)
spec2 = gridspec.GridSpec(ncols=3, nrows=1, figure=fig2)
ax1 = fig2.add_subplot(spec2[0, 0])
ax2 = fig2.add_subplot(spec2[0, 1])
ax3 = fig2.add_subplot(spec2[0, 2])

cmap = cm.coolwarm
bounds = list(np.arange(0, 9, 0.2))
norm = cm.colors.BoundaryNorm(bounds, cmap.N)
plot_ax(ax1, semitendinosus_z, 'semitendinosus', cmap, norm, X, Y)
plot_ax(ax2, patellar_z, 'patellar tendon', cmap, norm, X, Y)
plot_ax(ax3, gracilis_z, 'gracilis', cmap, norm, X, Y)
fig2.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap),
              ax=ax3,
              location='right')

fig2.savefig(
    results_dir + 'tension_radius_contours.pdf',
    format='pdf',
    dpi=1200)

plt.show()

